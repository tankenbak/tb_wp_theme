<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TankenBak
 */
global $option;

$sidebar = $template_data['layout']['sidebar_name'];

if ( ! is_active_sidebar( $sidebar ) ) {
    echo __("There is no widgets to load!, please add some", 'tankenbak');
    return;
}
$page_class = get_page_classes_tb( $template_data );
?>
<div id="secondary" class="widget-area <?php echo $page_class['sidebar']['class'] ?>" role="complementary">
    <?php dynamic_sidebar( $sidebar ); ?>
</div><!-- #secondary -->