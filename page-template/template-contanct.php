<?php
/**
 * Template Name: Kontakt
 *
 * @package TankenBak
 * @subpackage page template
 */
get_header();
$map_options = [];
$post_id = get_the_ID();
$meta = get_metadata( 'post', $post_id );

$map_options = [
    'map_id'        => 'MAPA_DOJAZD2',
    'center_adress' => $meta['adres'][0],
    'map_zoom'      => $meta['zoom'][0],
    'map_theme'     => $meta['theme'][0],
];
//$thumb_args = wp_parse_args( $map_options, $defaults );
$markers = get_field('markery');
if( $markers ){
    foreach ($markers as $marker){
        $map_options['markers'][] = [
            'adres'         => $marker['adres'],
            //'map_long_lat'  => $marker['map_long_lat'],
            'desc'     => $marker['desc'],
            'marker'     => $marker['marker']['url'],
        ];
    }
}

if( have_posts() ):  the_post();
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
             var isDraggable = $(document).width() > 480 ? true : false; // If document (your website) is wider than 480px, isDraggable = true, else isDraggable = false
                var map_options = {
                    map_id: "<?php echo $map_options['map_id']; ?>",
                    <?php if ( isset($map_options['center_adress']) && $map_options['center_adress'] != ''): ?>
                    address:  "<?php echo $map_options['center_adress']; ?>",
                    <?php endif ?>
                    <?php if ( isset($map_options['map_zoom']) && $map_options['map_zoom'] != ''): ?>
                    zoom:  <?php echo $map_options['map_zoom']; ?>,
                    <?php endif ?>
                   <?php if ( isset($map_options['map_theme']) && $map_options['map_theme'] != ''): ?>
                    theme:  <?php echo $map_options['map_theme']; ?>,
                    <?php endif ?>
                    draggable: isDraggable,
                    markers: [
                            <?php
                            foreach ( $map_options['markers'] as $map_marker ): ?>
                                {
                                <?php if ( isset($map_marker['adres']) && $map_marker['adres'] != ''): ?>
                                address:  "<?php echo $map_marker['adres']; ?>",
                                <?php endif ?>
                                <?php if ( isset($map_marker['desc']) && $map_marker['desc'] != ''): ?>
                                content:  "<?php echo marker_prepare_content( wpautop( $map_marker['desc'] ) ); ?>",
                                <?php endif ?>
                                <?php if ( isset($map_marker['map_long_lat']) && $map_marker['map_long_lat'] != ''): ?>
                                /*lat_lng:*/
                                <?php endif ?>
                                <?php if ( isset($map_marker['marker']) && $map_marker['marker'] != ''): ?>
                                icon:  "<?php echo $map_marker['marker']; ?>",
                                <?php endif ?>
                               // icon: "",

                                category: "-",
                                },
                            <?php endforeach; ?>
                            ],
                    center_on_markerclick: 0,
                    fit_to_markers: 0,
                    routes: 0,
                    route_link_label: "Driving directions",
                    clusters: 0        };
                //console.log(map_options);
                advanced_google_map( map_options );
            });
    </script>
    </div>

    <article <?php post_class() ?> >
    <div id="MAPA_DOJAZD2" style="height: 600px; background: red"></div>
    <div id="MAPA_DOJAZD" style="height: 600px; background: red"></div>
        <div class="container page-container">
        <?php get_template_part_part('block', 'content') ?>
        </div>
    </article>
    <div class="container">
    <?php
else:
    get_template_part_content( 'content', $template_data['layout']['none'] ); // post/s not found
endif;
?>




<?php
//get_template_part_tb('tests/test', 'template', $pass_data);
get_footer();
?>





