<?php
// initialize all the category caches we need.
//`add_action( 'init', 'my_setup_category_caches' );
// init menu cache
add_action( 'init', 'GWP_Menu_cache_init', 11 );

function GWP_Menu_cache_init() {
    global $option;
    if ( isset($option['admin-menu_cache-trants']) ) {
        $use_transient = $option['admin-menu_cache-transients'];
        if ( $use_transient === '1' ) {
             //   $GLOBALS['wp_menu_cache'] = new GWP_menu_cache();
        }
    }
}
// NOT FULLY CHECKED
/*
Plugin Name: GWP Menu Cache
Plugin URI: https://hitchhackerguide.com/2011/11/01/reducing-postmeta-queries-with-update_meta_cache/
Description: Cache wp meta queries
Version: 1
Author: THORSTEN
Author URI: https://hitchhackerguide.com/2011/11/01/reducing-postmeta-queries-with-update_meta_cache/
*/
add_filter( 'posts_results', 'cache_meta_data', 9999, 2 );
function cache_meta_data( $posts, $object ) {
    $posts_to_cache = array();
    // this usually makes only sense when we have a bunch of posts
    if ( empty( $posts ) || is_wp_error( $posts ) || is_single() || is_page() || count( $posts ) < 3 )
        return $posts;

    foreach( $posts as $post ) {
        if ( isset( $post->ID ) && isset( $post->post_type ) ) {
            $posts_to_cache[$post->ID] = 1;
        }
    }

    if ( empty( $posts_to_cache ) )
        return $posts;

    update_meta_cache( 'post', array_keys( $posts_to_cache ) );
    unset( $posts_to_cache );

    return $posts;
}

// SEEMS WORKING !
/*
Plugin Name: GWP Menu Cache
Plugin URI:
Description: A plugin to cache WordPress menus using the Transients API, based on this tutorial http://generatewp.com/?p=10473
Version: 1.0
Author: Ohad Raz
Author URI: http://generatewp.com
*/
/**
* GWP_menu_cache
*/
class GWP_menu_cache{
    /**
     * $cache_time
     * transient expiration time
     * @var int
     */
    public $cache_time = WEEK_IN_SECONDS; // cache time
    /**
     * $timer
     * simple timer to time the menu generation
     * @var time
     */
    public $timer;

    /**
     * __construct
     * class constructor will set the needed filter and action hooks
     *
     */
    function __construct(){
        global $wp_version;
        // only do all of this if WordPress version is 3.9+
        if ( version_compare( $wp_version, '3.9', '>=' ) ) {

            //show the menu from cache
            add_filter( 'pre_wp_nav_menu', array($this,'pre_wp_nav_menu'), 10, 2 );
            //store the menu in cache
            add_filter( 'wp_nav_menu', array($this,'wp_nav_menu'), 10, 2);
            //refresh on update
            add_action( 'wp_update_nav_menu', array($this,'wp_update_nav_menu'), 10, 1);
            // update for widgets
            add_filter('widget_nav_menu_args', array($this, 'widget_nav_menu_args'), 10, 3);
        }
    }

    /**
     * get_menu_key
     * Simple function to generate a unique id for the menu transient
     * based on the menu arguments and currently requested page.
     * @param  object $args     An object containing wp_nav_menu() arguments.
     * @return string
     */
    // function get_menu_key($args){
    //     return 'MC-' . md5( serialize( $args ).serialize(get_queried_object()) );
    // }
    function get_menu_key($args){
        return 'MC-' . md5(serialize($args->theme_location) . serialize(get_queried_object()) );
    }
    /**
     * get_menu_transient
     * Simple function to get the menu transient based on menu arguments
     * @param  object $args     An object containing wp_nav_menu() arguments.
     * @return mixed            menu output if exists and valid else false.
     */
    function get_menu_transient($args){
        $key = $this->get_menu_key($args);
        return get_transient($key);
    }



    /**
     * pre_wp_nav_menu
     *
     * This is the magic filter that lets us short-circit the menu generation
     * if we find it in the cache so anything other then null returend will skip the menu generation.
     *
     * @param  string|null $nav_menu    Nav menu output to short-circuit with.
     * @param  object      $args        An object containing wp_nav_menu() arguments
     * @return string|null
     */
    function pre_wp_nav_menu($nav_menu, $args){
        $this->timer = microtime(true);
        $in_cache = $this->get_menu_transient($args);
        $last_updated = get_transient('MC-' . $args->theme_location . '-updated');
        //var_dump($in_cache );
       // var_dump($last_updated);
        if (isset($in_cache['data']) && isset($last_updated) &&  $last_updated < $in_cache['time'] ){
            return $in_cache['data'].'<!-- From menu cache in '.number_format( microtime(true) - $this->timer, 5 ).' seconds -->';
        }
        return $nav_menu;
    }
    /**
    * widget_nav_menu_args
    * Filters the arguments passed to wp_nav_menu() function when fired within the
    * native WP Custom Menu Widget, by adding the theme_location parameter to them.
    *
    * This is a method hooked to a filter in the WP Widget Menu class.
    * Since in that wp_nav_menu() is not given theme_location in args, that results
    * in breaking this caching system for key generation based on that parameter.
    *
    * @since 4.2.0
    * @access public
    * @see WP_Nav_Menu_Widget in /wp-includes/default-widgets.php
    * @param array $nav_menu_args {
    * an array of arguments passed to wp_nav_menu() to retreive a custom menu.
    * @type callback|bool $fallback_cb Callback to fire if the menu doesn’t exist. Default empty.
    * @type mixed $menu Menu ID, slug, or name.
    * }
    * @param stdClass $nav_menu Nav menu object for the current menu.
    * @param array $args Display arguments for the current widget.
    */
    public function widget_nav_menu_args($nav_menu_args, $nav_menu, $args) {
        $locations = get_nav_menu_locations(); //retrieve menus theme_locations.
        $the_location = array_search($nav_menu->term_id, $locations); //get theme_location for this menu.
        if ($the_location !== false) {
            $nav_menu_args['theme_location'] = $the_location; //set theme_location in new args passed to wp_nav_menu
        }
        return $nav_menu_args; //we are good to go :)
    }

    /**
     * wp_nav_menu
     * store menu in cache
     * @param  string $nav      The HTML content for the navigation menu.
     * @param  object $args     An object containing wp_nav_menu() arguments
     * @return string           The HTML content for the navigation menu.
     */
    function wp_nav_menu( $nav, $args ) {
        $last_updated = get_transient('MC-' . $args->theme_location . '-updated');

        if( ! $last_updated ) {
            set_transient('MC-' . $args->theme_location . '-updated', time());
        }
        //$last_updated = get_transient('MC-' . $args->theme_location . '-updated');
        //var_dump($last_updated);
        $key = $this->get_menu_key($args);
        $data = array('time' => time(), 'data' => $nav);
        //var_dump($last_updated);
        set_transient( $key, $data ,$this->cache_time);
        return $nav.'<!-- Not From menu cache in '.number_format( microtime(true) - $this->timer, 5 ).' seconds -->';
    }

    /**
     * wp_update_nav_menu
     * refresh time on update to force refresh of cache
     * @param  int $menu_id
     * @return void
     */
    function wp_update_nav_menu($menu_id) {
        $locations = array_flip(get_nav_menu_locations());

        if( isset($locations[$menu_id]) ) {
            set_transient('MC-' . $locations[$menu_id] . '-updated', time());
        }
    }
}//end class for menu transients

/*
Cache posts in category/tag/taxonomy for better performing content blocks
@ https://hitchhackerguide.com/2012/05/10/cache-posts-in-categorytagtaxonomy-for-better-performing-content-blocks/
*/

class Cached_Category_Posts {

    private $args = array();
    private $cache_key = array();
    private $default_filter = 'cached_category_posts_defaults';
    private $args_filter = 'cached_category_posts_arguments';
    private $check_taxonomies = array( 'category' );

    public function __construct( $args=array() ) {
        $defaults = apply_filters( $this->default_filter, array( 'posts_per_page' => 10, 'orderby' => 'date', 'post_status' => 'publish' ) );
        $this->args = apply_filters( $this->args_filter, wp_parse_args( $args, $defaults ) );

        if ( !isset( $this->args['cat'] ) || empty( $this->args['cat'] ) )
            return new WP_Error( 'cached_category_posts_error', __( 'cat argument cannot be empty' ) );

        // make sure $this->args['cat'] is an integer
        $this->args['cat'] = (int) $this->args['cat'];

        // build a cache key based on the arguments
        $this->cache_key = md5( serialize( $this->args ) );

        // we know we only need to update the cache when terms change, so lets monitor term changes
        add_action( 'set_object_terms', array( &$this, 'maybe_refresh_cache' ), 9999, 6 );
    }

    /**
     * Cause a cache update whenever a term changes. Make sure to do this only when also other argument parameters
     * fit the post data to avoid cache refreshs every time a draft is saved
     */
    public function maybe_refresh_cache( $object_id, $terms, $tt_ids, $taxonomy, $append, $old_tt_ids ) {
        if ( !in_array( $taxonomy, $this->check_taxonomies ) )
            return;

        if ( !isset( $this->args['cat'] ) || empty( $this->args['cat'] ) )
            return;

        $post = get_post( $object_id );
        if ( is_wp_error( $post ) )
            return;

        // compare additional values in the post with the arguments set. eg post_status = publish
        foreach( array_keys( (array) $post ) as $key ) {
            if ( isset( $this->args[$key] ) && $post->{$key} <> $this->args[$key] ) {
                return;
            }
        }

        // grab an array of all terms included in this post and receive their term_ids
        $all_terms = array_unique( array_merge( $tt_ids, $old_tt_ids ) );
        $all_terms = array_map( array( &$this, 'get_term_id_from_tt_id' ), $all_terms );

        // if none of the set categories is in our arguments just skip it
        if ( !in_array( $this->args['cat'], $all_terms ) )
            return;

        // otherwise we need to refresh the cache
        $this->update();
    }

    /**
     * Receive the term_id from a term_taxonomy_id
     */
    private function get_term_id_from_tt_id( $tt_id ) {
        global $wpdb;
        return $wpdb->get_var( $wpdb->prepare( "SELECT term_id FROM $wpdb->term_taxonomy WHERE term_taxonomy_id = %d", $tt_id ) );
    }

    /**
     * Refresh the caches by calling get() with force_refresh = true
     */
    public function update() {
        return $this->get( $force_refresh = true );
    }

    /**
     * Run a WP_Query call with a check against a cached result first.
     */
    public function get( $force_refresh = false ) {
        if ( !isset( $this->args['cat'] ) || empty( $this->args['cat'] ) )
            return new WP_Error( 'cached_category_posts_error', __( 'cat argument cannot be empty' ) );;

        $result = get_transient( $this->cache_key );
        if ( ! $result || true === $force_refresh ) {
            $result = new WP_Query( $this->args );
            set_transient( $this->cache_key, $result );
        }
        return $result;
    }

}

function my_setup_category_caches() {
    global $my_cat_stories;
    $my_cat_stories['home-top-story'] = new Cached_Category_Posts( $args = array( 'cat' => 1, 'posts_per_page' => 1 ) );
    // possible usage in theme template $topstories = my_get_category_posts( 'home-top-story-2' );
}

// get category posts by their category identifier
function my_get_category_posts( $id ) {
    global $my_cat_stories;
    if ( isset( $my_cat_stories[$id] ) )
        return $my_cat_stories[$id]->get();
    else
        return false;
}





// add_filter( 'widget_update_callback', 'tb_widget_update_hook', 10, 3 );
// function tb_widget_update_hook( $instance, $new_instance, $old_instance ) {
//     return $instance;
// }





