<?php
// tempalte for global section titles
// Require to pass parametr string as title
?>
<?php if($template_data !== '') : ?>
    <div class="fc-section-heading">
        <h3>
            <?php echo $template_data ?>
        </h3>
    </div>
<?php endif ?>