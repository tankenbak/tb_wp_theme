<?php
// Image slaider based on owl carousel
if ( ! defined( 'ABSPATH' ) ) exit; // we dont like direct access

return array (
    'text-block',
    'slider',
    'image',
    'gallery',
    'image-carousel',
    'posts',
    'products',
    'call-to-action',
    'accordion',
    'mailchimp',
    'separator',
    'team',
    'files',
    'map',
    'flexible-layout',
);