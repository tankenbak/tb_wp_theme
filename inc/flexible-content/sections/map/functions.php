<?php
if ( ! defined( 'ABSPATH' ) ) exit; // we dont like direct access


function enqueue_google_map_script() {
    if( wp_script_is( 'jquery', 'done' ) && !wp_script_is( 'fc-google-map', 'enqueued' )) {
        wp_enqueue_script( 'fc-google-map', '//maps.google.com/maps/api/js?key='.tankenbak_google_maps_api_key(), '1.2.0', true) ;
    }
}


function fc_map_marker_prepare_text($text) {
    $text = str_replace('"', '\"', $text);
    $text = str_replace("'", "\'", $text);
    $text = str_replace(array("\n", "\r"), '<br>', $text);
    $text = str_replace(array("<p>", "</p>"), '', $text);
    return $text;
}


function fc_map_marker_prepare_content($content) {
    $content = str_replace('"', '\"', $content);
    $content = str_replace("'", "\'", $content);
    $content = str_replace(array("\n", "\r"), '', $content);
    return $content;
}



/* Add to Redux framework map options if redux exist */
if ( class_exists( 'Redux' ) ) {
    add_action( 'redux/construct', 'fc_setup_map_theme_option' );
}

function fc_setup_map_theme_option($redux){
    $fc_map_predefined_themes = fc_map_predefined_themes();

    $base_name = 'fc_map_opt';
    $redux->sections[]=  array(
        'id'         => $base_name.'fc_diver_05wR84Ds',
        'subsection' => false,
        'type' => 'divide'
    );
    $redux->sections[] = array(
        'title'  => __( 'Map', 'tankenbak' ),
        'id'     => $base_name,
        'permissions' => 'manage_options',
        'icon'   => 'el el-map-marker',
        'full_width' => true,
        'fields'     => array(
                array(
                    'id'=> $base_name.'-type',
                    'type' => 'switch',
                    'title' => __('Use Custom style', 'tankenbak'),
                    'default' => false,
                    'on' => __('Yes', 'tankenbak'),
                    'off' => __('No', 'tankenbak'),
                ),
                array(
                    'id'         => $base_name.'-map-api',
                    'type'       => 'text',
                    'title'      => __('Map API KEY', 'tankenbak'),
                    'subtitle'   => __('google map api key', 'tankenbak'),
                    'placeholder'=> 'API KEY',
                    'default'       => '',
                ),
           array(
               'id'       => $base_name.'-custom',
               'required' => array( $base_name.'-type','equals',true),
               'type'     => 'textarea',
               'url'      => true,
               'full_width' => true,
               'title'    => __('Map style', 'tankenbak'),
               'info'     => __('Enter map style code, you can get map style on <a target="_blank" href="https://snazzymaps.com/" >snazzymaps</a>', 'tankenbak'),
               'desc'       => __( 'Example Code: [{"featureType":"all","elementType":"all","stylers":[{"invert_lightness":true},{"saturation":-80},{"lightness":30},{"gamma":0.5},{"hue":"#3d433a"}]}]' , 'takenbak'),
               'default'  => '',
            ),
            array(
                'id'        => $base_name.'-predefined',
                //'type'      => 'select_image',
                'type'      => 'image_select',
                'full_width' => true,
                'title'     => __('Predefined themes', 'tankenbak'),
                'subtitle'  => __('You can choose one of predefined maps themes, </br>'
                                 .'Map styles in order: </br><small><ol>'
                                 .'<li>Default gogole map, </li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/13429/redand-black">Black and red</a></li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/6654/dark">Dark</a></li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/10453/bluetacticle">Blue tacticle</a></li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/13/neutral-blue">Neutral blue</a></li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/134/light-dream">Light dream</a></li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/25/blue-water">Blue water</a></li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/30/cobalt">Cobalt</a></li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/35/avocado-world">Avocado</a></li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/8381/even-lighter">Even Lighter</a></li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/28702/tko-website-redesign-map">TKO Website</a></li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/14889/flat-pale">Flat palete</a></li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/70/unsaturated-browns">Unsaturated Browns</a></li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/15/subtle-grayscale">Subtle Grayscale</a></li>'
                                 .'<li><a target="_blank" href="https://snazzymaps.com/style/153/military">Military</a></li>'
                                 .'</ol></small>', 'tankenbak'),
                'options'   => $fc_map_predefined_themes,
                'default'   => 'default',
                'class'     => 'fc-map_styles'
                ),
            ),
    );

}


function fc_map_predefined_themes() {
    $path = get_template_directory_uri().'/inc/flexible-content/sections/map/img/themes/';
    return array(
        "default" => array(
            'alt'       => __('Default Google map', 'redux_option'),
            'img'       => $path.'default.jpg',
        ),
        "black_red" => array(
            'alt'       => __('Black and red', 'redux_option'),
            'img'       => $path.'black_red.jpg',
        ),
        "dark" => array(
            'alt' => __('Dark', 'redux_option'),
            'img' => $path.'dark.jpg'
        ),
        "blue_tacticle" => array(
            'alt' => __('Blue tacticle', 'redux_option'),
            'img' => $path.'blue_tacticle.jpg'
        ),
        "neutral_blue" => array(
            'alt' => __('Neutral blue', 'redux_option'),
            'img' => $path.'neutral_blue.jpg'
        ),
        "light_dream" => array(
            'alt' => __('Light dream', 'redux_option'),
            'img' => $path.'light_dream.jpg'
        ),
        "blue_water" => array(
            'alt' => __('Blue water', 'redux_option'),
            'img' => $path.'blue_water.jpg'
        ),
        "cobalt" => array(
            'alt' => __('Cobalt', 'redux_option'),
            'img' => $path.'cobalt.jpg'
        ),
        "avocado" => array(
            'alt' => __('Avocado', 'redux_option'),
            'img' => $path.'avocado.jpg'
        ),
        "even_lighter" => array(
            'alt' => __('Even lighter', 'redux_option'),
            'img' => $path.'even_lighter.jpg'
        ),
        "tko_website" => array(
            'alt' => __('TKO Website', 'redux_option'),
            'img' => $path.'tko_website.jpg'
        ),
        "flat_palete" => array(
            'alt' => __('Flat palete', 'redux_option'),
            'img' => $path.'flat_palete.jpg'
        ),
        "unsaturated_browns" => array(
            'alt' => __('Unsaturated Browns', 'redux_option'),
            'img' => $path.'unsaturated_browns.jpg'
        ),
        "subtle_grayscale" => array(
            'alt' => __('Subtle Grayscale', 'redux_option'),
            'img' => $path.'subtle_grayscale.jpg'
        ),
        "military" => array(
            'alt' => __('Military', 'redux_option'),
            'img' => $path.'military.jpg'
        ),
    );
}

// Map colors presets,
// array key must be same as map spelect option key

function fc_get_map_styles( $style = 'default' ){
    $styles = array(
        'default' => '',

        'black_red' => '[{"featureType":"all","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"all","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"all","elementType":"geometry.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"all","elementType":"geometry.stroke","stylers":[{"visibility":"simplified"}]},{"featureType":"all","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"all","elementType":"labels.text","stylers":[{"visibility":"simplified"}]},{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40},{"visibility":"simplified"}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"simplified"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"},{"color":"#d1232a"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20},{"visibility":"simplified"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2},{"visibility":"simplified"}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative","elementType":"labels.text","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.country","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.neighborhood","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"landscape.natural","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#d1232a"},{"lightness":17}]}]',

        'dark' => '[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"landscape","elementType":"labels.icon","stylers":[{"saturation":"-100"},{"lightness":"-54"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"on"},{"lightness":"0"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"saturation":"-89"},{"lightness":"-55"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"transit.station","elementType":"labels.icon","stylers":[{"visibility":"on"},{"saturation":"-100"},{"lightness":"-51"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]',

        'blue_tacticle' => '[{"featureType":"all","elementType":"labels.text","stylers":[{"color":"#a1f7ff"}]},{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"color":"#000000"},{"lightness":13}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#144b53"},{"lightness":14},{"weight":1.4}]},{"featureType":"administrative","elementType":"labels.text","stylers":[{"visibility":"simplified"},{"color":"#a1f7ff"}]},{"featureType":"administrative.province","elementType":"labels.text","stylers":[{"visibility":"simplified"},{"color":"#a1f7ff"}]},{"featureType":"administrative.locality","elementType":"labels.text","stylers":[{"visibility":"simplified"},{"color":"#a1f7ff"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text","stylers":[{"visibility":"simplified"},{"color":"#a1f7ff"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#08304b"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#0c4152"},{"lightness":5}]},{"featureType":"poi.attraction","elementType":"labels","stylers":[{"invert_lightness":true}]},{"featureType":"poi.attraction","elementType":"labels.text","stylers":[{"visibility":"simplified"},{"color":"#a1f7ff"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"invert_lightness":true}]},{"featureType":"poi.park","elementType":"labels.text","stylers":[{"visibility":"simplified"},{"color":"#a1f7ff"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"color":"#a1f7ff"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#0b434f"},{"lightness":25}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"lightness":"0"},{"saturation":"0"},{"invert_lightness":true},{"visibility":"simplified"},{"hue":"#00e9ff"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"simplified"},{"color":"#a1f7ff"}]},{"featureType":"road.highway.controlled_access","elementType":"labels.text","stylers":[{"color":"#a1f7ff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#0b3d51"},{"lightness":16}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"invert_lightness":true}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"road.local","elementType":"labels","stylers":[{"visibility":"simplified"},{"invert_lightness":true}]},{"featureType":"transit","elementType":"all","stylers":[{"color":"#146474"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#021019"}]}]',

        'neutral_blue'  => '[{"featureType":"water","elementType":"geometry","stylers":[{"color":"#193341"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#2c5a71"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#29768a"},{"lightness":-37}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#3e606f"},{"weight":2},{"gamma":0.84}]},{"elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"weight":0.6},{"color":"#1a3541"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#2c5a71"}]}]',

        'light_dream'   => '[{"featureType":"landscape","stylers":[{"hue":"#FFBB00"},{"saturation":43.400000000000006},{"lightness":37.599999999999994},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#FFC200"},{"saturation":-61.8},{"lightness":45.599999999999994},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#0078FF"},{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#00FF6A"},{"saturation":-1.0989010989011234},{"lightness":11.200000000000017},{"gamma":1}]}]',

        'blue_water'    => '[{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]',

        'cobalt'        => '[{"featureType":"all","elementType":"all","stylers":[{"invert_lightness":true},{"saturation":10},{"lightness":30},{"gamma":0.5},{"hue":"#435158"}]}]',

        'avocado'       => '[{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#aee2e0"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#abce83"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#769E72"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#7B8758"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"color":"#EBF4A4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#8dab68"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#5B5B3F"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ABCE83"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#A4C67D"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#9BBF72"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#EBF4A4"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#87ae79"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#7f2200"},{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"},{"visibility":"on"},{"weight":4.1}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#495421"}]},{"featureType":"administrative.neighborhood","elementType":"labels","stylers":[{"visibility":"off"}]}]',

        'flat_palete'       => '[{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#6195a0"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"lightness":"0"},{"saturation":"0"},{"color":"#f5f5f2"},{"gamma":"1"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"lightness":"-3"},{"gamma":"1.00"}]},{"featureType":"landscape.natural.terrain","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#bae5ce"},{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#fac9a9"},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"color":"#4e4e4e"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#787878"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"transit.station.airport","elementType":"labels.icon","stylers":[{"hue":"#0a00ff"},{"saturation":"-77"},{"gamma":"0.57"},{"lightness":"0"}]},{"featureType":"transit.station.rail","elementType":"labels.text.fill","stylers":[{"color":"#43321e"}]},{"featureType":"transit.station.rail","elementType":"labels.icon","stylers":[{"hue":"#ff6c00"},{"lightness":"4"},{"gamma":"0.75"},{"saturation":"-68"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#eaf6f8"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#c7eced"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"lightness":"-49"},{"saturation":"-53"},{"gamma":"0.79"}]}]',

        'even_lighter'      => '[{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#6195a0"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#e6f3d6"},{"visibility":"on"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#f4d2c5"},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"color":"#4e4e4e"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#f4f4f4"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#787878"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#eaf6f8"},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#eaf6f8"}]}]',

        'tko_website'       => '[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#b4dce2"}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#20505f"},{"weight":2},{"gamma":0.84}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"weight":0.6},{"color":"#094251"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#16414f"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#518fa0"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"lightness":"-20"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"lightness":"0"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#11586b"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#12869b"},{"lightness":-37}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"lightness":"-10"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"lightness":"-5"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"lightness":"0"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"lightness":"-15"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"lightness":"-10"},{"gamma":"1.00"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#518fa0"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#05323e"}]}]',

        'unsaturated_browns' => '[{"elementType":"geometry","stylers":[{"hue":"#ff4400"},{"saturation":-68},{"lightness":-4},{"gamma":0.72}]},{"featureType":"road","elementType":"labels.icon"},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"hue":"#0077ff"},{"gamma":3.1}]},{"featureType":"water","stylers":[{"hue":"#00ccff"},{"gamma":0.44},{"saturation":-33}]},{"featureType":"poi.park","stylers":[{"hue":"#44ff00"},{"saturation":-23}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"hue":"#007fff"},{"gamma":0.77},{"saturation":65},{"lightness":99}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"gamma":0.11},{"weight":5.6},{"saturation":99},{"hue":"#0091ff"},{"lightness":-86}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"lightness":-48},{"hue":"#ff5e00"},{"gamma":1.2},{"saturation":-23}]},{"featureType":"transit","elementType":"labels.text.stroke","stylers":[{"saturation":-64},{"hue":"#ff9100"},{"lightness":16},{"gamma":0.47},{"weight":2.7}]}]',

        'subtle_grayscale' => '[{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]',

        'military'      => '[{"featureType":"all","elementType":"all","stylers":[{"invert_lightness":true},{"saturation":-80},{"lightness":30},{"gamma":0.5},{"hue":"#3d433a"}]}]',
    );

    return $styles[$style];
}