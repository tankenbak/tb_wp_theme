<?php
global $option;
$map_id = str_shuffle("abcdefghijk");
$main_address = $template_data['markers'][0]['coordinates'];
$zoom = $template_data['zoom'];

// map styles based o settings and map defined setup
if( isset( $template_data['map_theme'] ) && $template_data['map_theme'] != '' ){
    $theme = $template_data['map_theme'];
}elseif( isset( $option['fc_map_opt-custom'] ) && $option['fc_map_opt-custom'] != '' && $option['fc_map_opt-type'] === '1' ){
    $theme = $option['fc_map_opt-custom'];
}elseif( isset( $option['fc_map_opt-predefined'] ) && $option['fc_map_opt-predefined'] != '' && $option['fc_map_opt-predefined'] != 'default' && function_exists('fc_get_map_styles')  ){
    $theme = fc_get_map_styles($option['fc_map_opt-predefined']);
}else{
    $theme = '[]';
}

$markers = [];
$center_on_markerclick = 1;
$fit_to_markers = ($template_data['fit_to_markers'] != '')? 1 : 0;
$routes = 1;
$clusters = 1;
?>
<section class="flexible-content fc-map  <?php
    if($template_data['bottom_spacing'] === '0') echo 'no-spacing';
    if($template_data['setion_width'] === 'full_width') { echo ' full-width'; }else{  echo ' container'; };
    ?>">
<div class="row">
    <div id="<?php echo $map_id ?>" class="fc-map-container" ></div>
    <div id="directions_<?php echo $map_id ?>"></div>
</div>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            var map_options = {
                map_id: "<?php echo $map_id ?>",
                address: "<?php echo $main_address ?>",
                zoom: <?php echo $zoom ?>,
                theme: <?php echo $theme ?>,
                markers: [
                    <?php if(!empty($template_data['markers'])): foreach ($template_data['markers'] as $marker): ?>
                        <?php
                        $marker_cat = '';
                        $marker_address = fc_map_marker_prepare_text($marker['address']);
                        $marker_lat_lng = explode(',', $marker['coordinates']);
                        $marker_lat = $marker_lat_lng[0];
                        $marker_lng = $marker_lat_lng[1];
                        $marker_icon = ($marker['marker_image'] != '')? $marker['marker_image'] : get_template_directory_uri().'/inc/flexible-content/sections/map/img/pin.png';
                        $popup_content = '<h6>'.fc_map_marker_prepare_content($marker['popup_title']).'</h6>'
                                        .'<p>'.fc_map_marker_prepare_content($marker['popup_desc']).'</p>';
                                        //.'<div class=\"route\">directions</div>'; // HTTPS is required for geolocation to work
                        $popup_width = '';
                        $popup_height = '';
                        ?>
                        {
                            address: "<?php echo $marker_address ?>",
                            lat: "<?php echo $marker_lat ?>",
                            lng: "<?php echo $marker_lng ?>",
                            icon: "<?php echo $marker_icon ?>",
                            content: "<?php echo $popup_content ?>",
                            popup_width: "<?php echo $popup_width ?>",
                            popup_height: "<?php echo $popup_height ?>",
                            category: "<?php echo $marker_cat ?>"
                        },
                    <?php endforeach; endif ?>
                ],
                center_on_markerclick: <?php echo $center_on_markerclick ?>,
                fit_to_markers: <?php echo $fit_to_markers ?>,
                routes: <?php echo $routes ?>,
                route_link_label: "<?php _e('Driving directions', 'tankenbak') ?>",
                clusters: <?php echo $clusters ?>
            };
            //console.log(map_options);
            fc_map_init(map_options);
        });
    </script>
</section>
<?php
    //var_dump($template_data);
?>