jQuery(document).ready(function($) {

    function add_geolocation_button(address_input) {
        if(address_input.parent().find('.get_lat_lng_button').length == 0) {
            address_input.parent().append('<a href="#" class="get_lat_lng_button button button-primary">Get coordinates</a><img style="display:none;" src="/wp-admin/images/loading.gif">');
        }
    }

    // setup coordinates button on page load
    $('.acf-field.marker_address .acf-input-wrap input[type="text"]').each(function() {
        add_geolocation_button($(this));
    });

    // setup coordinates button when new section is being added
    if($('.acf-flexible-content').length > 0) {
        acf.add_action('append', function( $el ){
            if($el.find('.marker_address').length > 0) {
                add_geolocation_button($el.find('.marker_address .acf-input-wrap input[type="text"]'));
            }
        });
    }

    // geocode address
    $(document).on('click', '.get_lat_lng_button', function(){
        var t = $(this),
            geocoder = new google.maps.Geocoder(),
            address = t.prev().val(),
            lat_lng_input = t.closest('.acf-row').find('.marker_lat_lng input[type="text"]');
        t.next().show();
        geocoder.geocode({ 'address': address }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var lat = results[0].geometry.location.lat(),
                    lng = results[0].geometry.location.lng();
                lat_lng_input.val(lat+','+lng);
            } else {
                lat_lng_input.val('');
                alert('Coordinates could not be retrived for given address');
            }
            t.next().hide();
        });
    });

});