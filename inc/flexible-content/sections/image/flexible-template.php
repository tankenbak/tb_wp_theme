<?php
$class = 'pricture_demension ';
global $option;
// amount of cols for display
$data_section = [];
$image = null;
$class_align = '';
$wide_container = get_field('wide_content');
switch ( $template_data['proportion'] ) {
    case 'auto':
        $class = '';
        $image = wp_get_attachment_image( $template_data['ti_image'] ,$template_data['sizes'] );
        break;
    case '4x3':
        $class .= ' pricture_demension--4x3';
    case '3x4':
        $class .= ' pricture_demension--3x4';
    case '16x9':
        $class .= ' pricture_demension--16x9';
    case '1x1':
        $class .= ' pricture_demension--1x1';
        break;
}
switch ( $template_data['aligment'] ) {
    case 'left':
        $class_align = 'text-left';
        break;
    case 'right':
        $class_align = 'text-right';
        break;
    case 'center':
        $class_align = 'text-center';
        break;
}
if ( !$image) {
    $image_data = wp_get_attachment_image_src( $template_data['ti_image'] ,$template_data['sizes'] );
    $image = '<div class="'.$class.'">';
    $image .= '<div class="img-covered" style="background-image: url('.$image_data[0].') ">';
    $image .= '</div>';
    $image .= '<noscript>'.wp_get_attachment_image( $template_data['ti_image'] ,$template_data['sizes'] ).'</noscript>';
}
?>
<?php
if ( isset($template_data['section_id']) ):
    echo get_template_part_flexible( 'template-parts/section', 'anchorn', $template_data['section_id'] );
endif
?>
<?php echo get_template_part_flexible( 'template-parts/before', 'section', $template_data ) ?>
    <?php if ($wide_container): ?>
    <div class="container">
        <div class="row">
        <div class="col-xs-12 ">
    <?php endif ?>

            <div class="<?php echo $class_align ?>">
                <?php echo $image; ?>
            </div>
    <?php if ($wide_container): ?>
        </div>
        </div>
    </div>
    <?php endif ?>
<?php echo get_template_part_flexible( 'template-parts/after', 'section', $template_data ) ?>