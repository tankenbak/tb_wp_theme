<div class="flexible-content fc-mailchimp <?php
    if($template_data['bottom_spacing'] === '0') echo 'no-spacing';
    if($template_data['setion_width'] != 'full_width') echo ' container';
    ?>" style="background:<?php echo $template_data['background_color']; ?>;">
    <?php
    if($template_data['background_image'] != '') {
        lazy_thumb(array(
            'id' => $template_data['background_image'],
            'proportion' => 'none',
            'size' => array(
                  'big'       => 'tb-extra_large',
                  'medium'    => 'tb-large',
                  'small'     => 'tb-medium',
              )
        ));
        $opacity =  (int)$template_data['background_overlay_opacity'] / 100;
        echo '<div class="overlay" style="opacity: '.$opacity.'; -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=#{'.$opacity.' * 100})";"></div>';
    }
    ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12" style="color:<?php echo $template_data['text_color'] ?>">
                <?php if($template_data['heading'] != ''): ?>
                    <h4 style="color:<?php echo $template_data['text_color'] ?>"><?php echo $template_data['heading'] ?></h4>
                <?php endif ?>
                <?php if($template_data['description'] != ''): ?>
                    <p style="color:<?php echo $template_data['text_color'] ?>"><?php echo $template_data['description'] ?></p>
                <?php endif ?>
                <form method="post" class="fc-mailchimp-form">
                    <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('blue-dragon') ?>">
                    <input type="hidden" name="list_id" value="<?php echo $template_data['list_id'] ?>">
                    <div class="inputs">
                        <div class="mc-name">
                            <input style="border: 4px solid <?php echo $template_data['text_color'] ?>; color:<?php echo $template_data['text_color'] ?>" type="text" name="name" value="" placeholder="<?php _e('Name', 'tankenbak') ?>"/>
                        </div>
                        <div class="mc-email">
                            <input style="border: 4px solid <?php echo $template_data['text_color'] ?>; color:<?php echo $template_data['text_color'] ?>" type="text" name="email" value="" placeholder="<?php _e('Email', 'tankenbak') ?>"/>
                        </div>
                        <div class="mc-submit">
                            <input style="border: 4px solid <?php echo $template_data['text_color'] ?>; color:<?php echo $template_data['text_color'] ?>" type="submit" value="<?php _e('Sign up', 'tankenbak') ?>"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
    //var_dump($template_data);
?>