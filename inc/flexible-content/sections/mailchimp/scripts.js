jQuery(document).ready(function($) {

    $('.fc-mailchimp-form').on('submit', function() {
        var form = $(this);

        if(form.hasClass('working')) {
            return false;
        }

        //some validation
        $('.mc-name, .mc-email').removeClass('error');
        if(form.find('.mc-name input').val() == '') form.find('.mc-name').addClass('error');
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(form.find('.mc-email input').val())) form.find('.mc-email').addClass('error');

        //if there are no errors
        if(form.find('.error').length == 0) {
            form.addClass('working');
            var request = $.ajax({
                method: 'POST',
                url: theme_data.ajaxurl,
                data: {
                    action: 'submit_mc_form',
                    form_data: form.serialize(),
                }
            });
            request.done(function(data) {
                var response = JSON.parse(data);
                form.removeClass('working');
                swal({
                    title: response.title,
                    text: response.description,
                    type: response.type,
                    confirmButtonText: response.close_popup_button
                });
                if(response.type == 'success') {
                    form.find('.mc-name input').val('');
                    form.find('.mc-email input').val('');
                }
            });
            request.fail(function( jqXHR, textStatus ) {
                console.log(jqXHR);
                console.log(textStatus);
            });
        }
        return false;
    });
});