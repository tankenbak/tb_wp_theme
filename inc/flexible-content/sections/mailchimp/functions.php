<?php
// Image slaider based on owl carousel
if ( ! defined( 'ABSPATH' ) ) exit; // we dont like direct access



function submit_mc_form() {
    parse_str($_POST['form_data'], $form_data);

    //check inputs
    if(empty($form_data)
        || !isset($form_data['nonce'])
        || !isset($form_data['name'])
        || !isset($form_data['email'])
        || !isset($form_data['list_id'])
    ) {
        $response = array(
            'type' => 'error',
            'title' => __('Error', 'wpvakt'),
            'description' => __('Form data corrupted', 'wpvakt'),
            'close_popup_button' => __('Close', 'wpvakt'),
        );
        echo json_encode($response);
        die();
    }
    // verify nonce
    if(!wp_verify_nonce($form_data['nonce'], 'blue-dragon')) {
        $response = array(
            'type' => 'error',
            'title' => __('Error', 'wpvakt'),
            'description' => __('Form nonce corrupted', 'wpvakt'),
            'close_popup_button' => __('Close', 'wpvakt'),
        );
        echo json_encode($response);
        die();
    }
    // validate list ID
    $list_id = htmlspecialchars(stripslashes(trim($form_data['list_id'])));
    if($list_id == '') {
        $response = array(
            'type' => 'error',
            'title' => __('Error', 'wpvakt'),
            'description' => __('Invalid list ID', 'wpvakt'),
            'close_popup_button' => __('Close', 'wpvakt'),
        );
        echo json_encode($response);
        die();
    }
    // validate name
    $name = htmlspecialchars(stripslashes(trim($form_data['name'])));
    if($name == '') {
        $response = array(
            'type' => 'error',
            'title' => __('Error', 'wpvakt'),
            'description' => __('Please provide your name.', 'wpvakt'),
            'close_popup_button' => __('Close', 'wpvakt'),
        );
        echo json_encode($response);
        die();
    }
    // validate email
    $email = htmlspecialchars(stripslashes(trim($form_data['email'])));
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response = array(
            'type' => 'error',
            'title' => __('Error', 'wpvakt'),
            'description' => __('Please provide valid email address', 'wpvakt'),
            'close_popup_button' => __('Close', 'wpvakt'),
        );
        echo json_encode($response);
        die();
    }
    //signup to mailchimp list
    $apiKey = 'a8d099d81fb90ebb7cae0d33663ce488-us12';
    $listId = $list_id;
    $dataCenter = explode('-',$apiKey)[1];
    $memberId = md5(strtolower($email));
    $url = 'https://'.$dataCenter.'.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;
    $json = json_encode([
        'email_address' => $email,
        'status'        => 'subscribed', // "subscribed","unsubscribed","cleaned","pending"
        'merge_fields'  => [
            'FNAME'     => $name,
        ]
    ]);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); // PUT | DELETE
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    $result = json_decode(curl_exec($ch));
    //echo "<pre>"; print_r($result); echo "</pre>";
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if($httpCode != 200) {
        $response = array(
            'type' => 'error',
            'title' => __('Error', 'wpvakt'),
            'description' => __('Try again', 'tankenbak'),
            'close_popup_button' => __('Close', 'wpvakt'),
        );
        echo json_encode($response);
        die();
    }
    if(!isset($result->status)) {
        $response = array(
            'type' => 'error',
            'title' => __('Error', 'wpvakt'),
            'description' => __('Could not get the signup status - try again', 'tankenbak'),
            'close_popup_button' => __('Close', 'wpvakt'),
        );
        echo json_encode($response);
        die();
    }
    if($result->status != 'subscribed') {
        $response = array(
            'type' => 'error',
            'title' => __('Error', 'wpvakt'),
            'description' => __('Wrong signup status - try again', 'tankenbak'),
            'close_popup_button' => __('Close', 'wpvakt'),
        );
        echo json_encode($response);
        die();
    }

    $response = array(
        'type' => 'success',
        'title' => __('Signup successful', 'wpvakt'),
        'description' => __('Please check your email to activate your subscription', 'tankenbak'),
        'close_popup_button' => __('Close', 'wpvakt'),
    );
    echo json_encode($response);
    die();
}
add_action( 'wp_ajax_nopriv_submit_mc_form', 'submit_mc_form' );
add_action( 'wp_ajax_submit_mc_form', 'submit_mc_form' );