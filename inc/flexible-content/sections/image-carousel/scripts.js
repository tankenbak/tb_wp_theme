/*******************************************************/
// This file is used to merage all flexible content JS
// File is used for frontend - file meraged in flexible.js
// This fiquired bLazy load and Owl Carousel
/*******************************************************/

jQuery(document).ready(function($) {

    $('.fc-carousel').each(function() {
        var owlContainer = $(this).find('.owl-carousel');
        var margin  = parseInt( owlContainer.attr('data-margin') ),
            loop    = parseInt( owlContainer.attr('data-loop') ),
            items   = parseInt( owlContainer.attr('data-items') );
        owlContainer.owlCarousel({
            loop: loop,
            margin: margin,
            nav:true,
            items: items,
            dots: false,
            fluidSpeed: 5,
            navText: ['<i class="material-icons">keyboard_arrow_left</i>','<i class="material-icons">keyboard_arrow_right</i>'],
            responsive : {
                0 : {
                    items:2,
                },
                620 : {
                    items: (items-1),
                },
                992 : {
                    items:items,
                },
            },
            onTranslate: function(){
            },
            onInitialized: function (){
            },
        });
    });

});