<?php

    $images = $template_data['gallery_images'];
    //var_dump( $images );
    //var_dump( $template_data );
    $gallert_id = uniqid();
    $images_data = null;
    $images_string = '';
    $allery_items = count($images);
    foreach ($images as $image){
        $images_string .=  $image['id'].',';
    }

    $columns = '3';
    if (isset( $template_data['gallery_images_cols'] ) && $template_data['gallery_images_cols'] ) {
        $columns = $template_data['gallery_images_cols'];
    }
    //var_dump( $images_data );
    //data-open_text="<?php _e('Open Source', 'tankenbak')
?>
<?php
if ( isset($template_data['section_id']) ):
    echo get_template_part_flexible( 'template-parts/section', 'anchorn', $template_data['section_id'] );
endif
?>
<?php echo get_template_part_flexible( 'template-parts/before', 'section', $template_data ) ?>
<div role="gallery" id="gallery-<?php echo $gallert_id ?>" data-gallery_id="<?php echo $gallert_id ?>" data-nounce="<?php echo  wp_create_nonce("pizza_pizza_pie"); ?>" data-images="<?php echo $images_string ?>" class="flexible-content fc-gallery <?php
        if($template_data['setion_width'] === 'full_width') echo ' full-width';
        if($template_data['images_spacing'] === '1') echo ' image-spacing'
    ?>" >

<?php echo '[gallery wide="true" type="carousel" link="file" size="tb-medium" columns="'.$columns.'" ids="'.$images_string.'"]'; ?>

</div>
<?php echo get_template_part_flexible( 'template-parts/after', 'section', $template_data ) ?>
