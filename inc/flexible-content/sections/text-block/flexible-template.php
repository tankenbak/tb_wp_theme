<?php
$class = 'col-xs-12';
$content = [];
// amount of cols for display
switch ( $template_data['columns'] ) {
    case '0':
        // dont need to add class
        $content[] = $template_data['content_0'];
        break;
    case '1':
        $class .= ' col-sm-6';
        $content[] = $template_data['content_0'];
        $content[] = $template_data['content_1'];
        break;
    case '2':
        $class .= ' col-sm-4';
        $content[] = $template_data['content_0'];
        $content[] = $template_data['content_1'];
        $content[] = $template_data['content_3'];
        break;
}
?>

<?php
if ( isset($template_data['section_id']) ):
    echo get_template_part_flexible( 'template-parts/section', 'anchorn', $template_data['section_id'] );
endif;
?>

<?php echo get_template_part_flexible( 'template-parts/before', 'section', $template_data ) ?>
    <div class="container">
        <div class="row">
         <?php foreach ($content as $data_content): ?>
            <article class="<?php echo $class ?>">
                <?php echo wpautop( $data_content );  ?>
            </article>
         <?php endforeach ?>
        </div>
<?php echo get_template_part_flexible( 'template-parts/after', 'section', $template_data ) ?>