<?php
// Image slaider based on owl carousel
if ( ! defined( 'ABSPATH' ) ) exit; // we dont like direct access
$section_name = 'text-block';
$section_name_key = 'sec_'.$section_name;

$section_prefix = 'txt_';
//Standard advenced keys
$general_tab = $section_prefix .'get_tab124ds';
$advenced_tab = $section_prefix .'adv_tab23w5';
//$bg_color_key_id = 'ufgnfdu';
//$text_theme_key_id = 'zx52efes';
$pad_bot_key_id = $section_prefix .'pad_bot_a2sda';
$pad_top_key_id = $section_prefix .'pad_top_asf3';
$section_id_key = $section_prefix .'sec_id_3237';

return array (
    'key' => $section_name_key,
    'name' => $section_name,
    'label' => __('Text block', 'tankenbak'),
    'display' => 'block', //block / row / table
    'sub_fields' => array (
        array (
            'key' => 's_text-block_columns',
            'label' => __('Text layout', 'tankenbak'),
            'name' => 'columns',
            'type' => 'radio',
            'instructions' => __('Content can be split to 1, 2 or 3 columns', 'tankenbak'),
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'choices' => array (
                0 => __('1 column', 'tankenbak'),
                1 => __('2 columns', 'tankenbak'),
                2 => __('3 columns', 'tankenbak'),
            ),
            'allow_null' => 0,
            'other_choice' => 0,
            'save_other_choice' => 0,
            'default_value' => 0,
            'layout' => 'horizontal',
        ),
        array (
            'key' => 's_text-block_col_1',
            'label' => __('Column 1', 'tankenbak'),
            'name' => 'col_1',
            'type' => 'tab',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'placement' => 'top',
            'endpoint' => 1,
        ),
        array (
            'key' => 's_text-block_content_0',
            'label' => __('Content', 'tankenbak'),
            'name' => 'content_0',
            'type' => 'wysiwyg',
            'instructions' => __('Content for column 1', 'tankenbak'),
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'full',
            'media_upload' => 1,
        ),
        array (
            'key' => 's_text-block_col_2',
            'label' => __('Column 2', 'tankenbak'),
            'name' => 'col_2',
            'type' => 'tab',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => array (
                array (
                    array (
                        'field' => 's_text-block_columns',
                        'operator' => '==',
                        'value' => '1',
                    ),
                ),
                array (
                    array (
                        'field' => 's_text-block_columns',
                        'operator' => '==',
                        'value' => '2',
                    ),
                ),
            ),
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'placement' => 'top',
            'endpoint' => 0,
        ),
        array (
            'key' => 's_text-block_content_1',
            'label' => __('Content', 'tankenbak'),
            'name' => 'content_1',
            'type' => 'wysiwyg',
            'instructions' => __('Content for column 2', 'tankenbak'),
            'required' => 0,
            'conditional_logic' => array (
                array (
                    array (
                        'field' => 's_text-block_columns',
                        'operator' => '==',
                        'value' => '1',
                    ),
                ),
                array (
                    array (
                        'field' => 's_text-block_columns',
                        'operator' => '==',
                        'value' => '2',
                    ),
                ),
            ),
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'full',
            'media_upload' => 1,
        ),
        array (
            'key' => 's_text-block_col_3',
            'label' => __('Column 3', 'tankenbak'),
            'name' => 'col_3',
            'type' => 'tab',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => array (
                array (
                    array (
                        'field' => 's_text-block_columns',
                        'operator' => '==',
                        'value' => '2',
                    ),
                ),
            ),
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'placement' => 'top',
            'endpoint' => 0,
        ),
        array (
            'key' => 's_text-block_content_2',
            'label' => __('Content', 'tankenbak'),
            'name' => 'content_2',
            'type' => 'wysiwyg',
            'instructions' => __('Content for column 3', 'tankenbak'),
            'required' => 0,
            'conditional_logic' => array (
                array (
                    array (
                        'field' => 's_text-block_columns',
                        'operator' => '==',
                        'value' => '2',
                    ),
                ),
            ),
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'full',
            'media_upload' => 1,
        ),
        array (
            'key' => $advenced_tab,
            'label' => __('Advanced settings', 'tankenbak'),
            'name' => 'tab_advanced',
            'type' => 'tab',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'placement' => 'top',
            'endpoint' => 0,
        ),
        array (
            'key' => 'field_'.$section_id_key,
            'label' => __('Section ID', 'tankenbak'),
            'name' => 'section_id',
            'type' => 'text',
            'instructions' => __('Dont use space, as space use "-", normal spaces will be transformed into "-". This ID is used to anhorn, if you dont want add anchor on page, just leave this field empty', 'tankenbak'),
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => 'section-id',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),
        array (
            'key' => 'field_'.$pad_bot_key_id,
            'label' => __('Padding Bottom', 'tankenbak'),
            'name' => 'padding_bottom',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '50',
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 1,
            'ui' => 1,
            'ui_on_text' => 'Yes',
            'ui_off_text' => 'No',
        ),
        array (
            'key' => 'field_'.$pad_top_key_id,
            'label' => __('Padding Top', 'tankenbak'),
            'name' => 'padding_top',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '50',
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 1,
            'ui' => 1,
            'ui_on_text' => 'Yes',
            'ui_off_text' => 'No',
        ),
    ),
    'min' => '',
    'max' => '',
);