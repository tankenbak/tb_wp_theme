<?php
/**
 * Posts section
 * v 1.0
 */
?>
<div class="flexible-content fc-posts">
    <div class="container">
        <div class="fc-section-heading">
            <h3><?php echo $template_data['heading'] ?></h3>
        </div>
        <?php
        $args = array(
            'post_type' => 'product',
            'taxonomy' => 'product_cat',
            'term' => $template_data['posts_term'],
            'theme' => $template_data['displa_as'],
            'per_load' => $template_data['per_load'],
        );
        fc_posts($args);
        ?>
    </div>
</div>
<?php
    var_dump($template_data);
?>