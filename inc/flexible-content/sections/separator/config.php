<?php
// Image slaider based on owl carousel
if ( ! defined( 'ABSPATH' ) ) exit; // we dont like direct access

return array (
    'key' => 'e7c52b80e6fff',
    'name' => 'separator',
    'label' => __('Separator', 'tankenbak'),
    'display' => 'table', //block / row / table
    'sub_fields' => array (
        // Number
        array (
            'key' => 'field_57d24b9e6955f',
            'label' => 'Height',
            'name' => 'height',
            'type' => 'number',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '50',
            'placeholder' => '',
            'prepend' => '',
            'append' => 'px',
            'min' => '',
            'max' => '',
            'step' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),
        // color picker
        array (
            'key' => 'field_57d2wd4be956c',
            'label' => 'Background color',
            'name' => 'background_color',
            'type' => 'color_picker',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '#ffffff',
        ),
    ),
    'min' => '',
    'max' => '',
);