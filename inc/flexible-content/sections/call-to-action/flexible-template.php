<?php
    if($template_data['setion_width'] == 'boxed') {
        $wrapper_start = '<div class="container">';
        $wrapper_end = '</div>';
    } else {
        $wrapper_start = '';
        $wrapper_end = '';
    }
?>

<?php
if ( isset($template_data['section_id']) ):
    echo get_template_part_flexible( 'template-parts/section', 'anchorn', $template_data['section_id'] );
endif;
?>

<?php echo get_template_part_flexible( 'template-parts/before', 'section', $template_data ) ?>
    <?php echo $wrapper_start ?>
    <div class="inner" style="background:<?php echo $template_data['background_color'] ?>;">
        <?php
        if($template_data['background_image'] != '') {
            lazy_thumb(array(
                'id' => $template_data['background_image'],
                'proportion' => 'none',
                'size' => array(
                      'big'       => 'tb-extra_large',
                      'medium'    => 'tb-large',
                      'small'     => 'tb-medium',
                  )
            ));
            $opacity =  (int)$template_data['background_overlay_opacity'] / 100;
            echo '<div class="overlay" style="opacity: '.$opacity.'; -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=#{'.$opacity.' * 100})";"></div>';
        }
        ?>
        <div class="container">
            <div class="row">
                <div class="col-xs-12" style="color:<?php echo $template_data['text_color'] ?>">
                    <?php if($template_data['heading'] != ''): ?>
                        <h4 style="color:<?php echo $template_data['text_color'] ?>"><?php echo $template_data['heading'] ?></h4>
                    <?php endif ?>
                    <?php if($template_data['description'] != ''): ?>
                        <p style="color:<?php echo $template_data['text_color'] ?>"><?php echo $template_data['description'] ?></p>
                    <?php endif ?>
                    <?php if(isset($template_data['button']['title']) && $template_data['button']['title'] != ''): ?>
                        <a style="color:<?php echo $template_data['text_color'] ?>" class="fc-call-to-action-btutton" style="border-color:<?php echo $template_data['text_color'] ?>" href="<?php echo $template_data['button']['url'] ?>" target="<?php echo $template_data['button']['target'] ?>"><?php echo $template_data['button']['title'] ?></a>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <?php echo $wrapper_end ?>
<?php echo get_template_part_flexible( 'template-parts/after', 'section', $template_data ) ?>