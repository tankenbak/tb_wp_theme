jQuery(document).ready(function($) {

//setup for full screen slider
    if($('.fc-slider-full_screen').length > 0) {
        // Slider full screen - setup
        var slider = $('.fc-slider-full_screen').first(),
            window_h = $(window).height(),
            slider_offset = slider.offset(),
            off = ($('#masthead').hasClass('fixed'))? $('#masthead').outerHeight() : 0;

        slider.find('.fc-slide').css({
            'height': window_h - off,
        });

        // setup owl
        $('.fc-slider-container').owlCarousel({
            items:1,
            loop: 0,
            nav: 1,
            navText: ['<i class="material-icons">keyboard_arrow_left</i>','<i class="material-icons">keyboard_arrow_right</i>'],
        });

        $.extend( $.easing, {
            fc_slider_smooth: function (x, t, b, c, d) {
                if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
                return -c/2 * ((t-=2)*t*t*t - 2) + b;
            }
        });

        $('.fc-slider-scroll-down').on('click', function() {
            $('html,body').animate({
                scrollTop: $(window).height() + slider.offset().top
            }, 1000, 'fc_slider_smooth');
        });

        var resized;
        $(window).resize(function() {
            clearTimeout(resized);
            var wh = $(window).height();
            if(wh <= 768) return false;
            resized = setTimeout(function() {
                off = ($('#masthead').hasClass('fixed'))? $('#masthead').outerHeight() : 0;
                slider.find('.fc-slide').css({
                    'height':  wh - off,
                });
            }, 200);
        });

    } else {
        // Slider auto height - setup
        $('.fc-slider-container').owlCarousel({
            items:1,
            loop: 0,
            nav: 1,
            navText: ['<i class="material-icons">keyboard_arrow_left</i>','<i class="material-icons">keyboard_arrow_right</i>'],
            autoHeight: 1,
        });
    }

});