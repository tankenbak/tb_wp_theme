<?php
/**
 * Slider section
 * v 1.0
 */
?>
<div class="flexible-content fc-slider <?php
        if($template_data['bottom_spacing'] === '0') echo 'no-spacing';
        if($template_data['setion_width'] != 'full_width') echo ' container';
    ?>">
    <div class="owl-carousel fc-slider-container fc-slider-<?php echo $template_data['slider_height'] ?>">
        <?php if(!empty($template_data['slides'])) : foreach ($template_data['slides'] as $slide) : ?>
            <?php
            $image = wp_prepare_attachment_for_js($slide['background_image']);
            $link_type = $slide['link_type'];
            ?>
            <div class="fc-slide <?php if($link_type == 'whole_slide') echo 'whole_slide_link' ?>" <?php if($link_type == 'whole_slide') echo 'onclick="window.location.href = \''.$slide['link']['url'].'\';"' ?>>
                <div class="fc-slide-image">
                    <?php
                    lazy_thumb(array(
                        'id' => $image['id'],
                        'proportion' => 'none',
                        'size'     => array(
                              'big'       => 'tb-extra_large',
                              'medium'    => 'tb-large',
                              'small'     => 'tb-medium',
                          )
                    ));
                    ?>
                </div>
                <?php
                    $opacity =  (int)$slide['background_overlay_opacity'] / 100;
                    ?>
                    <div class="overlay" style="opacity: <?php echo $opacity?>; -ms-filter: 'progid:DXImageTransform.Microsoft.Alpha(Opacity=<?php echo $opacity * 100 ?>)'; filter: alpha(opacity=<?php echo $opacity * 100 ?> )"></div>';
                <div class="fc-slide-content container">
                    <div class="fc-slide-content-inner col-xs-12" style="text-align:<?php echo $slide['content_align'] ?>">
                        <?php
                        if($slide['title'] !== '') echo '<h2>'.$slide['title'].'</h2>';
                        if($slide['description'] !== '') echo '<p>'.$slide['description'].'</p>';
                        if($slide['link_type'] == 'button') echo '<a class="fc-slide-button" href="'.$slide['link']['url'].'" title="'.$slide['link']['title'].'" target="'.$slide['link']['target'].'">'.$slide['link']['title'].'</a>';
                        ?>
                    </div>
                </div>
            </div>
        <?php endforeach; endif ?>
    </div>
    <?php if($template_data['slider_height'] == 'full_screen') : ?>
    <div class="fc-slider-scroll-down"><span></span></div>
    <?php endif ?>
</div>
<?php
    //var_dump($template_data);
?>