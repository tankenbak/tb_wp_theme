<?php

function wpdocs_register_my_custom_submenu_page() {
    add_submenu_page(
        'themes.php',
        __( 'Flexible layouts', 'tankenbak' ),
        __( 'Flexible layouts', 'tankenbak' ),
        'publish_pages',
        'edit.php?post_type=flexible_sections'
    );
}
add_action('admin_menu', 'wpdocs_register_my_custom_submenu_page');
function flexible_sections_cpt_init() {
    $labels = array(
        'name'               => _x( 'Flexible layouts', 'post type general name', 'tankenbak' ),
        'singular_name'      => _x( 'Flexible layout', 'post type singular name', 'tankenbak' ),
        'menu_name'          => _x( 'Flexible layouts', 'admin menu', 'tankenbak' ),
        'name_admin_bar'     => _x( 'Flexible layout', 'add new on admin bar', 'tankenbak' ),
        'add_new'            => _x( 'Add new', 'book', 'tankenbak' ),
        'add_new_item'       => __( 'Add new flexible layout', 'tankenbak' ),
        'new_item'           => __( 'New flexible layouts', 'tankenbak' ),
        'edit_item'          => __( 'Edit flexible layout', 'tankenbak' ),
        'view_item'          => __( 'View flexible layout', 'tankenbak' ),
        'all_items'          => __( 'All flexible layouts', 'tankenbak' ),
        'search_items'       => __( 'Search flexible layouts', 'tankenbak' ),
        'parent_item_colon'  => __( 'Parent flexible layouts:', 'tankenbak' ),
        'not_found'          => __( 'No flexible layouts found.', 'tankenbak' ),
        'not_found_in_trash' => __( 'No flexible layouts found in Trash.', 'tankenbak' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( '', 'tankenbak' ),
        'public'             => true,
        'publicly_queryable' => true,
        'exclude_from_search'=> true,
        'show_ui'            => true,
        'show_in_menu'       => false,
        'show_in_nav_menus'  => false,
        'show_in_admin_bar'  => false,
        'query_var'          => 'flexible_sections',
        'rewrite'            => array( 'slug' => 'flexible_section' ),
        'capability_type'    => 'page',
        'has_archive'        => false,
        'hierarchical'       => true,
        'menu_position'      => null,
        'supports'           => array( 'title' )
    );
    register_post_type( 'flexible_sections', $args );
}
add_action( 'init', 'flexible_sections_cpt_init' );