<?php
// Image slaider based on owl carousel
if ( ! defined( 'ABSPATH' ) ) exit; // we dont like direct access

return array (
    'key' => 's_flexible-layout',
    'name' => 'flexible-layout',
    'label' => __('Flexible layout', 'tankenbak'),
    'display' => 'block', //block / row / table
    'sub_fields' => array (
        array (
        'key' => 's_flexible-layout_layout',
        'label' => __('Select layout', 'tankenbak'),
        'name' => 'layout',
        'type' => 'post_object',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array (
            'width' => '',
            'class' => '',
            'id' => '',
        ),
        'post_type' => array (
            0 => 'flexible_sections',
        ),
        'taxonomy' => array (
        ),
        'allow_null' => 0,
        'multiple' => 0,
        'return_format' => 'id',
        'ui' => 1,
    ),
    ),
    'min' => '',
    'max' => '',
);