<?php
    if($template_data['layout'] != '') {
        $flex_content_data = get_field( 'content', $template_data['layout'] );
        if(!empty($flex_content_data)) {
            foreach ( $flex_content_data as $layout) {
                get_template_part_flexible( 'sections/'.$layout['acf_fc_layout'].'/flexible', 'template', $layout );
            }
        }
    }
    //var_dump($template_data);
?>