<?php
// Image slaider based on owl carousel
if ( ! defined( 'ABSPATH' ) ) exit; // we dont like direct access

$section_name = 'accordion';
$section_name_key = 'sec_'.$section_name;


$section_prefix = 'acco_';
//Standard advenced keys
$general_tab = $section_prefix .'get_tab124ds';
$advenced_tab = $section_prefix .'adv_tab23w5';
//$bg_color_key_id = 'ufgnfdu';
//$text_theme_key_id = 'zx52efes';
$pad_bot_key_id = $section_prefix .'pad_bot_a2sda';
$pad_top_key_id = $section_prefix .'pad_top_asf3';
$section_id_key = $section_prefix .'sec_id_3237';
return array (
    'key' => $section_name_key,
    'name' => $section_name,
    'label' => __('Accordion', 'tankenbak'),
    'display' => 'block', //block / row / table
    'sub_fields' => array (
        array (
            'key' => 'field_'.$general_tab,
            'label' => __('General', 'tankenbak'),
            'name' => '',
            'type' => 'tab',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'placement' => 'top',
            'endpoint' => 0,
        ),
        array (
            'key' => 's_accordion_items',
            'label' => __('Sections', 'tankenbak'),
            'name' => 'items',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => 'repeater-spacing',
                'id' => '',
            ),
            'collapsed' => 's_accordion_title',
            'min' => 1,
            'max' => 10,
            'layout' => 'block',
            'button_label' => __('Add accordion item', 'tankenbak'),
            'sub_fields' => array (
                array (
                    'key' => 's_accordion_title',
                    'label' => __('Title', 'tankenbak'),
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                array (
                    'key' => 's_accordion_content',
                    'label' => __('Content', 'tankenbak'),
                    'name' => 'content',
                    'type' => 'wysiwyg',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => 'mini_editor',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'tabs' => 'visual',
                    'toolbar' => 'basic',
                    'media_upload' => 0,
                ),
            ),
        ),
        array (
            'key' => 'field_'.$advenced_tab,
            'label' => __('Advenced', 'tankenbak'),
            'name' => '',
            'type' => 'tab',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'placement' => 'top',
            'endpoint' => 0,
        ),
        array (
            'key' => 'field_'.$section_id_key,
            'label' => __('Section ID', 'tankenbak'),
            'name' => 'section_id',
            'type' => 'text',
            'instructions' => __('Dont use space, as space use "-", normal spaces will be transformed into "-". This ID is used to anhorn, if you dont want add anchor on page, just leave this field empty', 'tankenbak'),
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => 'section-id',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),
        array (
            'key' => 'field_'.$pad_bot_key_id,
            'label' => __('Padding Bottom', 'tankenbak'),
            'name' => 'padding_bottom',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '50',
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 1,
            'ui' => 1,
            'ui_on_text' => 'Yes',
            'ui_off_text' => 'No',
        ),
        array (
            'key' => 'field_'.$pad_top_key_id,
            'label' => __('Padding Top', 'tankenbak'),
            'name' => 'padding_top',
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '50',
                'class' => '',
                'id' => '',
            ),
            'message' => '',
            'default_value' => 1,
            'ui' => 1,
            'ui_on_text' => 'Yes',
            'ui_off_text' => 'No',
        ),
    ),
    'min' => '',
    'max' => '',
);