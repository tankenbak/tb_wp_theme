<?php
    $panel_id = 'accordion_'.uniqid();
    $accordions = $template_data['items'];
?>

<?php
if ( isset($template_data['section_id']) ):
    echo get_template_part_flexible( 'template-parts/section', 'anchorn', $template_data['section_id'] );
endif;
?>

<?php echo get_template_part_flexible( 'template-parts/before', 'section', $template_data ) ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel-group" id="<?php echo $panel_id; ?>">
                <?php
                $i = 0;
                foreach ($accordions as $accordion):
                    $href = sanitize_title( $accordion['title'] );
                    //in case you need open first as start
                    // if ( $i == 0) {
                    //     $class = 'in';
                    //     $aria = 'aria-expanded="true"';
                    // }else{
                    //     $class = null;
                    //     $aria = 'aria-expanded="false"';
                    // }
                    $class = null;
                    $aria = 'aria-expanded="false"';
                ?>
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" class="collapsed" data-parent="#<?php echo $panel_id; ?>" href="#<?php echo $href ?>">
                        <?php echo $accordion['title'] ?>
                            <div class="icon"></div>
                        </a>
                      </h4>
                    </div>
                    <div id="<?php echo $href ?>" class="panel-collapse collapse" <?php echo $class; echo $aria; ?> ">
                      <div class="panel-body">
                          <?php echo $accordion['content'] ?>
                      </div>
                    </div>
                  </div>
                <?php
                 $i++;
                endforeach
                ?>
                </div>

            </div>
        </div>
    </div>

<?php echo get_template_part_flexible( 'template-parts/after', 'section', $template_data ) ?>