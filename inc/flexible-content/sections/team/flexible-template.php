<?php
    //var_dump($template_data);
    $members = $template_data['members'];
?>
<section role="team-members" class="flexible-content fc-team">
    <div class="container">
        <div class="row equal-by-row">
            <?php foreach ($members as $key => $member): ?>
                <div class="equal col-xs-12 col-ms-6 col-sm-4 col-md-3">
                    <div class="team-member">
                       <?php
                            if ( isset($member['picture']) && $member['picture'] != null ) {
                              $args = [
                                  'id'    => $member['picture'],
                                  'preloader' => 'blur',
                                  'proportion' => '3x4',
                                  'size'       => array(
                                      'big'       => 'tb-small',
                                      'medium'    => 'tb-small',
                                      'small'     => 'tb-small'
                                  ),
                                  //'item_class'    => 'image-hover image-hover-zoom image-effect image-effect--special',
                              ];
                              lazy_thumb( $args );
                            }else{
                                ?>
                      <figure>
                        <div class="lazy-wrapper pricture_demension pricture_demension--3x4">
                          <div itemprop="thumbnail" style="background-image: url('<?php echo TB_URI ?>/inc/flexible-content/sections/team/img/no_profile_picture.jpg ')" class="b-blur b-loaded image-item"></div>
                        </div>
                      </figure>
                                <?php
                            }
                       ?>
                       <div class="member-data">
                           <h5 role="name" class="member-name"> <?php echo $member['name'] ?></h5>
                           <div class="job-title" > <?php echo $member['job_title'] ?> </div>
                           <div role="contact-info" class="contact-info">
                               <ul>
                               <?php if ( isset($member['phone']) && $member['phone'] != '' ): ?>
                                   <li><i class="material-icons">phone</i><a class="phone" href="tel:<?php echo $member['phone'] ?>"><?php echo $member['phone'] ?> </a></li>
                               <?php endif ?>
                               <?php if ( isset($member['email']) && $member['email'] != '' ): ?>
                                   <li><i class="material-icons">mail_outline</i><a class="email" href="mailto:<?php echo $member['email'] ?>"><?php echo $member['email'] ?> </a></li>
                               <?php endif ?>
                               </ul>
                           </div>
                       </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</section>