jQuery(document).ready(function($) {

    $('.fc-load-more').on('click', function(e) {
        e.preventDefault();
        var t = $(this),
            posts_container = t.prev(),
            post_type = t.attr('data-post_type'),
            taxonomy = t.attr('data-taxonomy'),
            term = t.attr('data-term'),
            theme = t.attr('data-theme'),
            per_load = parseInt(t.attr('data-per_load')),
            max_posts = t.attr('data-max_posts'),
            offset = posts_container.find('.fc-post').length;

        if(!t.hasClass('enabled') || t.hasClass('loading')) {
            return false;
        }

        t.addClass('loading');

        $.ajax({
            type: 'POST',
            url: theme_data.theme_ajax,
            //url: theme_data.ajaxurl,
            data: {
                action: 'builder_load_posts',
                post_type: post_type,
                taxonomy: taxonomy,
                term: term,
                theme: theme,
                per_load: per_load,
                offset: offset,
            },
            success: function(data, textStatus, jqXHR) {
                if ( posts_container.find('.fc-posts-ghosts') ) {
                    posts_container.find('.fc-posts-ghosts').remove();
                }
                posts_container.append(data);
                t.removeClass('loading');
                if(posts_container.find('.fc-post').length == max_posts) {
                    t.removeClass('enabled');
                }
                $('.fc-post.equal').matchHeight({
                    byRow: true,
                    property: 'height',
                });
            }
        });
    });

    /**
     * For performance reasons we are not showing posts on page load so...
     * lets trigger the "load more" button on load to get the first post package
     */
    $('.fc-load-more').trigger('click');

    $('.fc-posts-container.theme-carousel').owlCarousel({
        items:4,
        loop: 1,
        margin: 30,
        nav: 1,
        navText: ['<i class="material-icons">keyboard_arrow_left</i>','<i class="material-icons">keyboard_arrow_right</i>'],
        responsive : {
            0 : {
                items:1,
            },
            620 : {
                items:2,
            },
            992 : {
                items:3,
            },
            1170 : {
                items:4,
            },
        },
    });

});