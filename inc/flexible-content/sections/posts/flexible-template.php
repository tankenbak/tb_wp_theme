<?php
/**
 * Posts section
 * v 1.0
 */
?>
<div class="flexible-content fc-posts">
    <div class="container">
        <div class="fc-section-heading">
            <h3><?php echo $template_data['heading'] ?></h3>
        </div>
        <?php
        $posts_term = explode('|',$template_data['posts_term']);
        $taxonomy = $posts_term[0];
        $term = $posts_term[1];
        $args = array(
            'post_type' => $template_data['posts_post_type'],
            'taxonomy' => $taxonomy,
            'term' => $term,
            'theme' => $template_data['displa_as'],
            'per_load' => $template_data['per_load'],
        );
        fc_posts($args);
        ?>
    </div>
</div>
<?php
    //var_dump($template_data);
?>