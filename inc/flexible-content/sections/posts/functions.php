<?php
if ( ! defined( 'ABSPATH' ) ) exit; // we dont like direct access

/**
 * Helper that creates array with all term of all taxonomies for given post type
 */
function tbfc_get_post_type_terms_choices($post_type) {
    $choices = [];
    $taxonomies = get_object_taxonomies( $post_type, 'objects' );
    $ommit_taxonomies = array(
        'post_tag',
        'post_format',
    );

    foreach ($taxonomies as $tax => $tax_object) {
        if(!in_array($tax, $ommit_taxonomies)) {
            $choices[$tax.'|'] = $tax_object->labels->name.' '.__('(all)', 'tankenbak').' :';

            $terms = get_terms(array(
                'taxonomy' => $tax,
                'hide_empty' => false,
            ));

            if( !empty($terms) ) {
                foreach( $terms as $t ) {
                    $choices[$tax.'|'.$t->term_id] = ' - '.$t->name;
                }
            }

            //woocommerce only
            //if($tax == 'product_cat') {
            //    $choices['__meta___featured|yes'] = __('Featured products', 'tankenbak');
            //}
        }
    }
    return $choices;
}

/**
 * AJAX callback for generating term dropdown (on admin)
 */
function tbfc_get_post_type_term_choices() {
    $post_type = $_POST['post_type'];
    $choices = tbfc_get_post_type_terms_choices($post_type);
    //var_dump($choices);
    if(!empty($choices)) {
        foreach ($choices as $value => $label) {
            echo '<option value="'.$value.'">'.$label.'</option>';
        }
    } else {
        echo '<option value="">-</option>';
    }
    die();
}
add_action( 'wp_ajax_nopriv_get_post_type_term_choices', 'tbfc_get_post_type_term_choices' );
add_action( 'wp_ajax_get_post_type_term_choices', 'tbfc_get_post_type_term_choices' );

/**
 * Post layout builder based on query parameters
 */

function fc_post($post, $theme, $taxonomy='category') {
    //var_dump($post);
    switch($theme) {
        case 'grid' :
            $classes = 'col-lg-3 col-md-4 col-sm-6 col-ms-6 col-xs-12';
            $excerpt_length = 100;
            break;
        case 'list' :
            $classes = 'col-xs-12';
            $excerpt_length = 200;
            break;
        case 'carousel' :
            $classes = '';
            $excerpt_length = 100;
            break;
    }

    $product = ($post->post_type == 'product')? wc_get_product($post->ID) : false;

    ?>
    <article class="fc-post equal <?php echo $classes ?> theme-<?php echo $theme ?> fc-post-pt-<?php echo $post->post_type ?>">
        <?php echo edit_post_link(
            _x('Edit', 'Post edit link', 'tankenbak'),
            '',
            '',
            ///'<i class="post-edit-link material-icons featured-mark">mode_edit',
            ///'</i>',
            $post->ID
        ); ?>
        <a class="fc-post-image image-hover image-hover-link" href="<?php echo get_permalink($post->ID) ?>">
            <?php
            // custom marking for woocommerce featured products
            if($product && $product->is_featured()) {
                echo '<i class="material-icons featured-mark">star</i>';
            }
            lazy_thumb(array(
                'post_id' => $post->ID,
                'size'     => array(
                      'big'       => 'tb-small',
                      'medium'    => 'tb-small',
                      'small'     => 'tb-small',
                  )
            ));
            ?>
        </a>
        <main class="fc-post-info">
            <a class="fc-post-title" href="<?php echo get_permalink($post->ID) ?>">
                <h4><?php echo $post->post_title ?>
                    <?php
                    if ( $post->post_status != 'publish' ) {
                        echo "<small class='post-status'>({$post->post_status})</small>";
                    }
                    ?>
                </h4>
            </a>

            <p class="fc-post-excerpt"><?php echo custom_excerpt($post->post_content, $excerpt_length, ' [...]') ?></p>

            <?php if($post->post_type != 'product') : ?>
                <div class="fc-post-posted">
                    <?php
                    _e('Published', 'tankenbak');
                    echo date_i18n(' j M Y', strtotime($post->post_date));
                    //echo get_the_date( get_option('date_format'), $post->ID );
                    _e(' by', 'tankenbak');
                    echo ' '.get_user_by('id', $post->post_author)->display_name;
                    ?>
                </div>
            <?php endif ?>

            <?php if($post->post_type == 'product') : ?>
                <div class="fc-post-product-specific">
                    <?php echo do_shortcode('[add_to_cart id="'.$post->ID.'"]') ?>
                </div>
            <?php endif ?>

            <div class="fc-post-cats">
            <?php
                // note: we auto set taxonomy for products because query may ask for "featured products" (requires meta query and not tax query)
                // and in this case taxonomy wil not be a valid taxonomy name but rather '__meta__METAKEY' for meta query
                if($post->post_type == 'product') {
                    $taxonomy = 'product_cat';
                }
                _e('Categories: ', 'tankenbak');
                $cats = wp_get_object_terms($post->ID, $taxonomy);
                if(!empty($cats)) {
                    foreach ($cats as $c) {
                        echo '<a href="">'.$c->name.'</a>';
                    }
                }
                ?>
            </div>

        </main> <!-- fc-post-info -->
    </article>
    <?php
}

function fc_posts($args) {
    $defaults = array(
        'post_type' => 'post',
        'taxonomy' => 'category',
        'term' => '',
        'theme' => 'grid', // list, featured
        'per_load' => get_option( 'posts_per_page' ),
        'offset' => 0,
    );
    $fc_posts_args = wp_parse_args( $args, $defaults );
    extract($fc_posts_args);
    ?>
    <div class="fc-posts-container <?php if($theme == 'carousel') echo 'owl-carousel' ?> <?php if($theme != 'carousel') echo 'row' ?> theme-<?php echo $theme ?> equal-height-byrow">

        <?php
        $posts = fc_get_posts($args);

        if($theme !== 'carousel') :
            $ghost_styles = ['style-ghost-3', 'style-ghost-2', 'style-ghost-1', 'style-ghost-0'];
            echo "<div class='fc-posts-ghosts' >";
            for ( $i = 0 ; $i < $per_load; $i++) :
                ?>
                <div class="fc-post-ghost equal col-lg-3 col-md-4 col-sm-6 col-ms-6 col-xs-12 <?php echo $ghost_styles[array_rand($ghost_styles)] ?>" style="height: 321px;">
                        <a class="fc-post-image-ghost image-hover image-hover-link" >
                            <div class="pricture_demension pricture_demension--16x9"></div>
                        </a>
                        <div class="fc-post-info">
                            <a class="fc-post-title-ghost">
                                <h4></h4>
                            </a>
                            <div class="fc-post-excerpt-ghost">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="fc-posts-meta-ghost">

                            </div>

                        </div> <!-- fc-post-info -->
                    </div>
                <?php
            endfor;
            echo "</div>";
        endif;

        if($theme === 'carousel') { // posts in grid and list view are loaded by javascript "trigger click" on load more button
            if(!empty($posts->posts)) {
                foreach($posts->posts as $p) {
                    fc_post($p, $theme, $taxonomy);
                }
            }
        }
        ?>
    </div>
    <?php
    if($theme != 'carousel') :
        ?>
        <a href="#" <?php if($posts->found_posts <= $per_load) echo 'style="display:none"' ?>
                    class="fc-load-more enabled" data-post_type="<?php echo $post_type ?>"
                    data-taxonomy="<?php echo $taxonomy ?>"
                    data-term="<?php echo $term ?>"
                    data-theme="<?php echo $theme ?>"
                    data-per_load="<?php echo $per_load ?>"
                    data-max_posts="<?php echo $posts->found_posts ?>">
                <?php _e('Load more', 'tankenbak') ?>
            <i class="material-icons reload">refresh</i>
        </a>
        <?php
    endif;
}

function fc_get_posts($args) {
    $defaults = array(
        'post_type' => 'post',
        'taxonomy' => 'category',
        'term' => '',
        'theme' => 'grid', // list, featured
        'per_load' => get_option( 'posts_per_page' ),
        'offset' => 0,
    );
    $fc_posts_args = wp_parse_args( $args, $defaults );
    extract($fc_posts_args);
    $status = get_post_statues_for_query();
    // start building post query
    $query_args = array(
        'post_type' => $post_type,
        'posts_per_page' => $per_load,
        'offset' => $offset,
        'post_status' => 'publish',//$status,
    );
    // if posts from specific category
    if($taxonomy == 'category' && $term != '') {
        $query_args['cat'] = $term;
    }
    // if posts from specific taxonomy term
    // note: we also check if '__meta__' string is present in atxonomy name - if yes that means its a meta query and not tax query
    if($taxonomy != 'category' && $term != '' && strpos($taxonomy, '__meta__') === false) {
        $query_args['tax_query'] = array(
            array(
                'taxonomy' => $taxonomy,
                'field' => 'term_id',
                'terms' => $term,
            ),
        );
    }
    // if '__meta__' string is present in atxonomy name - if yes that means its a meta query and not tax query
    if(strpos($taxonomy, '__meta__') !== false) {
        $query_args['meta_key'] = str_replace('__meta__', '', $taxonomy);
        $query_args['meta_value'] = $term;
    }
    // get posts
    //var_dump($query_args);
    $posts =  new WP_Query($query_args);
    wp_reset_query();
    return $posts;
}


function builder_load_posts() {
    //$before = microtime(true);
    $status = get_post_statues_for_query();
    $args = array(
        'post_type' => $_POST['post_type'],
        'taxonomy' => $_POST['taxonomy'],
        'term' => $_POST['term'],
        'theme' => $_POST['theme'],
        'per_load' => $_POST['per_load'],
        'offset' => $_POST['offset'],
        'post_status' => $status,
    );
    $posts = fc_get_posts($args);
    if(!empty($posts->posts)) {
        foreach($posts->posts as $p) {
            fc_post($p, $args['theme'], $args['taxonomy']);
        }
    }
    //$after = microtime(true);
   // echo ($after-$before) . "  script\n";
    die();
}

add_action( 'wp_ajax_nopriv_builder_load_posts', 'builder_load_posts' );
add_action( 'wp_ajax_builder_load_posts', 'builder_load_posts' );


