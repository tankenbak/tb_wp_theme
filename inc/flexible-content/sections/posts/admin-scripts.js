jQuery(document).ready(function($) {

    // setup dynamic subfields on page load
    $('.values .layout[data-layout="posts"]').each(function() {
        var layout = $(this);
        setup_posts_subfields(layout);
    });

    // setup dynamic subfields when new section is being added
    if($('.acf-flexible-content').length > 0) {
        acf.add_action('append', function( $el ){
            setup_posts_subfields($el);
        });
    }

    // setup term dropdown when post type dropdown changes
    $(document).on('change', '.acf-field-select[data-name="posts_post_type"] select', function(){
        var layout = $(this).closest('.layout');
        layout.find('[data-name="posts_term"] input[type="text"]').val('');
        setup_posts_subfields(layout);
    });

    // store term dropdown value hidden text input
    $(document).on('change', '.posts_term_dropdown', function(){
        var t = $(this);
        t.prev().val(t.val());
    });

    // generate term dropdown based on active post type
    function setup_posts_subfields(layout) {
        var post_type_select = layout.find('.acf-field-select[data-name="posts_post_type"] select'),
            post_type = post_type_select.val(),
            term_input = layout.find('[data-name="posts_term"] input[type="text"]');

        // hide text input that holds term value and append empty select field that will be filled with options after ajax call
        if(layout.find('.posts_term_dropdown').length == 0) {
            term_input.hide();
            term_input.after('<select class="posts_term_dropdown" ></select>');
        }

        $.ajax({
            type: "POST",
            url: theme_data.ajaxurl,
            data: {
                action: 'get_post_type_term_choices',
                post_type: post_type
            },
            success: function(data, textStatus, jqXHR) {
                // add options to select
                layout.find('.posts_term_dropdown').html(data);
                if(term_input.val() == '') {
                    // if text input value is emapty set it to first available term oprion in term dropdown)
                    layout.find('.posts_term_dropdown option').first().prop("selected", true);
                    term_input.val(layout.find('.posts_term_dropdown option').first().attr('value'));
                } else {
                    // if text input value is NOT emapty set values on multi select dropdown
                    $.each(term_input.val().split(','), function(i, e){
                        layout.find('.posts_term_dropdown option[value="'+e+'"]').prop("selected", true);
                    });
                }
            },
        });
    }
});