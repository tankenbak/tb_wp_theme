<?php
    $files = $template_data['files'];
    $files_data = [];
    foreach ($files as $file) {
        if($file['file']) $files_data[] = wp_prepare_attachment_for_js( $file['file'] );
    }
?>
<?php
if ( isset($template_data['section_id']) ):
    echo get_template_part_flexible( 'template-parts/section', 'anchorn', $template_data['section_id'] );
endif;
?>

<?php echo get_template_part_flexible( 'template-parts/before', 'section', $template_data ) ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php echo get_template_part_flexible( 'template-parts/heading', 'section', $template_data['heading'] ) ?>
            </div>
            <div class="col-xs-12">
                <table role="file-list" class="file-list">
                    <tbody>
                    <?php if(!empty($files_data)) : foreach ($files_data as $key => $file): ?>
                        <tr class="file file-<?php $file['id'] ?>">
                            <td role="file-data" class="file-data">
                                <a alt="<?php echo $file['alt'] ?>" class="file-link" target="_blank" href="<?php echo $file['url'] ?>" download>
                                    <i class="icon material-icons">file_download</i>
                                    <div class="data">
                                        <h5 class="file-title"><?php echo $file['title'] ?></h5>
                                        <div class="file-meta">
                                            <ul>
                                                <li class="file-type">Type: <p><?php echo $file['type'] ?></p><small>(<?php echo $file['subtype'] ?>)</small></li>
                                                <li class="file-size">Size: <?php echo $file['filesizeHumanReadable'] ?></li>
                                                <li class="file-date">Uploaded: <?php echo $file['dateFormatted'] ?></li>
                                            </ul>
                                        </div>
                                    </div>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; endif; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php echo get_template_part_flexible( 'template-parts/after', 'section', $template_data ) ?>