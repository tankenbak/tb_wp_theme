<?php
if ( ! defined( 'ABSPATH' ) ) exit; // we dont like direct access
/***********************************************************
| This is file for base function for flexible content,
| This file should be included in init-theme.php
| This feature required ACF pro plugin
\***********************************************************/

//function for  flex fields will be here !
// Example code
// new fields add to $sections_array[]

/**
 * Class for build flexible content
 * use class ->use_transient( bool/string ) for use transients/
 * @since 0.9.5
 **/
class flexibleContentBuilder
{
    protected $post_id; //int

    protected $is_builder; //bool

    protected $field_types; //array

    protected $post_type; //string

    public $gf_required; //bool

    public $map_required; //bool

    public $use_transient = false; //bool

    private $page_flex_transient; //page transient data, null if empty or false to use transient

    private $flex_acf_content_data = null; // acf content data

    function __construct()
    {
        $this->set_post_id();
        $this->set_post_type();
        $this->set_is_builder();
    }

    /*
     *  Ready to output content data;
     *  @params $post_id - optional, if provided, you will get data from post, by provided ID
     */
    public function out_flex_builder_content( $post_id = null, $wp_content = null )
    {
        if (!$this->is_builder) {
            $new_content_data = new stdClass();
            $new_content_data->content = $wp_content;
            return $new_content_data;
        }

        /**
         * --- $new_content_data ---
         * Object contain:
         * $obj->content = output content;
         * $obj->gForm = mixed, if false - dont load Gform scripts/styles, or array of GF data('id');
         * $obj->map = bool - load or dont map script
         **/
        $new_content_data = new stdClass();

        if ( $post_id ) {
           $this->set_post_id( $post_id );
        }

        if ( $this->use_transient ) {
            $this->flex_page_transient();
        }

        // if ther is no saved transient or we dont use transient
        if( !$this->page_flex_transient || !$this->use_transient){
            $this->set_flex_acf_content_data();
        }

        if ( $this->flex_acf_content_data && !$this->page_flex_transient ) {
            //happening when there is no transient or dont use transient
            $new_content_data->content = $this->build_flexible_content( $this->flex_acf_content_data, $wp_content );
        }else{
            $new_content_data = $this->page_flex_transient;
        }

        if ( !isset($new_content_data->gForm) )
        {
            $new_content_data->gForm  = $this->check_if_gf_is_need();
        }

        if ( !isset($new_content_data->map) )
        {
            $new_content_data->map  = $this->check_if_map_is_need();
        }

        $this->maybe_enqueue_google_maps_js( $new_content_data->map );

        // save transient if is content and old dont exist
        if ( $this->use_transient && $new_content_data && !$this->page_flex_transient )
        {
            //var_dump( 'create trnsient' );
            $this->save_flex_page_content_transient( $new_content_data );
        }else{
            //var_dump('From transient');
        }

       // echo $new_content_data;

        return $new_content_data;
    }

    public function hooked_out_content( $content )
    {
        $new_content = $this->out_flex_builder_content( false, $content );
        return $new_content->content;
    }


    public function build_flexible_content( $flex_content_acf_data, $wp_content = null )
    {
        ob_start();
        foreach ( $flex_content_acf_data as $data) {
            get_template_part_flexible( 'sections/'.$data['acf_fc_layout'].'/flexible', 'template', $data );
        }
        $new_content =  ob_get_clean();
        return $new_content;
    }
    /*
     * Check if current page use flexible builder
     */

    public function check_if_builder($post_type = null)
    {
        if (!$post_type) {
            $post_type = $this->post_type;
        }
        // if ($post_type == null) {
        //     $post_type  = get_post_type();
        // }
        $is_builder = false;
        if( !function_exists('tb_builder_locations') && ( $post_type === 'page' || $post_type === 'flexible_sections' ) ){;
            return true;
        }

        //should be extendable
        if( function_exists('tb_builder_locations') ){
            $locations = tb_builder_locations();
            foreach ( $locations as $types ) {
                if( $types[0]['value'] === $post_type  ){
                     $is_builder = true;
                    break;
                }
            }
        }
        return $is_builder;
    }


    /*
     * Take all fields from flexible bluidler, and make array of type fields;
     */
    public function get_field_types( $flex_fields = null )
    {
        if ( !$flex_fields ) {
            if ( !$this->flex_acf_content_data ) {
                $this->set_flex_acf_content_data();
            }

            $flex_fields = $this->flex_acf_content_data;
        }

        $fields_types = [];
        if(!empty($flex_fields)) {
            foreach ($flex_fields as $layout) {
                $fields_types[] = $layout['acf_fc_layout'];
            }
        }

        return array_unique( $fields_types );

    }

    protected function set_field_types()
    {
        $fields = $this->get_field_types();
        $this->field_types = $fields;
    }

    /**
     * Checking if exist Gform in keys
     */
    public function check_if_gf_is_need()
    {
        $form_required = false;
        if ( !$this->field_types ) {
            $this->set_field_types();
        }
        if ( in_array( 'gform', $this->field_types ) ) {
            $form_required = true;
        }

        return $form_required;
    }
    /**
     * Checking if exist Gform in keys
     */
    public function check_if_map_is_need(){
        $map_required = false;
        if ( !$this->field_types ) {
            $this->set_field_types();
        }
        if ( in_array( 'map', $this->field_types ) ) {
            $map_required = true;
        }

        $this->map_required = $map_required;
        return $map_required;
    }

    protected function maybe_enqueue_google_maps_js( $map_required = false ) {
        if( $this->map_required || $map_required ) {
            add_action( 'wp_footer', 'enqueue_google_map_script' );
        }
    }

    /*
    public function check_if_videp_scripts_is_need($flex_content_data){
        $video_required = true;

        if(!empty($flex_content_data)) {
            foreach ($flex_content_data as $layout) {
                if( $layout['acf_fc_layout'] == 'big-title' && $layout['video_bg'] ) {
                    $video_required = true;
                }
            }
        }

        return $video_required;
    }

    protected function maybe_enqueue_video_scripts( $vide_required ) {
        if($vide_required) {
            add_action( 'wp_footer', 'enqueue_vimeo_api_script' );
        }
    }
    */

    /*
     * Set to  true for transient
     */
    public function use_transient($use_transient)
    {
        if ( $use_transient && $use_transient !== '0' ) {
            $this->use_transient = true;
        }
    }

    /*
     * set builder varrible
     */
    protected function set_flex_acf_content_data()
    {
        $this->flex_acf_content_data = get_field( 'content', $this->post_id );
    }

    /*
     * get page flex transient
     */
    public function flex_page_transient()
    {
        //if there is no setted transient var, set it;
        if (!$this->page_flex_transient) {
            $this->set_flex_page_transient();
        }

        return $this->page_flex_transient;
    }

    /*
     * set page flex transient
     */
    protected function set_flex_page_transient()
    {
        if ( $this->use_transient ) {
            $this->page_flex_transient = get_transient( 'page_flex_transient_'.$this->post_id );
        }else{
            $this->page_flex_transient = false;
        }
    }

    /*
     * save page flex transient
     */
    protected function save_flex_page_content_transient( $new_content )
    {
        $new_transient = new stdClass();
        $new_transient = $new_content;

        set_transient( 'page_flex_transient_'.$this->post_id , $new_transient, 80 * DAY_IN_SECONDS );
    }

    protected function set_is_builder()
    {
        return $this->is_builder = $this->check_if_builder();
    }

    /*
     * set builder varrible
     */
    protected function set_post_type()
    {
        $this->post_type = get_post_type();
    }

    protected function set_post_id( $post_id = null )
    {

        if (!$post_id) {
            global $post;
            $post_id = $post->ID;
        }

        $this->post_id = (int) $post_id;
    }
    /*
     * Show post ID
     */
    public function post_id()
    {
        return $this->post_id;
    }
    /*
     * Show  if it is builder content
     */
    public function is_builder()
    {
        return $this->is_builder;
    }

}



add_filter( 'the_content', 'build_new_content_tb', 20, 1 );
function build_new_content_tb( $content ){
    if ( !is_admin() && is_singular() && !is_front_page() && !is_archive() && !is_404() ) {
        global $option;
        $builder = new flexibleContentBuilder();
        $use_transient_opt = $option['admin-flexible-transients'];
        $builder->use_transient( $use_transient_opt );
        $content = $builder->hooked_out_content( $content );

    }
    return $content;
}


/*
 * array of new added post types
 */
function get_tb_builder_locations_post_types( $post_types_added = null ){
    $post_types = [
        'page',
        //'post',
        'flexible_sections'
    ];

    if ( is_array( $post_types_added ) && !empty( $post_types_added ) ) {
        $post_types = array_merge( $post_types, $post_types_added );
    }

    return $post_types;
}

function tb_builder_locations(){
    $post_types = get_tb_builder_locations_post_types();
    $tb_builder_locations = [];

    foreach ($post_types as $key => $post_type) {
         $tb_builder_locations[] =  array (
            array (
                'param' => 'post_type',
                'operator' => '==',
                'value' => $post_type,
            ),
        );
    }

    return $tb_builder_locations;
}



//builder standard setting classes
function tb_builder_standard_class( $template_data, $extra_data = null ){

    $is_bottom_padding = $template_data['padding_bottom']; //NOTE: Yes name is swapped but cant change, it will destroy current page, just swap in section desc names
    $is_top_padding = $template_data['padding_top'];
    $text_theme = 0;
    $info['extra'] = '';

    $info['class'] = ' fc-'.$template_data['acf_fc_layout'];
    if ($template_data['acf_fc_layout'] !== 'big-title') {
        $info['class'] .= ' links-outlined ';
    }else{
        $info['class'] .= ' links-filled ';
    }
    // Paddings
    if ($is_top_padding) {
        $info['class'] .= ' padding-top';
    }
    if ($is_bottom_padding) {
        $info['class'] .= ' padding-bottom';
    }


    return $info;
}



function clear_page_flex_content_transients($post_id, $post, $update) {
    if(wp_is_post_revision( $post_id )) return;
    global $option;
    $slug = $post->post_type;
    $builder = new flexibleContentBuilder();
    $is_builder = $builder->check_if_builder($slug);
    if( isset( $option['admin-flexible-transients'] ) && $option['admin-flexible-transients'] != '' &&  $option['admin-flexible-transients'] === '0' || !$is_builder ){
        return;
    }else{
        delete_transient( 'page_flex_transient_'.$post_id );
    }

    // switch ($slug) {
    //   case 'page':
    //         delete_transient( 'page_flex_transient_'.$post_id );
    //       break;
    //   default:
    //       return;
    //       break;
    // }
}
add_action( 'save_post', 'clear_page_flex_content_transients', 10, 3 );

if(is_admin() && current_user_can( 'manage_options' )){
    add_action( 'wp_ajax_tb_clear_transients', 'tb_clear_transients' );
    add_action( 'wp_ajax_nopriv_tb_clear_transients', 'tb_clear_transients' );
}

function tb_clear_transients(){
    global $wpdb;
    $sql = "DELETE
            FROM {$wpdb->options}
            WHERE option_name like '\_transient\_page_flex_transient\_%'
            OR option_name like '\_transient\_timeout\_page_flex_transient\_%'
            ";

    $resultat = $wpdb->get_results( $wpdb->query($sql) );
    $json = array( 'resultat'=> $resultat, 'message' => __('Flexible content transients are deleted', 'tankenbak') );

    echo json_encode($json);
    die();
}

function _get_all_image_sizes() {
    global $_wp_additional_image_sizes;
    $size_names = array(
         'thumbnail' => __('Thumbnail'),
         'medium'    => __('Medium'),
         'large'     => __('Large'),
         'full' => __('Full size'),
    );
    $size_names = tankenbak_image_sizes( $size_names );

    $default_image_sizes = array( 'thumbnail', 'medium', 'large' );
    foreach ( $default_image_sizes as $size ) {
        $image_sizes[$size]['width']    = intval( get_option( "{$size}_size_w") );
        $image_sizes[$size]['height'] = intval( get_option( "{$size}_size_h") );
        $image_sizes[$size]['crop'] = get_option( "{$size}_crop" ) ? true : false;
    }

    if ( isset( $_wp_additional_image_sizes ) && count( $_wp_additional_image_sizes ) ){
        $image_sizes = array_merge( $image_sizes, $_wp_additional_image_sizes );
    }

    foreach ( $image_sizes as $key => $size ) {
        if ( array_key_exists( $key, $size_names ) ) {
            $image_sizes[$key]['name']  = $size_names[$key];
        }else{
            $image_sizes[$key]['name']  = $key;
        }
    }

    return $image_sizes;
}
// add_action('acf/validate_save_post', 'my_acf_validate_save_post', 10, 0);

// function my_acf_validate_save_post() {
//     die('asdasd');
//     // check if user is an administrator
//     if( current_user_can('manage_options') ) {

//         // clear all errors
//         $GLOBALS['acf_validation_errors'] = array();

//     }

// }



