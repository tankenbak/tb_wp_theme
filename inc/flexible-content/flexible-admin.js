/*******************************************************/
// This file is used to merage all flexible content JS
// File is used for admin - file meraged in admin.js
/*******************************************************/


// @codekit-append "sections/posts/admin-scripts.js"
// @codekit-append "sections/map/admin-scripts.js"

jQuery(document).ready(function($) {
    // acf.add_filter('validation_complete', function( json, $form ){
    //     console.log($form );
    //     if(json.errors) {
    //         var field = jQuery('[name="' + json.errors[0].input + '"]', $form).parents('.acf-field');
    //             field = field[field.length - 1];
    //         var tab = jQuery(field, $form).prev('.acf-field-tab').attr('data-key');
    //         jQuery('.acf-tab-wrap a[data-key=' + tab + ']', $form).click();
    //     }
    //     return json;
    // });
    $(document).on('keyup', '.section-id input', function(e){
        var txt = $(this);
        var func = function(d) {
          if(d.keyCode === 32){
            txt.val(txt.val().replace(/\s/g, '-'));
          }
        }
        txt.keyup(func).blur(func);
    });

});
