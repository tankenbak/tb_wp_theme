<?php
/**
     * For full documentation, please visit: http://docs.reduxframework.com/
     * For a more extensive sample-config file, you may look at:
     * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "option";
    $page_layouts = tankenbak_options_layouts();
    $page_layouts_flex = tankenbak_options_layouts_flex();
    $archive_layouts = tankenbak_archive_layouts();
    $sidebars = tankenbak_options_sidebars();
    $field_types = options_generator_field_type();
    $dash_widgets = get_das_widgets_list_tb_opt();
    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        'opt_name' => 'option',
        'use_cdn' => FALSE,
        'display_name' => $theme->get('Name'),
        'display_version' => TB_VERSION,
        'page_slug' => 'theme-options',
        'disable_tracking' => true,
        'page_title' => __( 'Theme Option', 'tankenbak' ),
        'update_notice' => FALSE,
        'dev_mode'             => true,
       // 'intro_text' => __('Have a nice day!', 'tankenbak'),
        'admin_bar' => TRUE,
        'menu_type' => 'menu',
        'menu_title' => 'Options',
        'allow_sub_menu' => TRUE,
        'page_parent_post_type' => 'options',
        'customizer' => TRUE,
        'page_priority' => 99,
        'admin_bar_priority'   => 50,
        //'page_parent'          => 'edit.php',
        'default_mark' => ' <i class="fa fa-star-o"></i>',
        'class' => 'redux_settings',
        'hints' => array(
            'icon' => 'el el-idea',
            'icon_position' => 'right',
            'icon_color' => '#eeee22',
            'icon_size' => 'normal',
            'tip_style' => array(
                'color' => 'light',
                'style' => 'youtube',
            ),
            'tip_position' => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect' => array(
                'show' => array(
                    'effect' => 'fade',
                    'duration' => '500',
                    'event' => 'mouseover',
                ),
                'hide' => array(
                    'effect' => 'fade',
                    'duration' => '500',
                    'event' => 'mouseleave unfocus',
                ),
            ),
        ),
        'output' => TRUE,
        'output_tag' => TRUE,
        'settings_api' => false,
        'cdn_check_time' => '1440',
        'compiler' => TRUE,
        'page_permissions' => 'manage_options',
        'save_defaults' => TRUE,
        'show_import_export' => TRUE,
        //'database' => 'options-redux',
        //'transient_time' => '3600',
        //'network_sites' => TRUE,
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'http://tankenbak.no',
        'title' => 'Visit Our Page',
        'icon'  => 'fa fa-globe'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/TankenBakAs/',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    /*
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    */
    $args['share_icons'][] = array(
        'url'   => 'https://www.linkedin.com/company/tanken-bak-as',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'tankenbak' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'tankenbak' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'tankenbak' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'tankenbak' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'tankenbak' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */
    /*-----------------------
     * <--- General Opt
    -----------------------*/
    $base_name = 'general';
    Redux::setSection( $opt_name, array(
        'title'  => __( 'General', 'tankenbak' ),
        'id'     => $base_name,
        'icon'   => 'fa fa-xing',
    ) );


    // /*-- Site layout tab --*/
    // Redux::setSection($opt_name, array(
    //     'title'      => 'Page Sizes',
    //     'id'         =>  $base_name.'-site',
    //     'desc'       => 'Adjustment of page elements sizes',
    //     'subsection' => true,
    //     'fields'     => array(
    //         array(
    //             'id'=> $base_name.'-site-layout-header',
    //             'type' => 'image_select',
    //             'title' => __('Site Header Layout', 'tankenbak'),
    //             'options' => $page_layouts_flex,
    //             'default' => 'boxed',
    //             'class'   => 'layout-select',
    //         ),
    //         array(
    //             'id'=> $base_name.'-site-layout-content',
    //             'type' => 'image_select',
    //             'title' => __('Site Content Layout', 'tankenbak'),
    //             'options' => $page_layouts_flex,
    //             'default' => 'boxed',
    //             'class'   => 'layout-select',
    //         ),
    //         array(
    //             'id'=> $base_name.'-site-layout-footer',
    //             'type' => 'image_select',
    //             'title' => __('Site Footer Layout', 'tankenbak'),
    //             'options' => $page_layouts_flex,
    //             'default' => 'boxed',
    //             'class'   => 'layout-select',
    //         ),
    //     )
    // ));

    /* Sidebar opt */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Page Logo ', 'tankenbak' ),
        //'desc'       => __( 'Sonsors Detalis','tankenbak' ),
        'id'         => $base_name.'-logo',
        //'icon'       => 'el el-screen',
        'desc' => __('Not implemented yet', 'tankenbak'),
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => $base_name.'-logo-image',
                'type'     => 'background',
                'url'      => true,
                'title'    => __('Logo Image', 'tankenbak'),
                'subtitle'     => __('Select image or upload as logo', 'tankenbak'),
                'preview_media' => true,
                'preview'   => false,
                'background-color'  => false,
                'background-repeat' => false,
                'background-attachment' => false,
                'background-position'   => false,
                'background-size'   => false,
                'default'  => array(
                    'background-color' => 'transparent',
                    'background-size' => 'initial',
                    ),
                ),
        ),
    ));
    /*--- Pagination tab ---*/
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Pagination ', 'tankenbak' ),
        //'desc'       => __( 'Sonsors Detalis','tankenbak' ),
        'id'         => $base_name.'-pagin',
        //'icon'       => 'el el-screen',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'        => $base_name.'-pagination',
                'type'      => 'checkbox',
                'title'     => __('Pagination Type', 'tankenbak'),
                'subtitle' => __('Do you want use Bootstrap cool pagination, insted of Wordpress Next, Prev ?', 'tankenbak'),
                'default'  => '1'// 1 = on | 0 = off
                ),
        ),
    ));

    /* Sidebar tab */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Sidebars ', 'tankenbak' ),
        //'desc'       => __( 'Sonsors Detalis','tankenbak' ),
        'id'         => $base_name.'-sidebar',
        //'icon'       => 'el el-screen',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'        => $base_name.'-sidebars',
                'type'      => 'multi_text',
                'title'     => __('Add Sidebar', 'tankenbak'),
                'subtitle' => __('Enter name of Sidebar, you want to generate', 'tankenbak'),
                //'desc' => __('This is the description field, again good for additional info.', 'tankenbak')
                ),
        ),
    ));

    /*-- Lazy Load --*/
     Redux::setSection( $opt_name, array(
            'title'      => __( 'Lazy Load', 'tankenbak' ),
            //'desc'       => __( '', 'tankenbak' ),
            'id'         => $base_name.'-lazy',
            'subsection' => true,
            'fields'     => array(
                array(
                    'id'=> $base_name.'-lazy-content-images',
                    'type' => 'switch',
                    'title' => __('Lazy load Content Images', 'tankenbak'),
                    'desc'  => __('Turn on to use lazy loading for all images in page or posts content','tankenbak'),
                    'default' => false,
                    'on' => __('Yes', 'tankenbak'),
                    'off' => __('No', 'tankenbak'),
                ),
            ),
        ));
    /*--- Info tab ---*/
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Site Identity', 'tankenbak' ),
        'desc'       => __( 'To change it, go to <a style="btn btn-primary" href="'.admin_url('/customize.php').'">Customize -> Site Identity </a> <br>You can also change there <b>Favicon</b>','tankenbak' ),
        'id'         => $base_name.'-identity',
        //'icon'       => 'el el-screen',
        'subsection' => true,
    ));
    /*
     * <--- END SECTIONS
     */


    /*-----------------------
     * <--- Header Opt
    -----------------------*/
    $base_name = 'general';
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Cookie Law', 'tankenbak' ),
        'id'     => $base_name.'-cookie',
        'icon'   => 'el el-compass',
        'fields' => array(
                array(
                    'id'=> $base_name.'-cookie-law',
                    'type' => 'switch',
                    'title' => __('Show Cookie law message', 'tankenbak'),
                    'desc'  => __('Turn on to show info on your page ','tankenbak'),
                    'default' => false,
                    'on' => __('Show', 'tankenbak'),
                    'off' => __('No', 'tankenbak'),
                ),
                array(
                    'id'         => $base_name.'-cookie-text',
                    'type'       => 'textarea',
                    'title'      => __('Text for cookie info', 'tankenbak'),
                    'subtitle'   => __('Enter text, that will be showed on cookie info bar.', 'tankenbak'),
                    'full_width' => true,
                    'placeholder'=> 'Cookie law text',
                    'default'    => 'This site uses cookies. By continuing to browse the site, you are agreeing to our use of cookies. Find out more.',
                    'rows'       => '4',
                ),
                array(
                    'id'         => $base_name.'-cookie-accept',
                    'type'       => 'text',
                    'title'      => __('Accept Button Text', 'tankenbak'),
                    'subtitle'   => __('Text displayed on Accept button', 'tankenbak'),
                    'placeholder'=> 'Accept text',
                    'default'       => 'Accept',
                ),
                array(
                    'id'         => $base_name.'-cookie-more',
                    'type'       => 'text',
                    'title'      => __('Read More Button Text', 'tankenbak'),
                    'subtitle'   => __('Text displayed on Read More button', 'tankenbak'),
                    'placeholder'=> 'Read More text',
                    'default'       => 'Read More',
                ),
                array(
                    'id'         => $base_name.'-cookie-link',
                    'type'       => 'text',
                    'validate'   => 'url',
                    'title'      => __('Read More Link', 'tankenbak'),
                    'subtitle'   => __('Link to more information page, about cookie info', 'tankenbak'),
                    'placeholder'=> 'Read More link',
                    'default'       => 'https://en.wikipedia.org/wiki/HTTP_cookie',
                ),
                array(
                    'id'=> $base_name.'-cookie-debug',
                    'type' => 'switch',
                    'title' => __('Debug mode', 'tankenbak'),
                    'desc'  => __('Turn on dont dont save cookie','tankenbak'),
                    'default' => false,
                    'on' => __('Yes', 'tankenbak'),
                    'off' => __('No', 'tankenbak'),
                ),
            ),
    ) );
    /*-- Cookie Law info --*/
    /*
     * <--- END SECTIONS
     */
/*-----------------------
     * <--- Front Page
    -----------------------*/
// Redux::setSection($opt_name, array(
//     'id'         => 'diver-menu-1',
//     'subsection' => false,
//     'type' => 'divide'
// ));
//     $base_name = 'front_page'; // USE POST TYPE NAME!!!
//     Redux::setSection( $opt_name, array(
//         'title'  => __( 'Front Page', 'tankenbak' ),
//         'id'     => $base_name,
//         'desc'   => __( 'Option for standard wordpress Pages', 'tankenbak' ),
//         'icon'   => 'fa fa-home',
//         'fields' => array(
//             array(
//                 'id'=> $base_name.'-single-layout',
//                 'type' => 'image_select',
//                 'title' => __('Front Layout', 'tankenbak'),
//                 'options' => $page_layouts,
//                 'default' => 'fullwidth',
//                 'class'   => 'layout-select',
//                 //'desc' => __('Not implemented yet', 'tankenbak'),
//             ),
//             array(
//                 'id'=> $base_name.'-single-sidebar',
//                 'type' => 'select',
//                 'title' => __('Front Sidebar', 'tankenbak'),
//                 'description'   => __('If you add sidebar in "General" settings, and you cant fint it, Save settings, and refresh page.', 'tankenbak'),
//                 'required' => array( $base_name.'-single-layout','equals',$sidebars),
//                 'data' => 'sidebars',
//                 'desc' => __('If you select sidebar and you use VC stretch ... its simply not good idea', 'tankenbak'),
//             ),
//             array(
//                 'id'      => $base_name.'-block-layout',
//                 'type'    => 'sorter',
//                 'title'   => __('Front Page Layout.', 'tankenbak'),
//                 'desc'    => __('Simply organize your single post layout template.', 'tankenbak'),
//                 'options' => array(
//                     'enabled'  => array(
//                         //'thumb'   => 'Thumbmail',
//                         'content' => 'Content',
//                         //'share'   => 'Share Block',
//                         //'cats'   => 'Category & Tags Block',
//                         //'comment'   => 'Comment'
//                     ),
//                     'disabled' => array(
//                         'title'   => 'Title',
//                         //'meta'     => 'Meta Block',
//                     ),
//                     'Not Used' =>array(
//                         //'links' => 'Posts Links',
//                     )
//                 ),
//             ),
//         ),
//     ) );
    /*-----------------------
     * <--- Pages Opt
    -----------------------*/
    /*
    $base_name = 'page';
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Pages', 'tankenbak' ),
        'id'     => $base_name,
        'desc'   => __( 'Option for standard wordpress Pages', 'tankenbak' ),
        'icon'   => 'fa fa-file-powerpoint-o',
        'fields' => array(),
    ) );
    */
    /*
     * <--- END SECTIONS
     */
    $base_name = 'archives'; // USE POST TYPE NAME!!!
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Archive pages', 'tankenbak' ),
        'id'     => $base_name,
        'desc'   => __( 'Select Archives pages', 'tankenbak' ),
        'icon'   => 'fa fa-search',
        'fields' => array(
             array(
                'id'         => $base_name.'-projects',
                'type'       => 'select',
                'title'      => __('Select Page for Projects', 'tankenbak'),
                //'desc'       => __('Select page for redirect from "for kursleder" archive.', 'tankenbak'),
                'data'       => 'page',
            ),
             array(
                'id'         => $base_name.'-articles',
                'type'       => 'select',
                'title'      => __('Select Page for Articles', 'tankenbak'),
                //'desc'       => __('Select page for redirect from "for kursleder" archive.', 'tankenbak'),
                'data'       => 'page',
            ),
            array(
                 'id'         => $base_name.'-employee',
                 'type'       => 'select',
                 'title'      => __('Select Page for Employees', 'tankenbak'),
                 //'desc'       => __('Select page for redirect from "for kursleder" archive.', 'tankenbak'),
                 'data'       => 'page',
             )
        ),
    ) );
    /*-----------------------
     * <--- Blog Opt
    -----------------------*/
    $base_name = 'blog_page'; // USE POST TYPE NAME!!!
    /* Blog page */
   /* Redux::setSection( $opt_name, array(
        'title'      => __( 'Blog page', 'tankenbak' ),
        'desc'       => __( 'Settings for Archive single Post', 'tankenbak' ),
        'id'         => $base_name.'-blog',
        'subsection' => false,
        'fields' => array(
            // array(
            //     'id'=> $base_name.'-blog-layout',
            //     'type' => 'image_select',
            //     'title' => __('Blog page Layout', 'tankenbak'),
            //     'options' => $page_layouts,
            //     'default' => 'fullwidth',
            //     'class'   => 'layout-select',
            //    // 'desc'  => __( 'Wide elements will not work if in general settings, content is boxed :-)', 'tankenbak' ),
            // ),
            // array(
            //     'id'=> $base_name.'-single-sidebar',
            //     'type' => 'select',
            //     'title' => __('Select Sidebar', 'tankenbak'),
            //     'description'   => __('If you add sidebar in "General" settings, and you cant fint it, Save settings, and refresh page.', 'tankenbak'),
            //     'required' => array( $base_name.'-blog-layout','equals',$sidebars),
            //     'data' => 'sidebars'
            // ),
            array(
                'id'=> $base_name.'-blog-style-layout',
                'type' => 'image_select',
                'title' => __('Page Layout', 'tankenbak'),
                'options' => $archive_layouts,
                'default' => 'box-style',
                'class' => 'archive-style',
                //'desc' => __('Not implemented yet', 'tankenbak'),
            ),
            array(
                'id'        => $base_name.'-blog-box-count',
                'required' => array( $base_name.'-blog-style-layout','equals','box-style'),
                'type'      => 'slider',
                'title'     => __('Boxes in row', 'tankenbak'),
                'subtitle'  => __('How many boxes should be in row', 'tankenbak'),
                'desc'      => __('You can choose number of block in row displayed on blog page', 'tankenbak'),
                "default"   => 3,
                "min"       => 1,
                "step"      => 1,
                "max"       => 4,
                'display_value' => 'label'
            ),
        ),
    ));*/

    /*-----------------------
     * <--- 404 Page
    -----------------------*/
/*
    $base_name = '404';
    Redux::setSection( $opt_name, array(
        'title'  => __( '404 Page', 'tankenbak' ),
        'id'     => $base_name,
        'desc'   => __( 'Option for standard wordpress Pages', 'tankenbak' ),
        'icon'   => 'fa fa-life-ring',
        'fields' => array(
            array(
                'id'=> $base_name.'-single-layout',
                'type' => 'image_select',
                'title' => __('Front Layout', 'tankenbak'),
                'options' => $page_layouts,
                'default' => 'fullwidth',
                'class'   => 'layout-select',
                //'desc' => __('Not implemented yet', 'tankenbak'),
            ),
            array(
                'id'=> $base_name.'-single-sidebar',
                'type' => 'select',
                'title' => __('Front Sidebar', 'tankenbak'),
                'description'   => __('If you add sidebar in "General" settings, and you cant fint it, Save settings, and refresh page.', 'tankenbak'),
                'required' => array( $base_name.'-single-layout','equals',$sidebars),
                'data' => 'sidebars',
                'desc' => __('If you select sidebar and you use VC stretch ... its simply not good idea', 'tankenbak'),
            ),
        ),
    ) );
    */
    /*
     * <--- END SECTIONS
     */


Redux::setSection($opt_name, array(
    'id'         => 'diver-menu-2',
    'subsection' => false,
    'type' => 'divide'
));
    /*
     * <--- END SECTIONS
     */
    /*-----------------------
     * <--- Footer Opt
    -----------------------*/
    $base_name = 'footer';
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Footer', 'tankenbak' ),
        'id'     => $base_name,
        'desc'   => __( 'Option for standard wordpress posts', 'tankenbak' ),
        'icon'   => 'el  el-inbox',
        'fields'     => array(
             array(
                'id'       => $base_name.'-background-image',
                'type'     => 'background',
                'url'      => true,
                'title'    => __('Background Image', 'tankenbak'),
                'subtitle'     => __('Select image or upload', 'tankenbak'),
                'preview_media' => true,
                'preview'   => false,
                'default'  => array(
                    'background-color' => 'transparent',
                    'background-size' => 'initial',
                    ),
                ),
        ),
    ));

    /*
     * <--- END SECTIONS
     */

    /*-----------------------
     * <--- Social Opt
    -----------------------*/
    $base_name = 'social';
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Social', 'tankenbak' ),
        'id'     => $base_name,
        'desc'   => __( 'Enable follow options', 'tankenbak' ),
        'icon'   => 'el el-friendfeed',
        'fields'     => array(
            ),
    ) );
    /* Social Share */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Social Share post', 'tankenbak' ),
        'desc'       => __( 'Enable where content can be shared. <br>', 'tankenbak' ),
        'icon'       => 'fa fa-share-alt',
        'id'         => $base_name.'-share',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'=> $base_name.'-facebook-share',
                'type' => 'switch',
                'title' => __('Share to Facebook', 'tankenbak'),
                'default' => true,
                'on' => __('Yes', 'tankenbak'),
                'off' => __('No', 'tankenbak'),
            ),
            array(
                'id'=> $base_name.'-twitter-share',
                'type' => 'switch',
                'title' => __('Share to Twitter', 'tankenbak'),
                'default' => false,
                'on' => __('Yes', 'tankenbak'),
                'off' => __('No', 'tankenbak'),
            ),
            array(
                'id'=> $base_name.'-pinterest-share',
                'type' => 'switch',
                'title' => __('Share to Pinterest', 'tankenbak'),
                'default' => false,
                'on' => __('Yes', 'tankenbak'),
                'off' => __('No', 'tankenbak'),
            ),
           /* array(
                'id'=> $base_name.'-instagram-share',
                'type' => 'switch',
                'title' => __('Share to Instagram', 'tankenbak'),
                'default' => false,
                'on' => __('Yes', 'tankenbak'),
                'off' => __('No', 'tankenbak'),
            ),*/
            array(
                'id'=> $base_name.'-linkedin-share',
                'type' => 'switch',
                'title' => __('Share to LinkedIn', 'tankenbak'),
                'default' => false,
                'on' => __('Yes', 'tankenbak'),
                'off' => __('No', 'tankenbak'),
            ),
            array(
                'id'=> $base_name.'-googleplus-share',
                'type' => 'switch',
                'title' => __('Share to Google Plus', 'tankenbak'),
                'default' => false,
                'on' => __('Yes', 'tankenbak'),
                'off' => __('No', 'tankenbak'),
            ),
        ),
    ));

     /* facebook settings */
     Redux::setSection( $opt_name, array(
         'title'      => __( 'Facebook', 'tankenbak' ),
         'desc'       => __( 'Facebook Detalis', 'tankenbak' ),
         'id'         => $base_name.'-facebook',
         'icon'       => 'fa fa-facebook',
         'subsection' => true,
         'fields'     => array(
                    array(
                        'id'=> $base_name.'-facebook-show',
                        'type' => 'switch',
                        'title' => __('Show Facebook', 'tankenbak'),
                        'default' => false,
                        'on' => __('Yes', 'tankenbak'),
                        'off' => __('No', 'tankenbak'),
                    ),
                   array(
                       'id'       => $base_name.'-facebook-icon',
                       'required' => array( $base_name.'-facebook-show','equals',true),
                       'type'     => 'media',
                       'url'      => true,
                       'title'    => __('Icon', 'tankenbak'),
                       'desc'     => __('Select icon for facebook', 'tankenbak'),
                       'subtitle' => __('Upload any media using the WordPress native uploader', 'tankenbak'),
                       'default'  => array(
                           //'url'  => dirname( __FILE__ ).'/img/fb2.svg'
                       ),
                   ),
                   array(
                       'id'       => $base_name.'-facebook-title',
                       'required' => array( $base_name.'-facebook-show','equals',true),
                       'type'     => 'text',
                       'title'    => __('Title', 'tankenbak'),
                       'desc'     => __('Enter title displayed near icon', 'tankenbak'),
                       'placeholder' => __('Title', 'tankenbak'),
                       'default'  => __('Follow us on ', 'tankenbak'),
                   ),
                   array(
                       'id'       => $base_name.'-facebook-link',
                       'required' => array( $base_name.'-facebook-show','equals',true),
                       'type'     => 'text',
                       'title'    => __('URL', 'tankenbak'),
                       'desc'     => __('Enter Url To Fanpage', 'tankenbak'),
                       'placeholder' => __('facebook.com', 'tankenbak'),
                       'default'  => __('#', 'tankenbak'),
                   ),
            ),
     ));

     /* Twitter settings */
     Redux::setSection( $opt_name, array(
         'title'      => __( 'Twitter', 'tankenbak' ),
         'desc'       => __( 'Twitter Detalis', 'tankenbak' ),
         'id'         => $base_name.'-twitter',
         'icon'       => 'fa fa-twitter',
         'subsection' => true,
         'fields'     => array(
                    array(
                        'id'=> $base_name.'-twitter-show',
                        'type' => 'switch',
                        'title' => __('Show Twitter', 'tankenbak'),
                        'default' => false,
                        'on' => __('Yes', 'tankenbak'),
                        'off' => __('No', 'tankenbak'),
                    ),
                   array(
                       'id'       => $base_name.'-twitter-icon',
                       'required' => array( $base_name.'-twitter-show','equals',true),
                       'type'     => 'media',
                       'url'      => true,
                       'title'    => __('Icon', 'tankenbak'),
                       'desc'     => __('Select icon for twitter', 'tankenbak'),
                       'subtitle' => __('Upload any media using the WordPress native uploader', 'tankenbak'),
                       'default'  => array(
                           //'url'  => dirname( __FILE__ ).'/img/fb2.svg'
                       ),
                   ),
                   array(
                       'id'       => $base_name.'-twitter-title',
                       'required' => array( $base_name.'-twitter-show','equals',true),
                       'type'     => 'text',
                       'title'    => __('Title', 'tankenbak'),
                       'desc'     => __('Enter title displayed near icon', 'tankenbak'),
                       'placeholder' => __('Title', 'tankenbak'),
                       'default'  => __('Follow us on ', 'tankenbak'),
                   ),
                   array(
                       'id'       => $base_name.'-twitter-link',
                       'required' => array( $base_name.'-twitter-show','equals',true),
                       'type'     => 'text',
                       'title'    => __('URL', 'tankenbak'),
                       'desc'     => __('Enter Url To Fanpage', 'tankenbak'),
                       'placeholder' => __('twitter.com', 'tankenbak'),
                       'default'  => __('#', 'tankenbak'),
                   ),
            ),
     ));

     /* Pinterest settings */
     Redux::setSection( $opt_name, array(
         'title'      => __( 'Pinterest', 'tankenbak' ),
         'desc'       => __( 'Pinterest Detalis', 'tankenbak' ),
         'id'         => $base_name.'-pinterest',
         'icon'       => 'fa fa-pinterest-p',
         'subsection' => true,
         'fields'     => array(
                    array(
                        'id'=> $base_name.'-pinterest-show',
                        'type' => 'switch',
                        'title' => __('Show Pinterest', 'tankenbak'),
                        'default' => false,
                        'on' => __('Yes', 'tankenbak'),
                        'off' => __('No', 'tankenbak'),
                    ),
                   array(
                       'id'       => $base_name.'-pinterest-icon',
                       'required' => array( $base_name.'-pinterest-show','equals',true),
                       'type'     => 'media',
                       'url'      => true,
                       'title'    => __('Icon', 'tankenbak'),
                       'desc'     => __('Select icon for pinterest', 'tankenbak'),
                       'subtitle' => __('Upload any media using the WordPress native uploader', 'tankenbak'),
                       'default'  => array(
                           //'url'  => dirname( __FILE__ ).'/img/fb2.svg'
                       ),
                   ),
                   array(
                       'id'       => $base_name.'-pinterest-title',
                       'required' => array( $base_name.'-pinterest-show','equals',true),
                       'type'     => 'text',
                       'title'    => __('Title', 'tankenbak'),
                       'desc'     => __('Enter title displayed near icon', 'tankenbak'),
                       'placeholder' => __('Title', 'tankenbak'),
                       'default'  => __('Follow us on ', 'tankenbak'),
                   ),
                   array(
                       'id'       => $base_name.'-pinterest-link',
                       'required' => array( $base_name.'-pinterest-show','equals',true),
                       'type'     => 'text',
                       'title'    => __('URL', 'tankenbak'),
                       'desc'     => __('Enter Url To Fanpage', 'tankenbak'),
                       'placeholder' => __('pinterest.com', 'tankenbak'),
                       'default'  => __('#', 'tankenbak'),
                   ),
            ),
     ));

     /* Youtube settings */ /*
     Redux::setSection( $opt_name, array(
         'title'      => __( 'Youtube', 'tankenbak' ),
         'desc'       => __( 'Youtube Detalis', 'tankenbak' ),
         'id'         => $base_name.'-youtube',
         'icon'       => 'el el-group',
         'subsection' => true,
         'fields'     => array(
                 array(
                     'id'=> $base_name.'-youtube-show',
                     'type' => 'switch',
                     'title' => __('Show Youtube', 'tankenbak'),
                     'default' => false,
                     'on' => __('Yes', 'tankenbak'),
                     'off' => __('No', 'tankenbak'),
                 ),
                   array(
                       'id'       => $base_name.'-youtube-icon',
                       'required' => array( $base_name.'-youtube-show','equals',true),
                       'type'     => 'media',
                       'url'      => true,
                       'title'    => __('Icon', 'tankenbak'),
                       'desc'     => __('Select icon for youtube', 'tankenbak'),
                       'subtitle' => __('Upload any media using the WordPress native uploader', 'tankenbak'),
                       'default'  => array(
                           //'url'  => dirname( __FILE__ ).'/img/fb2.svg'
                       ),
                   ),
                   array(
                       'id'       => $base_name.'-youtube-title',
                       'required' => array( $base_name.'-youtube-show','equals',true),
                       'type'     => 'text',
                       'title'    => __('Title', 'tankenbak'),
                       'desc'     => __('Enter title displayed near icon', 'tankenbak'),
                       'placeholder' => __('Title', 'tankenbak'),
                       'default'  => __('Follow us on ', 'tankenbak'),
                   ),
                   array(
                       'id'       => $base_name.'youtube-link',
                       'required' => array( $base_name.'-youtube-show','equals',true),
                       'type'     => 'text',
                       'title'    => __('URL', 'tankenbak'),
                       'desc'     => __('Enter Url To Fanpage', 'tankenbak'),
                       'placeholder' => __('youtube.com', 'tankenbak'),
                       'default'  => __('#', 'tankenbak'),
                   ),
            ),
     ));*/

/* LinkedIn settings */
     Redux::setSection( $opt_name, array(
         'title'      => __( 'LinkedIn', 'tankenbak' ),
         'desc'       => __( 'LinkedIn Detalis', 'tankenbak' ),
         'id'         => $base_name.'-linkedin',
         'icon'       => 'fa fa-linkedin',
         'subsection' => true,
         'fields'     => array(
                    array(
                        'id'=> $base_name.'-linkedin-show',
                        'type' => 'switch',
                        'title' => __('Show LinkedIn', 'tankenbak'),
                        'default' => false,
                        'on' => __('Yes', 'tankenbak'),
                        'off' => __('No', 'tankenbak'),
                    ),
                   array(
                       'id'       => $base_name.'-linkedin-icon',
                       'required' => array( $base_name.'-linkedin-show','equals',true),
                       'type'     => 'media',
                       'url'      => true,
                       'title'    => __('Icon', 'tankenbak'),
                       'desc'     => __('Select icon for linkedin', 'tankenbak'),
                       'subtitle' => __('Upload any media using the WordPress native uploader', 'tankenbak'),
                       'default'  => array(
                           //'url'  => dirname( __FILE__ ).'/img/fb2.svg'
                       ),
                   ),
                   array(
                       'id'       => $base_name.'-linkedin-title',
                       'required' => array( $base_name.'-linkedin-show','equals',true),
                       'type'     => 'text',
                       'title'    => __('Title', 'tankenbak'),
                       'desc'     => __('Enter title displayed near icon', 'tankenbak'),
                       'placeholder' => __('Title', 'tankenbak'),
                       'default'  => __('Follow us on ', 'tankenbak'),
                   ),
                   array(
                       'id'       => $base_name.'-linkedin-link',
                       'required' => array( $base_name.'-linkedin-show','equals',true),
                       'type'     => 'text',
                       'title'    => __('URL', 'tankenbak'),
                       'desc'     => __('Enter Url To Fanpage', 'tankenbak'),
                       'placeholder' => __('linkedin.com', 'tankenbak'),
                       'default'  => __('#', 'tankenbak'),
                   ),
            ),
     ));

/* Google+ settings */
     Redux::setSection( $opt_name, array(
         'title'      => __( 'Google+', 'tankenbak' ),
         'desc'       => __( 'Google+ Detalis', 'tankenbak' ),
         'id'         => $base_name.'-googleplus',
         'icon'       => 'fa fa-google-plus',
         'subsection' => true,
         'fields'     => array(
                    array(
                        'id'=> $base_name.'-googleplus-show',
                        'type' => 'switch',
                        'title' => __('Show Google Plus', 'tankenbak'),
                        'default' => false,
                        'on' => __('Yes', 'tankenbak'),
                        'off' => __('No', 'tankenbak'),
                    ),
                   array(
                       'id'       => $base_name.'-googleplus-icon',
                       'required' => array( $base_name.'-googleplus-show','equals',true),
                       'type'     => 'media',
                       'url'      => true,
                       'title'    => __('Icon', 'tankenbak'),
                       'desc'     => __('Select icon for google+', 'tankenbak'),
                       'subtitle' => __('Upload any media using the WordPress native uploader', 'tankenbak'),
                       'default'  => array(
                           //'url'  => dirname( __FILE__ ).'/img/fb2.svg'
                       ),
                   ),
                   array(
                       'id'       => $base_name.'-googleplus-title',
                       'required' => array( $base_name.'-googleplus-show','equals',true),
                       'type'     => 'text',
                       'title'    => __('Title', 'tankenbak'),
                       'desc'     => __('Enter title displayed near icon', 'tankenbak'),
                       'placeholder' => __('Title', 'tankenbak'),
                       'default'  => __('Follow us on ', 'tankenbak'),
                   ),
                   array(
                       'id'       => $base_name.'-googleplus-link',
                       'required' => array( $base_name.'-googleplus-show','equals',true),
                       'type'     => 'text',
                       'title'    => __('URL', 'tankenbak'),
                       'desc'     => __('Enter Url To Fanpage', 'tankenbak'),
                       'placeholder' => __('Google+.com', 'tankenbak'),
                       'default'  => __('#', 'tankenbak'),
                   ),
            ),
     ));

     /* Instagram settings */
     Redux::setSection( $opt_name, array(
         'title'      => __( 'Instagram', 'tankenbak' ),
         'desc'       => __( 'Instagram Detalis', 'tankenbak' ),
         'id'         => $base_name.'-instagram',
         'icon'       => 'fa fa-instagram',
         'subsection' => true,
         'fields'     => array(
                    array(
                        'id'=> $base_name.'-instagram-show',
                        'type' => 'switch',
                        'title' => __('Show Instagram', 'tankenbak'),
                        'default' => false,
                        'on' => __('Yes', 'tankenbak'),
                        'off' => __('No', 'tankenbak'),
                    ),
                   array(
                       'id'       => $base_name.'-instagram-icon',
                       'required' => array( $base_name.'-instagram-show','equals',true),
                       'type'     => 'media',
                       'url'      => true,
                       'title'    => __('Icon', 'tankenbak'),
                       'desc'     => __('Select icon for Instagram', 'tankenbak'),
                       'subtitle' => __('Upload any media using the WordPress native uploader', 'tankenbak'),
                       'default'  => array(
                           //'url'  => dirname( __FILE__ ).'/img/fb2.svg'
                       ),
                   ),
                ),
    ));
   /*-----------------------
    * <--- Login Screen Opt
   -----------------------*/
   $base_name = 'log-screen';
   Redux::setSection( $opt_name, array(
       'title'  => __( 'Login Screen', 'tankenbak' ),
       'id'     => $base_name,
       'desc'   => __( 'Option Login Screen WP-LOGIN', 'tankenbak' ),
       'icon'   => 'fa fa fa-key',
   ) );

   /* logo opt */
   Redux::setSection( $opt_name, array(
       'title'      => __( 'Logo ', 'tankenbak' ),
       'desc'       => __( 'Settings for logo in login screen <br>Preview <a href="'.get_bloginfo('url').'/wp-login.php">Login Page</a>', 'tankenbak' ),
       'id'         => $base_name.'-logo',
       'subsection' => true,
       'fields'     => array(
           array(
               'id'       => $base_name.'-logo-image',
               'type'     => 'background',
               'url'      => true,
               'title'    => __('Logo Image', 'tankenbak'),
               'subtitle'     => __('Select image or upload as logo', 'tankenbak'),
               'preview_media' => true,
               'preview'   => false,
               'default'  => array(
                   'background-color' => 'transparent',
                   'background-size' => 'initial',
                   ),
               ),
           array(
               'id'             => $base_name.'-logo-height',
               'type'           => 'spacing',
               'output'         => false,
               'mode'           => 'absolute',
               'units'          => array('em', 'px', '%'),
               'units_extended' => false,
               'bottom'         => false,
               'left'           => false,
               'right'           => false,
               'title'          => __('Logo Height', 'tankenbak'),
               'default'            => array(
                       'top'     => '125px',
                       'units'          => 'px',
                   ),
               ),
           array(
               'id'             => $base_name.'-logo-width',
               'type'           => 'spacing',
               'output'         => false,
               'mode'           => 'absolute',
               'units'          => array('em', 'px', '%'),
               'units_extended' => false,
               'bottom'         => false,
               'left'           => false,
               'top'           => false,
               'title'          => __('Logo Width', 'tankenbak'),
               'default'            => array(
                       'right'     => '100%',
                       'units'          => '%',
                   ),
               ),
           array(
               'id'             => $base_name.'-logo-margin',
               'type'           => 'spacing',
               'output'         => false,
               'mode'           => 'margin',
               'units'          => array('em', 'px', '%'),
               'units_extended' => 'false',
               'title'          => __('Margin Option', 'tankenbak'),
               'subtitle'       => __('Allow your users to choose the spacing or margin they want.', 'tankenbak'),
               'desc'           => __('You can enable or disable any piece of this field. Top, Right, Bottom, Left, or Units.', 'tankenbak'),
               'default'            => array(
                       'margin-top'     => '0px',
                       'margin-right'   => '0px',
                       'margin-bottom'  => '0px',
                       'margin-left'    => '0px',
                       'units'          => 'px',
                   )

           ),
           array(
               'id'             => $base_name.'-logo-padding',
               'type'           => 'spacing',
               'output'         => false,
               'mode'           => 'padding',
               'units'          => array('em', 'px', '%'),
               'units_extended' => 'false',
               'title'          => __('Margin Option', 'tankenbak'),
               'subtitle'       => __('Allow your users to choose the spacing or margin they want.', 'tankenbak'),
               'desc'           => __('You can enable or disable any piece of this field. Top, Right, Bottom, Left, or Units.', 'tankenbak'),
               'default'            => array(
                       'margin-top'     => '0px',
                       'margin-right'   => '0px',
                       'margin-bottom'  => '0px',
                       'margin-left'    => '0px',
                       'units'          => 'px',
                   )

           ),
           array(
               'id'       => $base_name.'-logo-url',
               'type'     => 'text',
               'validate' => 'url',
               'title'    => __('Logo URL', 'tankenbak'),
               'subtitle' => __('Enter URL, after clicking to logo user will go to link', 'tankenbak'),
               'msg'      => __('Should be a link!', 'tankenbak'),
               'default'  => get_bloginfo('url'),
               'placeholder' => __('URL', 'tankenbak'),
              ),
               array(
               'id'       => $base_name.'-logo-title',
               'type'     => 'text',
               //'validate' => 'url',
               'title'    => __('Logo title', 'tankenbak'),
               'subtitle' => __('Enter title for titte attr.', 'tankenbak'),
               'default'  => get_bloginfo('name'),
               'placeholder' => __('Title', 'tankenbak'),
              ),
       ),
   ));
   /* Form */
   Redux::setSection( $opt_name, array(
       'title'      => __( 'Login Form', 'tankenbak' ),
       'desc'       => __( 'Login form base settings<br>Preview <a href="'.get_bloginfo('url').'/wp-login.php">Login Page</a>', 'tankenbak' ),
       'id'         => $base_name.'-form',
       'subsection' => true,
       'fields'     => array(
               array(
                   'id'       => $base_name.'-form-shake',
                   'type'     => 'checkbox',
                   'title'    => __('Shake login', 'tankenbak'),
                   'subtitle' => __('Disable or Enable login shake after auth fail', 'tankenbak'),
                   'default' => '1'
               ),
               array(
                   'id'       => $base_name.'-form-error',
                   'type'     => 'checkbox',
                   'title'    => __('Login Error Message', 'tankenbak'),
                   'subtitle' => __('Disable or Enable login error after auth fail', 'tankenbak'),
                   'default' => '1'
               ),
       ),
   ));

   /* Extra */
   Redux::setSection( $opt_name, array(
       'title'      => __( 'Background & CSS ', 'tankenbak' ),
       'subtitle'       => __( 'Settings for logo in login screen', 'tankenbak' ),
       'id'         => $base_name.'-extra',
       'subsection' => true,
       'fields'     => array(
           array(
               'id'       => $base_name.'-page-background',
               'type'     => 'background',
               'url'      => true,
               'title'    => __('Bage Packground', 'tankenbak'),
               'subtitle'     => __('Customize Page background, You can use only color or add background image', 'tankenbak'),
               'preview_media' => true,
               'preview'   => false,
               'default'  => array(
                   'background-color' => '#fff',
                   ),
               ),
       ),
   ));

    /*
     * <--- END SECTIONS
     */

    $base_name = 'css';
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Custom CSS', 'tankenbak' ),
        'id'     => $base_name,
        'permissions' => 'manage_options',
        'icon'   => 'fa fa-css3',
        'fields'     => array(
            array(
                'id'        => $base_name.'-enable',
                'type'      => 'checkbox',
                'title'     => __('Custom Styles', 'tankenbak'),
                'subtitle' => __('Enable or disable custom styles:', 'tankenbak'),
                'options'  => array(
                    '1' => 'Site CSS ',
                    '2' => 'Login CSS',
                    '3' => 'Admin CSS'
                    ),
                'default'  => array(
                    '1' => '0',
                    '2' => '1',
                    '3' => '0'
                    ),
                ),
            ),
    ) );

    /* Front page area CSS*/
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Page CSS', 'tankenbak' ),
        'desc'       => __( 'You can enter your code for page <br>Before compile, SAVE SETTINGS!', 'tankenbak' ),
        'id'         => $base_name.'-page-css',
        'subsection' => true,
        'fields'     => array(
            array(
                    'id'        => $base_name.'-0',
                    'type'      => 'info',
                    'notice' => false,
                    'style' => 'info',
                    'permissions' => 'manage_options',
                    'title' => '<strong style="font-size:20px;">' . __('Compile to CSS File', 'tankenbaks') . '</strong>',
                    'desc' => '<div id="compile_info" ></div>'
                        . '<button id="SaveCode" class="button button-primary">' . __('Save Settings', 'tankenbaks') . '</button> '
                        . '<button id="SaveScssPage" class="button button-primary">' . __('Compile CSS', 'tankenbaks') . '</button> '
                        . '<br/><br/><span style="color: black;">' . __('<b>Note</b>: Click First - "Save Settings" and then "Compile CSS" to compile code you wrote below.', 'tankenbaks') . '</span>'
                ),
            array(
                'id'       => $base_name.'-page-scss',
                'type'     => 'ace_editor',
                'full_width' => true,
                'title'    => __('CSS/SCSS Code', 'tankenbak'),
                'desc'  => 'Dont forget save me, and compile :)',
                'subtitle' => __('Be cerful, using SCSS, if you do somethink wrong you will trigger error.', 'tankenbak'),
                'mode'     => 'scss',
                'theme'    => 'monokai',
                //'desc'     => 'Possible modes can be found at <a href="http://ace.c9.io" target="_blank">http://ace.c9.io/</a>.',
                'default'  => "",
            ),
        ),
    ));

    /* login page CSS*/
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Login Page CSS', 'tankenbak' ),
        'desc'       => __( 'Login form base settings<br>Preview <a href="'.get_bloginfo('url').'/wp-login.php">Login Page</a><br>Before compile, SAVE SETTINGS!', 'tankenbak' ),
        'id'         => $base_name.'-login-css',
        'subsection' => true,
        'fields'     => array(
            array(
                    'id'        => $base_name.'-1',
                    'type'      => 'info',
                    'notice' => false,
                    'style' => 'info',
                    'permissions' => 'manage_options',
                    'title' => '<strong style="font-size:20px;">' . __('Compile to CSS File', 'tankenbaks') . '</strong>',
                    'desc' => '<div id="compile_info" ></div>'
                        . '<button id="SaveCode" class="button button-primary">' . __('Save Settings', 'tankenbaks') . '</button> '
                        . '<button id="SaveScssLogin" class="button button-primary">' . __('Compile CSS', 'tankenbaks') . '</button> '
                        . '<br/><br/><span style="color: black;">' . __('<b>Note</b>: Click First - "Save Settings" and then "Compile CSS" to compile code you wrote below.', 'tankenbaks') . '</span>'
                ),
            array(
                'id'       => $base_name.'-login-scss',
                'type'     => 'ace_editor',
                'full_width' => true,
                'title'    => __('CSS/SCSS Code', 'tankenbak'),
                'desc'  => 'you dont have to use !important to override logo or page background',
                'subtitle' => __('Be cerful, using SCSS, if you do somethink wrong you will trigger error.<br>Preview <a href="'.get_bloginfo('url').'/wp-login.php">Login Page</a>'
                                .'Default SCSS is cool flat TankenBak Login screen', 'tankenbak'),
                'mode'     => 'scss',
                'theme'    => 'monokai',
                //'desc'     => 'Possible modes can be found at <a href="http://ace.c9.io" target="_blank">http://ace.c9.io/</a>.',

                'default' => "\$bg_color: #333333; \n"
                            ."\$form_color: #2B2B2B; \n"
                            ."\$input_bg_color: #3E3E3E; \n"
                            ."\$input_text_color: #C9C9C9; \n"
                            ."\$button_bg: #B81F20; \n"
                            ."body.login{\n    margin: 0;\n    background-color: \$bg_color;\n"
                            ."    #login{ width: 400px; @media(max-width: 450px){width: 100%;}\n"
                            ."        #loginform{\n"
                            ."            background-color: \$form_color;\n"
                            ."            label[for='user_login'], label[for='user_pass']{\n"
                            ."                position:relative;\n"
                            ."                font-size:0;\n"
                            ."                &:before{\n"
                            ."                    position: absolute;\n"
                            ."                    font-family: dashicons;\n"
                            ."                    font-family: dashicons;\n"
                            ."                    font-size: 25px;\n"
                            ."                    top: 17px;\n"
                            ."                    left: 11px;\n"
                            ."                }\n"
                            ."            }\n"
                            ."            label[for='user_login']{\n"
                            ."                &:before{\n"
                            ."                    content: '\\f110';\n"
                            ."                }\n"
                            ."            }\n"
                            ."            label[for='user_pass']{\n"
                            ."                &:before{\n"
                            ."                    content: '\\f160';\n"
                            ."                }\n"
                            ."            }\n"
                            ."            .submit{\n"
                            ."             display: inline-block;\n"
                            ."             width: 100%;\n"
                            ."            .button-primary{\n"
                            ."                background:\$button_bg;\n"
                            ."                border-color: darken(\$button_bg, 18%);\n"
                            ."                -webkit-box-shadow: none;\n"
                            ."                box-shadow: none;\n"
                            ."                border-radius: 0;\n"
                            ."                width: 100%;\n"
                            ."                font-size: 16px;\n"
                            ."                height: auto;\n"
                            ."                padding: 8px 0px;\n"
                            ."                margin-top: 20px;\n"
                            ."                text-shadow: none;\n"
                            ."                -webit-text-shadow: none;\n"
                            ."                -moz-text-shadow: none;\n"
                            ."                transition: all 0.2s ease-in-out;\n"
                            ."                -webkit-transition: all 0.2s ease-in-out;\n"
                            ."                -ms-transition: all 0.2s ease-in-out;\n"
                            ."                -moz-transition: all 0.2s ease-in-out;\n"
                            ."                &:hover{\n"
                            ."                    background-color: #A52023;\n"
                            ."                }\n"
                            ."                }\n"
                            ."            }\n"
                            ."            .input, input[type=checkbox], .login input[type=text]{\n"
                            ."                background: \$input_bg_color;\n"
                            ."                border-color: rgba(255, 255, 255, 0.08);\n"
                            ."                font-weight: 300;\n"
                            ."                color: \$input_text_color;\n"
                            ."                font-size: 16px;\n"
                            ."                padding: 14px 15px 14px 45px;\n"
                            ."            }\n        }\n    }\n}"
            ),
        ),
    ));

    /* Admin area CSS*/
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Admin Page CSS', 'tankenbak' ),
        'desc'       => __( 'You can enter your code for admin area.<br>Before compile, SAVE SETTINGS!', 'tankenbak' ),
        'id'         => $base_name.'-admin-css',
        'subsection' => true,
        'fields'     => array(
            array(
                    'id'        => $base_name.'-2',
                    'type'      => 'info',
                    'notice' => false,
                    'style' => 'info',
                    'permissions' => 'manage_options',
                    'title' => '<strong style="font-size:20px;">' . __('Compile to CSS File', 'tankenbaks') . '</strong>',
                    'desc' => '<div id="compile_info" ></div>'
                        . '<button id="SaveCode" class="button button-primary">' . __('Save Settings', 'tankenbaks') . '</button> '
                        . '<button id="SaveScssAdmin" class="button button-primary">' . __('Compile CSS', 'tankenbaks') . '</button> '
                        . '<br/><br/><span style="color: black;">' . __('<b>Note</b>: Click First - "Save Settings" and then "Compile CSS" to compile code you wrote below.', 'tankenbaks') . '</span>'
                ),
            array(
                'id'       => $base_name.'-admin-scss',
                'type'     => 'ace_editor',
                'full_width' => true,
                'title'    => __('CSS/SCSS Code', 'tankenbak'),
                'desc'  => 'Dont forget save me, and compile :)',
                'subtitle' => __('Be cerful, using SCSS, if you do somethink wrong you will trigger error.', 'tankenbak'),
                'mode'     => 'scss',
                'theme'    => 'monokai',
                //'desc'     => 'Possible modes can be found at <a href="http://ace.c9.io" target="_blank">http://ace.c9.io/</a>.',
                'default'  => "",
            ),
        ),
    ));
    /*
     * <--- END SECTIONS
     */
    $base_name = 'custom-js';
     Redux::setSection( $opt_name, array(
        'title'  => __( 'Custom JS', 'tankenbak' ),
        'id'     => $base_name.'-enable',
        'permissions' => 'manage_options',
        'icon'   => 'fa fa-jsfiddle',
    ));
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Page JS', 'tankenbak' ),
        'desc'       => __( 'You can enter your code below', 'tankenbak' ),
        'id'         => $base_name.'-page',
        'permissions' => 'manage_options',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'        => $base_name.'-enable-js',
                'type'      => 'checkbox',
                'title'     => __('Custom Styles', 'tankenbak'),
                'subtitle' => __('Enable or disable custom styles:', 'tankenbak'),
                'options'  => array(
                    '1' => 'Site JS ',
                    //'2' => 'Login JS',
                    //'3' => 'Admin JS'
                    ),
                'default'  => array(
                    '1' => '0',
                    //'2' => '1',
                    //'3' => '0'
                    ),
                ),
            array(
                'id'       => $base_name.'-page-jq',
                'type'     => 'ace_editor',
                'full_width' => true,
                //'title'    => __('CSS/SCSS Code', 'rxes'),
                //'desc'  => 'Dont forget save me, and compile :)',
                //'subtitle' => __('You can enter custom JS', 'rxes'),
                'mode'     => 'javascript',
                'theme'    => 'chrome',
                //'desc'     => 'Possible modes can be found at <a href="http://ace.c9.io" target="_blank">http://ace.c9.io/</a>.',
                'default'  => "jQuery(document).ready(function($) {\n\n});",
            ),
        ),
    ));


    $base_name = 'customizer';
    Redux::setSection( $opt_name, array(
        'title'  => __( 'More Options', 'tankenbak' ),
        'id'     => $base_name,
        'desc'   => __( 'This Theme have more options, but to see all settings you must use <a href="'.admin_url('/customize.php').'">Customizer</a><br>'
            .'There are settingslike: '
            .'<ul>'
                .'<li><b>Site Identity</b> - '.__( 'Site title, Tagline, Siteicons (app icon and Favicon)', 'tankenbak' ).'</li>'
                .'<li><b>Background Image</b> - '.__( 'This option allow you, to add image as background for whole page, be cerfule.', 'tankenbak' ).'</li>'
                .'<li><b>Menus</b> - '.__( 'Option for menus', 'tankenbak' ).'</li>'
                .'<li><b>Widgets</b> - '.__( 'Customize For widgets elements', 'tankenbak' ).'</li>'
                .'<li><b>Static Front Page</b> - '.__( 'Options for eidt which page should be front, and which should be blog (only for base wordpress functions)', 'tankenbak' ).'</li>'
            .'</ul>'
            .'<a style="btn btn-primary" href="'.admin_url('/customize.php').'">Go to Customize!</a>'
            , 'tankenbak' ),
        'icon'   => 'el el-address-book',
    ) );
    /*
     * <--- END SECTIONS
     */

    /*-----------------------
     * <--- TankenBak Admin Settings
    -----------------------*/
    $base_name = 'wp';
    Redux::setSection( $opt_name, array(
        'title'  => __( 'WP Elements', 'tankenbak' ),
        'id'     => $base_name,
        //'desc'   => __( 'Administration options, Only TankenBak Admin can access there :)', 'tankenbak' ),
        'icon'   => 'el el-wordpress',
        //'permissions' => 'iron_admin',
    ) );
    /* Deregister Elements */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Dashwidgets', 'tankenbak' ),
        'id'         => $base_name.'_dashboard',
        'description' => __('You can Disable specyfic  base dash widgets','tankenbak'),
        'subsection' => true,
        'fields'     => array(
                array(
                    'id'       => $base_name.'_welcome_panel',
                    'type'     => 'switch',
                    'title'    => __('Welcome panel', 'tankenbak'),
                    'default'  => true,
                ),
                array(
                    'id'         => $base_name.'_dashwidgets_unset',
                    'type'       => 'checkbox',
                    'title'      => __('Disable base Wordpesss widgets', 'tankenbak'),
                    'subtitle'   => __('You can disable wordpress base widgets', 'tankenbak'),
                    'options'  => $dash_widgets,
                ),
            ),
    ));

    /* Unsed Base Wordpress Widgets  */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'WP Widgets', 'tankenbak' ),
        'id'         => $base_name.'_WP_Widget',
        'description' => __('You can Disable specyfic base wordpress widget','tankenbak'),
        'subsection' => true,
        'fields'     => array(
                array(
                    'id'         => $base_name.'_widgets_unset',
                    'type'       => 'checkbox',
                    'title'      => __('Disable base Wordpesss widgets', 'tankenbak'),
                    'subtitle'   => __('You can disable wordpress base widgets', 'tankenbak'),
                    'options'  => array(
                        'WP_Widget_Pages' => __('Pages', 'tankenbak'),
                        'WP_Widget_Calendar' => __('Calendar', 'tankenbak'),
                        'WP_Widget_Archives' => __('Archives', 'tankenbak'),
                        'WP_Widget_Links' => __('Links', 'tankenbak'),
                        'WP_Widget_Meta' => __('Meta', 'tankenbak'),
                        'WP_Widget_Search' => __('Search', 'tankenbak'),
                        'WP_Widget_Text' => __('TextWidget', 'tankenbak'),
                        'WP_Widget_Categories' => __('Categories', 'tankenbak'),
                        'WP_Widget_Recent_Posts' => __('RecentPosts', 'tankenbak'),
                        'WP_Widget_Recent_Comments' => __('RecentComments', 'tankenbak'),
                        'WP_Widget_RSS' => __('RSS', 'tankenbak'),
                        'WP_Widget_Tag_Cloud' => __('TagCloud', 'tankenbak'),
                        'WP_Nav_Menu_Widget' => __('CustomMenu', 'tankenbak'),
                        'Twenty_Eleven_Ephemera_Widget' => __('TwentyElevenEphemera', 'tankenbak'),
                    ),
                ),
            ),
    ));


    /* Admin Bar */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Admin Bar', 'tankenbak' ),
        'id'         => $base_name.'_admin_bar',
        'description' => __('Admin Bar options','tankenbak'),
        'subsection' => true,
        'fields'     => array(
                array(
                    'id'       => $base_name.'_admin_bar_show',
                    'type'     => 'switch',
                    'title'    => __('Show Admin Bar', 'tankenbak'),
                    'default'  => true,
                ),
            ),
    ));

    /* Search Bar */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'MISC opt', 'tankenbak' ),
        'id'         => $base_name.'_misch',
        'description' => __('Simple wp options','tankenbak'),
        'subsection' => true,
        'fields'     => array(
                array(
                    'id'       => $base_name.'_search_slug',
                    'type'     => 'switch',
                    'title'    => __('Use /search/ in url insted of ?s=[]', 'tankenbak'),
                    'default'  => true,
                ),
                array(
                    'id'       => $base_name.'_cleanup_head',
                    'type'     => 'switch',
                    'title'    => __('Clean Header', 'tankenbak'),
                    'default'  => true,
                ),
                array(
                    'id'       => $base_name.'_post_status_colors',
                    'type'     => 'switch',
                    'title'    => __('Use colors for displaying post stsus', 'tankenbak'),
                    'default'  => true,
                ),
                array(
                    'id' => $base_name.'_image_quality',
                    'type' => 'slider',
                    'title' => __('Upload image Quality', 'tankenbak'),
                    "default" => 8,
                    "min" => 5,
                    "step" => 1,
                    "max" => 10,
                    'display_value' => 'text'
                ),
                array(
                    'id'       => $base_name.'_remove_comments_links',
                    'type'     => 'switch',
                    'title'    => __('Disable Links from User Comments', 'tankenbak'),
                    'description' => __('Disable automatic wrapping anchor into http://, helping deal with spamers', 'tankenbak'),
                    'default'  => false,
                ),
                array(
                    'id'       => $base_name.'_display_templates',
                    'type'     => 'switch',
                    'title'    => __('Enable displaying templates name', 'tankenbak'),
                    'description' => __('Dispaly template name in page lists', 'tankenbak'),
                    'default'  => true,
                ),
            ),
    ));
    /*
     * <--- END SECTIONS
     */
    /*-----------------------
     * <--- TankenBak Admin Settings
    -----------------------*/
    $base_name = 'admin';
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Admin options', 'tankenbak' ),
        'id'     => $base_name,
        'desc'   => __( 'Administration options, Only TankenBak Admin can access there :)', 'tankenbak' ),
        'icon'   => 'fa fa-empire',
        //'permissions' => 'iron_admin',
    ) );

    /* Deregister Elements */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Deregister CSS & JS', 'tankenbak' ),
        'id'         => $base_name.'-deregister-section',
        'subsection' => true,
        'fields'     => array(
            array(
                    'id'        => $base_name.'-0',
                    'type'      => 'info',
                    'notice' => true,
                    'style' => 'info',
                    'permissions' => 'iron_admin',
                    'title' => '<strong style="font-size:20px;">' . __('How To :', 'tankenbaks') . '</strong>',
                    'desc' => '<span style="color: black;">' . __('<b>Using plugins: </b>: <a href="https://wordpress.org/plugins/debug-bar/">Debug bar</a> and <a href="https://wordpress.org/plugins/debug-bar-list-dependencies/">Debug Bar List Script & Style</a> ', 'tankenbaks') . '</span>'
                ),
            array(
                'id'        => $base_name.'-deregister-css',
                'type'      => 'multi_text',
                'title'     => __('Deregister CSS', 'tankenbak'),
                'subtitle' => __('Add Id element to deregister!! USE CERFULLY', 'tankenbak'),
                //'desc' => __('This is the description field, again good for additional info.', 'tankenbak')
                ),
            array(
                'id'        => $base_name.'-deregister-js',
                'type'      => 'multi_text',
                'title'     => __('Deregister JS', 'tankenbak'),
                'subtitle' => __('Add Id element to deregister!! USE CERFULLY', 'tankenbak'),
                //'desc' => __('This is the description field, again good for additional info.', 'tankenbak')
                ),

            ),
    ));
     Redux::setSection( $opt_name, array(
            'title'      => __( 'Transients', 'tankenbak' ),
            //'desc'       => __( '', 'tankenbak' ),
            'id'         => $base_name.'-transient',
            'subsection' => true,
            'fields'     => array(
                array(
                    'id'=> $base_name.'-flexible-transients',
                    'type' => 'switch',
                    'title' => __('Flexible builder transient', 'tankenbak'),
                    'desc'  => __('Disable transient caching for flexible builder.','tankenbak'),
                    'default' => false,
                    'on' => __('Yes', 'tankenbak'),
                    'off' => __('No', 'tankenbak'),
                ),
                array(
                        'id'        => $base_name.'-0',
                        'type'      => 'info',
                        'notice' => false,
                        'style' => 'info',
                        'permissions' => 'manage_options',
                        'title' => '<strong style="font-size:20px;">' . __('Clear transients', 'tankenbaks') . '</strong><br>'.__('Sometime you want do it if there is some issues on page, but you want keep website fast using transients', 'tankenbak'),
                        'desc' => '<div id="compile_info" ></div>'
                            . '<button id="CleanTransients" class="button button-primary">' . __('Clear TB Builder transients', 'tankenbaks') . '</button> '
                            . '<br/><br/>'
                    ),
                // array(
                //     'id'=> $base_name.'-menu_cache-transients',
                //     'type' => 'switch',
                //     'title' => __('Menu transients', 'tankenbak'),
                //     'desc'  => __('There is issue if you use menus in widget, so disable this transient if you have any problems in widgets menus.','tankenbak'),
                //     'default' => true,
                //     'on' => __('Yes', 'tankenbak'),
                //     'off' => __('No', 'tankenbak'),
                // ),
            ),
        ));



    /*
     * <--- END SECTIONS
     */