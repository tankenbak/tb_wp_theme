<?php

    /**
     * For full documentation, please visit: http://docs.reduxframework.com/
     * For a more extensive sample-config file, you may look at:
     * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "option_admin";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        'opt_name' => 'option_admin',
        'use_cdn' => FALSE,
        'display_name' => 'TankenBak Secret Administrator functions',
        'display_version' => '0.0',
        'page_slug' => 'admin-options',
        'page_title' => 'Administrator Option',
        'update_notice' => FASLE,
        'dev_mode'             => FALSE,
        'intro_text' => '<p>Hello</p>',
        'admin_bar' => FASLE,
        'menu_type' => 'menu',
        'menu_title' => 'Administration',
        'allow_sub_menu' => FASLE,
        'page_parent_post_type' => 'administration',
        'page_priority' => FASLE,
        'page_parent'          => 'options-general.php',
        'customizer' => FASLE,
        'admin_bar_priority'   => 50,
        'default_mark' => '<small>(def)</small>',
        'class' => 'redux_settings admin_settings',
        'hints' => array(
            'icon' => 'el el-idea',
            'icon_position' => 'right',
            'icon_color' => '#eeee22',
            'icon_size' => 'normal',
            'tip_style' => array(
                'color' => 'light',
                'style' => 'youtube',
            ),
            'tip_position' => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect' => array(
                'show' => array(
                    'effect' => 'fade',
                    'duration' => '500',
                    'event' => 'mouseover',
                ),
                'hide' => array(
                    'effect' => 'fade',
                    'duration' => '500',
                    'event' => 'mouseleave unfocus',
                ),
            ),
        ),
        'output' => TRUE,
        'output_tag' => TRUE,
        'settings_api' => TRUE,
        'cdn_check_time' => '1440',
        'compiler' => TRUE,
        'page_permissions' => 'manage_options',
        'save_defaults' => TRUE,
        'show_import_export' => TRUE,
        //'database' => 'options-redux-administrator',
        //'transient_time' => '3600',
        //'network_sites' => TRUE,
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://www.linkedin.com/company/redux-framework',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'tankenbak' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'tankenbak' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'tankenbak' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'tankenbak' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'tankenbak' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*
     * <--- END SECTIONS
     *-----------------------
     * <--- Header Opt
    */


    Redux::setSection( $opt_name, array(
        'title'  => __( 'Header', 'tankenbak' ),
        'id'     => 'header',
      //  'desc'   => __( 'Basic field with no subsections.', TD ),
        'icon'   => 'el el-home',
    ) );

/* megamenu opt */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Megamenu ', TD ),
        'desc'       => __( 'Sonsors Detalis', TD ),
        'id'         => 'megamenu_opt',
        'icon'       => 'el el-screen',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'=>'generate_post_types',
                'type' => 'multi_text',
                'title' => __('Multi Text Option - Color Validated', 'redux-framework-demo'),
                //'validate' => 'color',
                //'subtitle' => __('If you enter an invalid color it will be removed. Try using the text "blue" as a color.  ;)', 'redux-framework-demo'),
                //'desc' => __('This is the description field, again good for additional info.', 'redux-framework-demo')
                ),
        ),
    ));
    /*
     * <--- END SECTIONS
     */
    /*
     * <--- END SECTIONS
     *-----------------------
     * <--- Header Opt
    */



    /*
     * <--- END SECTIONS
     */