<?php

function tankenbak_google_maps_api_key() {
    global $option;
    $apikey = 'AIzaSyAy-GXq3p9GAvKI_1bvolHVatjLq9U-ir8';
    if (isset($option['fc_map_opt-map-api']) && $option['fc_map_opt-map-api'] != '' ){
        $apikey = $option['fc_map_opt-map-api'];
    };
    return $apikey;
}

/********************************************
*           Include scripts & styles.       *
*********************************************/
function tankenbak_scripts() {

    $theme_data = array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'home_page_url' => get_option('home'),
        'theme_url' => get_template_directory_uri(),
        'theme_ajax' => get_template_directory_uri().'/inc/ajax/theme-ajax.php',
    );
    wp_enqueue_script('jquery');
    wp_enqueue_script('theme-scripts', TB_JS . '/min/scripts.min.js', array('jquery'), '1.0.1', true);
    wp_localize_script('theme-scripts', 'theme_data', $theme_data);
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'tankenbak_scripts' );


/********************************************
*       Elements for admin dashboard        *
*     Script & Scripts  Admin Dashboard     *
*********************************************/
function theme_admin_enqueue() {
    $theme_data = array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'img_url' => TB_URI.'/inc/flexible-content/',
    );
    wp_enqueue_script('tb-google-maps', '//maps.google.com/maps/api/js?v=3.24&sensor=false&key='.tankenbak_google_maps_api_key(),array('jquery'), false, true);
    wp_enqueue_script('theme-admin-scripts', TB_JS . '/min/admin.min.js', array('jquery'), '1.1', true );
    wp_localize_script('theme-admin-scripts', 'theme_data', $theme_data);
}
add_action( 'admin_enqueue_scripts', 'theme_admin_enqueue' );


/********************************************
*       Elements for admin dashboard        *
*        Script For theme settings          *
*********************************************/

function theme_admin_enqueue_settings( $hook_suffix ) {
    if($hook_suffix == 'toplevel_page_theme-options') {

        wp_enqueue_script( 'tankenbak-theme-settings', TB_JS . '/min/admin-tankenbak.min.js', array('jquery') , '071220015' , true);
        wp_localize_script('tankenbak-theme-settings', 'TankenbakSettings', array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'security' => wp_create_nonce( 'this-is-ajax-settings' )
        ));

    }
}
add_action( 'admin_enqueue_scripts', 'theme_admin_enqueue_settings' );


/**************************************************
*      LOAD SCRUPTS AS DEFER                      *
* @http://mygenesisthemes.com/wordpress-cache/
***************************************************/
function defer_parsing_of_js ( $url ) {
   if( ! is_admin() ){
      if ( FALSE === strpos( $url, '.js' ) ) return $url;
      if ( strpos( $url, 'jquery.js' ) ) return $url;
      return "$url' defer ";
   }else{
    return $url;
   }
}
add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );

/***********************************
*   All Scripts into wp_footer()   *
************************************/
// need to check
// if(!is_admin()){
//     remove_action('wp_head', 'wp_print_scripts');
//     remove_action('wp_head', 'wp_print_head_scripts', 9);
//     remove_action('wp_head', 'wp_enqueue_scripts', 1);

//     add_action('wp_footer', 'wp_print_scripts', 5);
//     add_action('wp_footer', 'wp_enqueue_scripts', 5);
//     add_action('wp_footer', 'wp_print_head_scripts', 5);
// }


