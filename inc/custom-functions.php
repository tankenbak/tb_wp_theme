<?php
//var_dump('aassda');

/**
 * Custom functions that act independently of the theme templates.
 * Eventually, some of the functionality here could be replaced by core features.
 * @package TankenBak
 */
/**
 * Adds custom classes to the array of body classes.
 * @param array $classes Classes for the body element.
 * @return array
 */
function tankenbak_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	return $classes;
}
add_filter( 'body_class', 'tankenbak_body_classes' );



/************************************************
 * For Konrad local copy function
/************************************************/
function local_copy_classes_tb($classes) {
	//if(is_home() || is_front_page()) $classes[] = 'menu_closed';
	if(strpos($_SERVER['DOCUMENT_ROOT'], 'projects') > 0) {
		$classes[] = 'local_copy';
		add_action('wp_footer', 'local_copy_element_tb');
	}
	//$classes[] = 'TEST';
	return $classes;
}
function local_copy_element_tb(){
  echo '<div class="mqi"></div> ';
}
add_filter('body_class', 'local_copy_classes_tb');



/************************************************
 * Adding custom body classes for BACKEND
/************************************************/
function custom_admin_body_classes( $classes ) {
	global $wpdb, $post;
	if(!$post) return $classes;
	$post_type = get_post_type( $post->ID );
	if ( is_admin() && ( $post_type == 'page' )) {
		$classes .= ' post_type-' . $post_type;
		$classes .= ' post_id-' . $post->ID;
		$classes .= ' post_parent_'.$post->post_parent;
	}
	if(get_option('show_on_front') == 'page' && get_option('page_on_front') == $post->ID) {
		$classes .= ' home_page_post';
	}
	return $classes;
}
add_filter( 'admin_body_class', 'custom_admin_body_classes' );


/********************************************
*        check if it is login page          *
*********************************************/
if(!function_exists('is_login_page')) {
  function is_login_page() {
	  return in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'));
  }
}


/********************************************
*     Convert strings to friendly strins    *
*        Super Duper -> super-duper         *
*********************************************/
if(!function_exists('friendly_name')) {
  function friendly_name($string) {
	  //Lower case everything
	  $string = strtolower($string);
	  //Make alphanumeric (removes all other characters)
	  $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
	  //Clean up multiple dashes or whitespaces
	  $string = preg_replace("/[\s-]+/", " ", $string);
	  //Convert whitespaces and underscore to dash
	  $string = preg_replace("/[\s_]/", "-", $string);
	  return $string;
  }
}

/********************************************
 *              Custom Excerpt              *
 ********************************************/
if(!function_exists('custom_excerpt')) {
	function custom_excerpt($text, $length, $end='') {
		$text = preg_replace('|\[(.+?)\]|s', '', $text);
		$text = strip_tags($text);
		if(strlen($text) > $length) {
			$text = substr($text, 0, $length);
			$space = strrpos($text, ' ');
			$text = substr($text, 0, $space);
			$text .= $end;
		}
		return $text;
	}
}


/**********************************************************************************************
| Custom nawigation using BOOTSTRAP 3
| For usage use in archive.php, etc.
| if ( function_exists('vb_pagination') ) {      vb_pagination();    }
| If cutom loop and custom items per page - use your $querry as argument to vb_pagination!
/**********************************************************************************************/
function vb_pagination( $query=null ) {

  global $wp_query;
  $query = $query ? $query : $wp_query;
  $big = 999999999;

  $paginate = paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'type' => 'array',
	'total' => $query->max_num_pages,
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'prev_text' => __('&laquo;'),
	'next_text' => __('&raquo;'),
	)
  );
  if ($query->max_num_pages > 1) :
	?>
	<ul class="pagination">
	  <?php
	  foreach ( $paginate as $page ) {
		echo '<li>' . $page . '</li>';
	  }
	  ?>
	</ul>
	<?php
	  endif;
}


/********************************************
 *            SOCIAL SHARE LINKS            *
 *   Add to array new if you want use more  *
 *  And you should add new to Redux opt too *
 ********************************************/
if(!function_exists('do_social_share')) {
  function do_social_share( $args = null ){

	$defaults = [
		'class'	=> '',
		'from_options'	=> true,
		'types'	=> ['facebook', 'twitter', 'googleplus', 'linkedin', 'pinterest']
	];
	$shareArgs = wp_parse_args( $args, $defaults );


	global $post, $option;

	$url = get_permalink($post->ID);
	$page_info = get_bloginfo('description');
	$page_title = get_bloginfo('name');
	if (has_post_thumbnail( $post->ID ) ){
	  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
	  $image =  $image[0];
	}else{
	  $image = TB_PLACEHOLDER_IMG;
	}

	$share = '<ul id="page-share-tb" class="page-share '.$shareArgs['class'].'">';
	foreach ($shareArgs['types'] as $name) {
	  if( $shareArgs['from_options'] && $option['social-'.$name.'-share'] != TRUE) continue;
	  switch ($name) {
		case 'facebook':
		  $share_link = 'http://www.facebook.com/share.php?u='.$url.'&title='.$page_title.' - '.$page_info;
		  $name_share = 'Facebook';
		  break;
		case 'twitter':
		  $share_link = 'https://twitter.com/share?url='.$url.'&text='.$page_title.' - '.$page_info;
		  $name_share = 'Twitter';
		  break;
		case 'googleplus':
		  $share_link = 'https://plus.google.com/share?url='.$url;
		  $name_share = 'Google+';
		  break;
		case 'pinterest':
		  $share_link = 'http://pinterest.com/pin/create/button/?url='.$url.'&media='.$image.'&description='.$page_title.' - '.$page_info;
		  $name_share = 'Pin it';
		  break;
		case 'linkedin':
		  $share_link = 'https://www.linkedin.com/shareArticle?mini=true&url='.$url.'&title='.$page_title.' - '.$page_info;
		  $name_share = 'Linkedin';
		  break;
	  }
	  //var_dump($option['social-'.$name.'-share']);
	  $share .= "<li class='share-link {$name}-share'><a class='content-share' href='javascript:void(0);' data-share_link='{$share_link}' data-share_name='{$name_share}', data-share_height='445', data-share_width='600' >{$name_share}</a></li>";
	} // endforeach
	$share .= '</ul>';
	//$facebook_option = '"http://www.facebook.com/share.php?u='.$url.'&title='.$page_info.' - '.$title.'", "Facebook",600,445';
	echo $share;
	?>
	<?php
	return;
  }
}

/***************************************************
 *             LAZY LOAD THUMB IMAGE               *
 * This function generate automaticly Lazy thumbs  *
 * Added Image faker for better loading effect     *
 * Added attribute for displaying 'caption '       *
 ***************************************************/
if(!function_exists('lazy_thumb')) {
	function get_lazy_thumb( $args = null ){
	   $defaults = array(
		  'id'            => null,
		  'post_id'       => null,
		  'proportion'    => '16x9', // 16x9 4x9, 1x1, none,
		  'preloader'     => 'blur', // blur, preloader
		  'type'          => 'div',
		  'wrapper'       => true,
		  'caption'		  => false,
		  'wrapper_class' => 'b-wrapper lazy-wrapper base-border-color ',
		  'item_class'    => 'image-item ',
		  'lightbox'      => false, // can be array if you want use groups for lightbox
		  /*
		  'lightbox' => array(
			'group'  => 'name',
		  ),
		   */
		  'size'     => array(
			  'big'       => 'tb-medium',
			  'medium'    => 'tb-medium',
			  'small'     => 'tb-small',
		  ),
		  'placeholder'   => array(
			  'big'       => TB_PLACEHOLDER_IMG,
			  'medium'    => TB_PLACEHOLDER_IMG,
			  'small'     => TB_PLACEHOLDER_IMG,
		  )
		);

		$thumb_args = wp_parse_args( $args, $defaults );

		$thumb_id = false;
		$figure_caption = '';
		$noscript_html = '';
		$wrapper_figure = null;

		if ( $thumb_args['type'] != 'img' ) {
			$thumb_args['wrapper_class'] .= 'pricture_demension pricture_demension--'.$thumb_args['proportion'].' ';
		}


		//in loop thumb
		if( !$thumb_args['id'] && !$thumb_args['post_id'] ){
			global $post;
			if ( has_post_thumbnail( $post->ID ) ){
				$thumb_id = get_post_thumbnail_id( $post->ID );
			}
		}elseif( $thumb_args['id']  ){
			$thumb_id = $thumb_args['id'];
		}else if($thumb_args['post_id']){
			if ( has_post_thumbnail( $thumb_args['post_id'] ) ){
				$thumb_id = get_post_thumbnail_id( $thumb_args['post_id'] );
			}
		}

		if ( $thumb_id ) {
			$full_image_data = wp_prepare_attachment_for_js(  $thumb_id );
			$sizes = [ $thumb_args['size']['big'], $thumb_args['size']['medium'], $thumb_args['size']['small'] ];
			$image_data = get_image_js_sizes( $full_image_data, $sizes );
			$image_blazy = $image_data['blazy-data'];
			//blur image preload

			$src = null;
			if ( $thumb_args['preloader'] === 'blur' ) {
				$thumb_args['item_class'] .= ' b-blur';
				if (isset( $full_image_data['sizes']['mini-placeholder']['url'] ) ) {
					$src = $full_image_data['sizes']['mini-placeholder']['url'];
				}else{
					$thumb_args['item_class'] .= ' b-thumb';
				}

			}elseif( $thumb_args['preloader'] === 'preloader'  ){
				$thumb_args['item_class'] .= ' b-thumb';
			}
			$noscript_html ="<noscript><img alt='{$full_image_data['alt']}' title='{$full_image_data['title']}'  src='{$full_image_data['url']}' /></noscript>";
			$wrapper_figure['start'] = '<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">';
			$wrapper_figure['end']   = '</figure>';
			$figure_caption          = "<figcaption itemprop='caption description'>{$full_image_data['caption']}<small>{$full_image_data['description']}</small></figcaption>";

		}else{
			$full_image_data = false;
			$image_blazy  =' data-src="'.$thumb_args['placeholder']['big'].'"';
			$image_blazy .=' data-src-medium="'.$thumb_args['placeholder']['medium'].'"';
			$image_blazy .=' data-src-small="'.$thumb_args['placeholder']['small'].'"';
		}

		//Serup wrapper
		if( $thumb_args['wrapper'] && ( $thumb_args['lightbox'] == false ) && $thumb_id ){
			$wrapper_html['start'] = "<div class='{$thumb_args['wrapper_class']}'>";
			$wrapper_html['end']   = "</div>";
		}elseif( ($thumb_args['lightbox'] != false ) && $thumb_id ){
			$group = null;
			if ( isset($thumb_args['lightbox']['group']) ) {
				$group = "data-group='{$thumb_args['lightbox']['group']}'";
			}
			$wrapper_html['start'] = "<a href='{$image_data['largest_thumb']['url']}' data-title='{$full_image_data['title']}<small>{$full_image_data['caption']}' data-source='{$full_image_data['url']}' {$group} data-source_text='". __('Open source')."'  class='lightbox-picture {$thumb_args['wrapper_class']}'>";
			$wrapper_html['end']   = "</a>";
		}elseif( !$thumb_id ){
			$wrapper_html['start'] = "<div class='no-image  {$thumb_args['wrapper_class']}'>";
			$wrapper_html['end']   = "</div>";
		}else{
			$wrapper_html['start'] = null;
			$wrapper_html['end']   = null;
		}

		//check if image exist if not then use placeholder


		if ( $full_image_data ) {
			switch ($thumb_args['type']){
				case 'div':
					$img_faker = "<div style='background-image: url({$src})' class='image-faker'></div>";
					$image['start'] = "<div itemprop='thumbnail' class='{$thumb_args['item_class']}' {$image_blazy}> ";
					$image['end']   ="</div>{$img_faker}";
					break;
				case 'img':
					$image['start'] = "<img itemprop='thumbnail' alt='{$full_image_data['alt']}' title='{$full_image_data['title']}'  class='{$thumb_args['item_class']}' src='{$src}'  {$image_blazy} />";
					$image['end']   = '';
					break;
				default:
					break;
			}
		}else{
			switch ($thumb_args['type']){
				case 'div':
					$image['start'] = "<div class='{$thumb_args['item_class']} b-lazy' {$image_blazy}>";
					$image['end']   ="</div>";
					break;
				case 'img':
					$image['start'] = "<img class='{$thumb_args['item_class']}' src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==  {$image_blazy} />";
					$image['end']   = '';
					break;
				default:
					break;
			}
		}

		if ( $thumb_args['caption'] ) {
			$html = $wrapper_figure['start'].$wrapper_html['start'].$image['start'] .$image['end'] . $noscript_html . $wrapper_html['end'] . $figure_caption . $wrapper_figure['end'];
		}else{
			$html = $wrapper_figure['start'].$wrapper_html['start'].$image['start'] .$image['end'] . $noscript_html . $figure_caption . $wrapper_html['end'] . $wrapper_figure['end'];
		}
		return $html;
	}

	/* Output as html */
	function lazy_thumb( $args = null ){
	  echo get_lazy_thumb( $args );
	}
}

/**
* @since 0.9
*
* @param  $image_data -> all data of image taken by wp_get_attachment_image_src
* @param $sizes array of wanted sizes, big, medium, small
*
* @return string $js_data array
**/
function get_image_js_sizes( $image_data, $sizes = null ){
	if ( !$image_data ) {
		return;
	}
	if ( !$sizes ) {
		$sizes = ['tb-large', 'tb-medium', 'tb-small'];
	}
	$max[] = null;
	$max['height'] = 0;
	foreach ( $image_data['sizes'] as $key => $size) {
		$tmp = $size['height'];
		if( $key === 'full' ) CONTINUE;
		if ( $tmp > $max['height'] ) {
			$max = $size;

		}
		unset( $tem );
	}
	if( $max['height'] == 0){
		$js_data['largest_thumb'] = $image_data['sizes']['full'];
	}else{
		$js_data['largest_thumb'] = $max;
	}
	$js_data['srcset'] = 'Not yet';

	if ( array_key_exists( $sizes[0] , $image_data['sizes'] ) ) {
		$js_data['large']       = $image_data['sizes'][ $sizes[0] ] ;
		$js_data['medium']      = $image_data['sizes'][ $sizes[1] ] ;
		$js_data['small']       = $image_data['sizes'][ $sizes[2] ] ;

		$js_data['blazy-data']  = " data-src='{$js_data['large']['url']}' data-src-medium='{$js_data['medium']['url']}' data-src-small='{$js_data['small']['url']}' ";

	}elseif ( array_key_exists( $sizes[1] , $image_data['sizes'] ) ) {
		// get fallback to get bigggest image if no current size avaible,
		// we dont want to load full image!
		$max[] = null;
		$max['height'] = 0;
		foreach ( $image_data['sizes'] as $key => $size) {
			$tmp = $size['height'];
			if( $key === 'full' ) CONTINUE;
			if ( $tmp > $max['height'] ) {
				$max = $size;
			}
			unset( $tem );
		}

		$js_data['large'] = $max;
		$js_data['medium'] = $image_data['sizes'][ $sizes[1] ] ;
		$js_data['small'] = $image_data['sizes'][ $sizes[2] ] ;
		$js_data['blazy-data']  = " data-src='{$js_data['large']['url']}' data-src-medium='{$js_data['medium']['url']}' data-src-small='{$js_data['small']['url']}' ";

	}elseif ( array_key_exists( $sizes[2] , $image_data['sizes'] ) ) {
		// get fallback to get bigggest image if no current size avaible,
		// we dont want to load full image!
		$max[] = null;
		$max['height'] = 0;
		foreach ( $image_data['sizes'] as $key => $size) {
			$tmp = $size['height'];
			if( $key === 'full' ) CONTINUE;
			if ( $tmp > $max['height'] ) {
				$max = $size;
			}
			unset( $tem );
		}
		$js_data['large'] = $max;
		$js_data['medium'] = $image_data['sizes'][ $sizes[2] ] ;
		$js_data['blazy-data'] = " data-src='{$js_data['large']['url']}' data-medium='{$js_data['medium']['url']}' ";

	}else{
		$js_data['large']       = $image_data['sizes']['full'];
		$js_data['blazy-data']  = " data-src='{$js_data['large']['url']}' ";

	}

	return $js_data;
}
/********************************************
 *            GET ALL POST TYPES            *
 *       Take all awaible post types,       *
 *              and put to array            *
 ********************************************/
function get_post_types_tb( $args = null ) {

	$defaults = array(
	  'public'    => true,
	  '_builtin'  => false,
	  'ignored'   => array()
	);
	$types_args = wp_parse_args( $args, $defaults );

	$output = 'object';
	$operator = 'and';
	$post_types = get_post_types( $types_args, $output, $operator );
	$ignore = $types_args['ignored'];

	foreach ( $post_types as $post_type ) {
		$exclude = $ignore;
		if( TRUE === in_array( $post_type->name, $exclude )   )  continue;
		  $types[$post_type->labels->singular_name] = $post_type->name;
	}

	return array_unique($types);
}


/********************************************
 *          GET PAGE OR POST TYPE           *
 *          TAKE POST TYPE OR OUTPUT        *
 *      OTHER TYPES LIKE FRONT PAGE...      *
 ********************************************/
function get_page_type_tankenbak(){
  global $post;
  $page_type =null;
  if(is_front_page() && !is_home()){ // in case it is front page for load template
	$page_type = 'front_page';
  }elseif(is_home()){ // its blog page
	$page_type = 'blog_page';
  }elseif(is_search()){ // options for search
	$page_type = 'search';
  }elseif(is_404()){ // options for 404
	$page_type = '404';
  }elseif(is_singular()){ // load  layout option based on post type
	$page_type = get_post_type($post->ID);
  }else{
	$page_type = 'default';
  }
  return $page_type;
}

/********************************************
 *       CUSTOM FUNCTION FOR GET SLUG      *
 ********************************************/

function get_the_slug( $id=null ){
  if( empty($id) ):
	global $post;
	if( empty($post) )
	  return ''; // No global $post var available.
	$id = $post->ID;
  endif;

  $slug = basename( get_permalink($id) );
  return $slug;
}
/********************************************
 * Display the page or post slug
 * Uses get_the_slug() and applies 'the_slug' filter.
 ********************************************/
function the_slug( $id=null ){
  echo apply_filters( 'the_slug', get_the_slug($id) );
}




function get_page_classes_tb( $template_data ){
	$class = array(
	  'page' => null,
	  'container' => null,
	  'sidebar' => null,
	  );

	$post_type = get_page_type_tankenbak();

	$class['page']['class'] = 'page '.$post_type;
	$class['container']['class'] = null;
	$class['sidebar']['class'] = null;

	$sidebar_pull = ' col-sm-pull-8 col-md-pull-9';
	$sidebar_push = ' col-sm-push-8 col-md-push-9';

	$sidebar        = ' col-sm-4 col-md-3 col-xs-12';
	$container      = ' col-sm-8 col-md-9 col-xs-12';
	$container_pull = ' col-sm-pull-4 col-md-pull-3';
	$container_push = ' col-sm-push-4 col-md-push-3';


	if( $template_data['layout']['sidebar'] ){
		$class['sidebar']['type'] = 'first';  // load sidebar as first or second div in content block !
		switch ($template_data['layout']['sidebar_position']):
			case 'left':
			default:
				$class['container']['class'] = $container ;
				$class['sidebar']['class']   = $sidebar;
				if($template_data['layout']['sidebar_collapse'] == 'bottom'):
					$class['sidebar']['type'] = 'second';
					$class['container']['class'] .= $container_push;
					$class['sidebar']['class']   .= $sidebar_pull;
				endif;
			break;
			case 'right':
				$class['container']['class'] = $container ;
				$class['sidebar']['class']   = $sidebar;
				$class['sidebar']['type'] = 'second';
				if($template_data['layout']['sidebar_collapse'] == 'top'):
					$class['sidebar']['type'] = 'first';
					$class['container']['class'] .= $container_pull;
					$class['sidebar']['class']   .= $sidebar_push;
				endif;
			 break;
		endswitch;
	}else{
		$class['container']['class'] = '';
	}

	return $class;
}
/********************************************
 *                QR Code link              *
 ********************************************/
function get_qr_link($id = null, $qr_size = '150x150'){
	if($id = null){
		global $post;
		$id = $post->ID;
	}
	$api_link = "http://api.qrserver.com/v1/create-qr-code/";
	//$qr_size = "150x150";
	$data = get_the_permalink($id);
	$title = get_the_title($id);

	$qr_link = "<img src='{$api_link}?size={$qr_size}&data={$data}' title='{$title}' alt='QR Code for {$title}' />";
	return $qr_link;
}

/********************************************
 *    GET OLDEST POST ID from post type     *
 ********************************************/
if(!function_exists('get_oldest_post_id')) {
  function get_oldest_post_id($posttype){
	  $my_post = null;
	  $args = array(
		  'numberposts'     => 1,
		  'offset'          => 0,
		  'orderby'         => 'post_date',
		  'order'           => 'ASC',
		  'post_type'       => $posttype,
		  'post_status'     => 'publish',
		  'no_found_rows'   => true,
		  'update_post_term_cache' => false,
		  'fields'          => 'ids'
		  );
	  $my_post = get_posts( $args );
	  return $my_post[0];
  }
}

/*************************
*   GENERATE SORT LINK   *
**************************/
function tankenbak_sort_order($order, $orderby){
	$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$before = null;
	if(isset($_GET) ) if($_GET != null) $before = '&'; else  $before = '?';
	if(isset($_GET['order']) && isset($_GET['orderby']) ){
		$href = $actual_link ;
		$query = parse_url( $href, PHP_URL_QUERY );
		parse_str( $query, $params );
		$params['order'] = $order;
		$query = http_build_query( $params );
		// build url
		$query = explode( '?', $href )[0] . '?' . $query;
		//var_dump($query);
		echo $query;
	}else{
		$order = $actual_link.$before.'order='.$order.'&orderby='.$orderby;
		echo $order;
	}
}

/**
 * Function for menaging loading elements
 *
 * @since 0.4
 * @link #tankenbak_get_template
 *
 * @param $args - array of data for loading templates - read more in docs.
 *
 * @since 0.8.6
 * Removed unnecessery elements
 */
function tankenbak_get_template( $args = null ){
   $defaults = array(
	  'content'           => '',
	  'sidebar'           => false,
	  'sidebar_name'      => 'sidebar-1',
	  'sidebar_position'  => 'left',
	  'sidebar_collapse'  => 'bottom',
	  'navigation'        => false,
	  'comments'          => false,
	);
	$r = wp_parse_args( $args, $defaults );

	return $r;
}

/**
 * Based on get_template_part function.
 * Fires before the specified template part file is loaded.
 * Custom function to get template parts
 * And passings data to it in varrible $data
 *
 * @since 0.8.3
 *
 * @link #get_template_part_tb
 *
 * @param string $slug The slug name for the generic template.
 * @param string $name The name of the specialized template.
 * @param varible $template_data , can be array The data is passed to loaded tempalte part.
 *
 * @return information if file dosnt exist or require template part, and datas in varrible $template_data
 */
if ( !function_exists('get_template_part_tb') ) {
	function get_template_part_tb( $slug, $name = null, $template_data = null ){

		do_action( "get_template_part_{$slug}", $slug, $name );

		$templates = array();
		$name = (string) $name;
		if ( '' !== $name ){
			$templates[] = "{$slug}-{$name}.php";
		}

		$templates[] = "{$slug}.php";
		$part = locate_template( $templates );
		if ( '' != $part ) {
		   require $part;
		}else{
			echo "<pre class='debug error'> Can't localize:<strong> {$templates[0]} </strong></pre>";
		}
	}
}
/**
 * Functions for loading parts
 * Based on get_template_part_tb function.
 * @since 0.8.6
 * @link #get_template_part_loop
 * @link #get_template_part_content
 * @link #get_template_part_part
 * @link #get_template_part_acf
 * @link #get_template_part_flexible
 *
 * @param string $slug The slug name for the generic template.
 * @param string $name The name of the specialized template.
 * @param varible $template_data , can be array The data is passed to loaded tempalte part.
 *
 * @return view of template loop parts
 */
if ( !function_exists('get_template_part_loop') ) {
	function get_template_part_loop( $slug, $name = null, $template_data = null ){
	  get_template_part_tb( 'template/template-loops/'.$slug, $name, $template_data );
	}
}
if ( !function_exists('get_template_part_content') ) {
	function get_template_part_content( $slug, $name = null, $template_data = null ){
	  get_template_part_tb( 'template/template-contents/'.$slug, $name, $template_data );
	}
}
if ( !function_exists('get_template_part_part') ) {
	function get_template_part_part( $slug, $name = null, $template_data = null ){
	  get_template_part_tb( 'template/template-parts/'.$slug, $name, $template_data );
	}
}
if ( !function_exists('get_template_part_acf') ) {
	function get_template_part_acf( $slug, $name = null, $template_data = null ){
	  get_template_part_tb( 'template/template-acf/'.$slug, $name, $template_data );
	}
}
if ( !function_exists('get_template_part_flexible') ) {
	function get_template_part_flexible( $slug, $name = null, $template_data = null ){
	  get_template_part_tb( 'inc/flexible-content/'.$slug, $name, $template_data );
	}
}

/**
 * Based on default get_sidebar
 * Fires before the sidebar template file is loaded.
 *
 * The hook allows a specific sidebar template file to be used in place of the
 * default sidebar template file. If your file is called sidebar-new.php,
 * you would specify the filename in the hook as get_sidebar( 'new' ).
 *
 * @since 0.8.5
 *
 * @link #get_sidebar_tb
 *
 * @param string $name Name of the specific sidebar file to use.
 * @param varible $template_data , can be array The data is passed to loaded tempalte part.
 *
 * @return information if file dosnt exist or require template part, and datas in varrible $template_data
 */
function get_sidebar_tb( $name = null, $template_data = null ){

	do_action( 'get_sidebar', $name );

	$templates = array();
	$name = (string) $name;
	if ( '' !== $name )
		$templates[] = "sidebar.php";

	$sidebar = locate_template( $templates );
	if ( '' != $sidebar ) {
	   require $sidebar;
	}else{
		echo "<span class='debug error'> Cant localize sidebar: {$templates[0]} <span>";
	}
}
/*****************************************************************
*           GENERATE LIST OF ALL AWAIBLE WP DASH WIDGETS         *
******************************************************************/
/**
 * function for adding to option list of dashwidgets,
 * option name is "tb_rsx_awaible_dashwidgets", function is called
 * in dashboard  home.
 *
 * @since 0.8.4
 *
 * @link #manage_dash_setup_tb
 *
 */
function manage_dash_setup_tb($meta_boxes) {
	global $wp_meta_boxes, $wpdb;
	if ( is_array($wp_meta_boxes['dashboard']) ) {
		$opt_widgets = get_option( 'tb_rsx_awaible_dashwidgets', false );
		if( $opt_widgets ){
			update_option( 'tb_rsx_awaible_dashwidgets', $wp_meta_boxes['dashboard'], '', 'no' );
		}else{
			add_option( 'tb_rsx_awaible_dashwidgets', $wp_meta_boxes['dashboard'], '', 'no' );
		}
	}
}
add_action('wp_dashboard_setup', 'manage_dash_setup_tb');

/**
 * Function to list dash widgets from db option "tb_rsx_awaible_dashwidgets"
 * If option dont exist it will set default wp widgets (wp widtegs 4.0+ )
 *
 * @since 0.8.4
 *
 * @link #get_das_widgets_list_tb
 *
 * @return array of dashwidgets
 */
function get_das_widgets_list_tb(){
	$opt_widgets = get_option( 'tb_rsx_awaible_dashwidgets', false );
	$dash_widgets = array();

	if($opt_widgets){
		foreach ($opt_widgets['normal']['core'] as $key => $widget_id) {
			$dash_widgets[ $widget_id['id'] ]['id'] =  $widget_id['id'];
			$dash_widgets[ $widget_id['id'] ]['callback'] =  $widget_id['callback'];
			$dash_widgets[ $widget_id['id'] ]['title'] =  $widget_id['title'];
			$dash_widgets[ $widget_id['id'] ]['type'] =  'normal';
		}
		foreach ($opt_widgets['side']['core'] as $key => $widget_id) {
			$dash_widgets[ $widget_id['id'] ]['id'] =  $widget_id['id'];
			$dash_widgets[ $widget_id['id'] ]['callback'] =  $widget_id['callback'];
			$dash_widgets[ $widget_id['id'] ]['title'] =  $widget_id['title'];
			$dash_widgets[ $widget_id['id'] ]['type'] =  'side';
		}
	}else{
			$dash_widgets['dashboard_right_now']['id'] =  'dashboard_right_now';
			$dash_widgets['dashboard_right_now']['callback'] = 'wp_dashboard_right_now';
			$dash_widgets['dashboard_right_now']['title'] =  'At a Glance';
			$dash_widgets['dashboard_right_now']['type'] =  'normal';

			$dash_widgets['dashboard_activity']['id'] =  'dashboard_activity';
			$dash_widgets['dashboard_activity']['callback'] = 'wp_dashboard_site_activity';
			$dash_widgets['dashboard_activity']['title'] =  'Activity';
			$dash_widgets['dashboard_activity']['type'] =  'normal';

			$dash_widgets['dashboard_quick_press']['id'] =  'dashboard_quick_press';
			$dash_widgets['dashboard_quick_press']['callback'] = 'wp_dashboard_quick_press';
			$dash_widgets['dashboard_quick_press']['title'] =  'Quick Draft';
			$dash_widgets['dashboard_quick_press']['type'] =  'side';

			$dash_widgets['dashboard_primary']['id'] =  'dashboard_primary';
			$dash_widgets['dashboard_primary']['callback'] = 'wp_dashboard_primary';
			$dash_widgets['dashboard_primary']['title'] =  'WordPress News';
			$dash_widgets['dashboard_primary']['type'] =  'side';
	}
	return $dash_widgets;
}

/**
 * Similar function to get_das_widgets_list_tb but return values
 * for use in option for redux framework
 * [name] => "title"
 *
 * @since 0.8.4
 *
 * @link #get_das_widgets_list_tb_opt
 *
 * @return array of dashwidgets and names
 */
function get_das_widgets_list_tb_opt(){
	$dash_list = array();
	$opt_widgets = get_option( 'tb_rsx_awaible_dashwidgets', false );
	if($opt_widgets){
		foreach ($opt_widgets['normal']['core'] as $key => $widget_id) {
			$dash_list[ $widget_id['id'] ] = $widget_id['title'];
		}
		foreach ($opt_widgets['side']['core'] as $key => $widget_id) {
			$dash_list[ $widget_id['id'] ] = $widget_id['title'];
		}
	}else{
			$dash_widgets['dashboard_right_now']    =  'At a Glance';
			$dash_widgets['dashboard_activity']     =  'Activity';
			$dash_widgets['dashboard_quick_press']  =  'Quick Draft';
			$dash_widgets['dashboard_primary']      =  'WordPress News';
	}
	return $dash_list;
}



/**
 *
 *
 */
/*function login_test(){

  if (is_user_logged_in()) { ?>
	<a class="login_button" href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a>
<?php } else { ?>
	<a class="login_button" id="show_login" href="">Login</a>
	<form id="login" action="login" method="post">
		<h1>Site Login</h1>
		<p class="status"></p>
		<label for="username">Username</label>
		<input id="username" type="text" name="username">
		<label for="password">Password</label>
		<input id="password" type="password" name="password">
		<a class="lost" href="<?php echo wp_lostpassword_url(); ?>">Lost your password?</a>
		<input class="submit_button" type="submit" value="Login" name="submit">
		<a class="close" href="">(close)</a>
		<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
	</form>
<?php } ?>


<?php

}

function ajax_login_init(){

	wp_register_script('ajax-login-script', get_template_directory_uri() . '/js/min/ajax-login.min.js', array('jquery') );
	wp_enqueue_script('ajax-login-script');

	wp_localize_script( 'ajax-login-script', 'ajax_login_object', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'redirecturl' => home_url( $_SERVER['REQUEST_URI'] ),
		'loadingmessage' => __('Sending user info, please wait...')
	));
	// Enable the user with no privileges to run ajax_login() in AJAX
	add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
}
// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
   // add_action('init', 'ajax_login_init');
}

function ajax_login(){

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	// Nonce is checked, get the POST data and sign user on
	$info = array();
	$info['user_login'] = $_POST['username'];
	$info['user_password'] = $_POST['password'];
	$info['remember'] = true;

	$user_signon = wp_signon( $info, false );
	if ( is_wp_error($user_signon) ){
		echo json_encode( array( 'loggedin'=>false, 'message'=> __('Wrong username or password.') ) );
	} else {
		echo json_encode( array( 'loggedin'=>true, 'message'=> __('Login successful, redirecting...') ) );
	}

	die();
}
*/

  //$current_user = wp_get_current_user();
 //$current_user->add_cap( 'vp_vakt' );
  // $testu_user = get_user_by('ID',1 );
  // $testu_user->add_cap( 'vp_vakt' );
  //$roles = $current_user->roles;

  // echo "<pre style='position: fixed; overflow: scroll; z-index: 9999; top: 0; height: 100%; background: #fff'>";

  // echo "</pre>";



function marker_prepare_text($text) {
	$text = str_replace('"', '\"', $text);
	$text = str_replace("'", "\'", $text);
	$text = str_replace(array("\n", "\r"), '<br>', $text);
	$text = str_replace(array("<p>", "</p>"), '', $text);
	return $text;
}
function marker_prepare_content($content) {
	$content = str_replace('"', '\"', $content);
	$content = str_replace("'", "\'", $content);
	$content = str_replace(array("\n", "\r"), '', $content);
	return $content;
}


/**
 * Function for returning array of posts statuses that should be displayed in querry,
 * Function return only publish status for users, that cant publish posts.
 *
 * @since 0.9
 *
 * @link #get_post_statues_for_query
 *
 * @return array of posts status
 */
function get_post_statues_for_query(){
	$status = array( 'publish' );
	$user = wp_get_current_user();
	if ( $user->exists() && $user->allcaps['publish_posts'] ) {
		$status = array( 'publish', 'pending', 'draft', 'future', 'private' );
	}
	return $status;
}




/**
 * Test funciton to get posts without WP Query.
 *
 * @since 0.9
 *
 * @link #test_query_products
 *
 * @var
 * $args - array of some querry data
 * $extra - array of additional info
 *
 * Known issues:
 * There is per_page -1 !
 * Order by meta value is not ready YET !!!!!
 *
 * @return post object
 */
function new_query_test( $args = null, $extra = null ){
	$before = microtime(true);
	$default_extra = array(
		'meta'		=>	false,
		'thumb'		=>	false,		//not ready yet
		'comments'	=>	false,		//not ready yet
	);
	$default_data = array(
		'post_type'		=>	array('posts'),
		'post_status'	=>	array('publish'),
		'order'			=>	'DESC',
		'orderby'		=>	'post_date'

	);
	// we dont want querry option it it's necessery
	if( isset( $args['per_page'] ) && $args['per_page'] != '' ){
		$default_data['per_page'] = $args['per_page'];
	}else{
		$default_data['per_page'] = get_option('posts_per_page');
	}

	//prepare data for query
	$post_data = wp_parse_args( $args, $default_data );
	// prepare post types
	$post_type = array_map( 'esc_sql', (array) $post_data['post_type'] );
	$post_type = "'" . implode( "', '", $post_type ) . "'";

	// prepare post status
	$post_status = array_map( 'esc_sql', (array) $post_data['post_status'] );
	$post_status = "'" . implode( "', '", $post_status ) . "'";

	$post_orderby = 	esc_sql($post_data['orderby']);
	$post_order = 	esc_sql($post_data['order']);
	//Start making of query
	global $wpdb;
	$query = $wpdb->prepare("
		SELECT $wpdb->posts.*
		FROM $wpdb->posts
		WHERE $wpdb->posts.post_status IN ({$post_status})
		AND $wpdb->posts.post_type IN({$post_type})
		AND $wpdb->posts.post_date < NOW()
		ORDER BY $wpdb->posts.{$post_orderby} {$post_order}
		LIMIT %d
	",
		$post_data['per_page'] // limit data
	);
	$post_object = $wpdb->get_results($query);


	//EXTRA ELEMENTS
	$extra_data = wp_parse_args( $extra, $default_extra );

		if( $extra_data['meta'] ){
		$ids = null;
		foreach ($post_object as $product) {
			$ids .= ','.$product->ID;
		}
		$ids = substr($ids, 1);
		$meta = "
			SELECT $wpdb->postmeta.post_id, $wpdb->postmeta.meta_key, $wpdb->postmeta.meta_value
			FROM $wpdb->postmeta
			WHERE $wpdb->postmeta.post_id IN( {$ids} )
		";
		$meta_tmp = $wpdb->get_results($meta);
		$meta_data = array();
		// merrage object and array
		foreach ($meta_tmp as $meta) {
			$meta_data[ $meta->post_id ][ $meta->meta_key ] = $meta->meta_value;
		}

		foreach ($post_object as $product) {
			$product->meta = $meta_data[$product->ID];
		}
	}


	$after = microtime(true);
	echo ($after-$before) . "  script\n";
	return $post_object;
}



// +- 0.15s
function test_normal_querry(){

	$before = microtime(true);
	$querry = new WP_Query( array(
			'post_type'      => 'post',
			'posts_per_page' => -1,
		) );
	$after = microtime(true);
	echo ($after-$before) . "  script\n";

}

//add_action( 'pre_get_posts'  , 'hooks_setup' );
// http://presscustomizr.com/snippet/three-techniques-to-alter-the-query-in-wordpress/
//add_action( 'template_redirect', 'hooks_setup' , 20 );
function hooks_setup() {
	global $wp_query;
	// if ( !is_home() ) //<= you can also use any conditional tag here
	// 	return;
	remove_all_actions( '__before_loop');
	remove_all_actions( '__after_loop' );
}

function set_my_query() {
	var_dump('efsf');
	global $wp_query, $wp_the_query;
	switch ( current_filter() ) {
		case '__before_loop':
			//replace the current query by a custom query
			//Note : the initial query is stored in another global named $wp_the_query
			$wp_query = new WP_Query( array(
				'post_type'         => 'page',
				'post_status'       => 'publish',
			   //others parameters...
			) );
		break;

		default:
			//back to the initial WP query stored in $wp_the_query
			$wp_query = $wp_the_query;
		break;
	}
}

// ******************************************
// Format Phone Number
// ******************************************
function localize_us_number($phone) {
  $numbers_only = preg_replace("/[^\d]/", "", $phone);
  return preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $numbers_only);
}

/**
 * Take from post excerpt by ID or Content if there is no excerpt;
 * default length is 300 of emd is used or if is used content insted of
 * excerpt  length is also set as 300
 *
 * @param  int $post_id
 * @param  int $length     excerpt wanted length
 * @param  string $end     end of string
 * @return string         [description]
 */

function get_post_excerpt( $post_id = null, $length = null, $end = null ){
	if( !$post_id ){
		global $post;
		$post_id = $post->ID;
	}
	$excerpt =  get_post_field( 'post_excerpt', $post_id );

	if ( !$excerpt || $excerpt == '') {
			$excerpt =  get_post_field( 'post_content', $post_id );
			if ( !$length ) {
				$length = 300;
			}
			if( $end == null){
				$end = '...';
			}
	}

	if ( $length || $end ) {
		if ( !$length ) {
			$length = 300;
		}
		$excerpt = custom_excerpt($excerpt, $length, $end );
	}

	return $excerpt;
}



//add_action('admin_footer', 'require_featured_images_by_jq');
function require_featured_images_by_jq(){
	global $typenow;
	$error_msg = __('Please set featured image', 'tankenbak');
	if (in_array($typenow, array( 'post' ))){
		?>
		<style id="smal-helper-featured-required">
		.is_required{
			outline: 2px solid #EF3340;
		}
		</style>
		<script language="javascript" type="text/javascript">
			jQuery(document).ready(function($) {
				jQuery('#post').submit(function() {
					if (jQuery("#set-post-thumbnail").find('img').size() > 0) {
						jQuery('#ajax-loading').hide();
						jQuery('#publish').removeClass('button-primary-disabled');
						return true;
					}else{
						alert('<?php echo $error_msg ?>');
						jQuery('#postimagediv').addClass('is_required');
						jQuery('#ajax-loading').hide();
						jQuery('#publish').removeClass('button-primary-disabled');
						return false;
					}
					return false;
				});
			});
		</script>

		<?php
	}
}



function wp_get_attachment_meta( $attachment_id ) {
	$attachment = get_post( $attachment_id );
	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}




function add_video_embed_elements($html, $url, $attr) {
	$attr['class']	=	'class="b-lazy"';
	 if ( !is_admin() && strpos($html, 'iframe' ) !== false) {
		$html = str_replace( 'src', $attr['class'].' data-src', $html );
		$html = str_replace('feature=oembed', 'feature=oembed&#038;rel=0', $html);
		return "<div class='iframe-flexible-container b-wrapper'>".$html."</div>";
	 } else {
		return $html;
	 }

}
add_filter('embed_oembed_html', 'add_video_embed_elements', 10, 3);


// function remove_unnecessery_metaboxes() {
// 	remove_meta_box( 'tagsdiv-post_tag', 'post', 'side' );
// 	remove_meta_box( 'formatdiv', 'post', 'side' );
// 	remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
// 	remove_submenu_page('themes.php', 'widgets.php');

// }

// function remove_posts_tags_column( $columns ) {

//   unset($columns['tags']);
//   //unset($columns['categories']);
//   unset($columns['comments']);

//   return $columns;
// }

// add_action( 'admin_menu', 'remove_unnecessery_metaboxes' );

// function remove_unnecessery_elements_forposts(){

// 	add_filter( 'manage_posts_columns' , 'remove_posts_tags_column' );
// }

// add_action( 'admin_init' , 'remove_unnecessery_elements_forposts' );

// $args = array(
// 	'per_page'	=> 3,
// 	'post_type'	=> array('post'),
// 	);
// $extra = array(
// 	'meta'		=> true,
// 	);
// var_dump( new_query_test($args, $extra) );



add_action( 'wp_enqueue_scripts', 'deregsiter_wp_materialize_for_non_logged', 99 );

/**
 * Deregister style for mateiial WP when user is not logged in.
 */
function deregsiter_wp_materialize_for_non_logged() {
	if ( !is_user_logged_in() && !is_admin() && $GLOBALS['pagenow'] !== 'wp-login.php' ) {
		wp_deregister_style( 'material-wp_dynamic' );
	}
}