<?php

/**
 * Implement the Custom Header feature.
 * Disabled becaouse dont work if some one upload SVG Image!
 */
// require get_template_directory() . '/inc/custom-header.php';

/********************************************
*          Add file for scripts             *
*********************************************/
require ( TB_LIB.'/enque-scripts.php' );

/********************************************
*          Add file for Styles              *
*********************************************/
require ( TB_LIB.'/enque-styles.php' );

/********************************************
*    Set up wordpress sidebars areas        *
*********************************************/
require ( TB_LIB.'/sidebars.php' );

/********************************************
*        Set up wordpress widgets           *
*********************************************/
require ( TB_LIB.'/widgets.php' );


/********************************************
*       Tiny MCE extends functions          *
*********************************************/
require ( TB_LIB.'/tinyMCE.php' );

/********************************************
*       File for create post types          *
*********************************************/
require ( TB_LIB.'/create_post_types.php' );

/*********************************************
*        Custom functions that act           *
*   independently of the theme templates.    *
**********************************************/
require ( TB_LIB.'/custom-functions.php' );

/*********************************************
*           Functions for views              *
**********************************************/
require ( TB_LIB.'/view-functions.php' );

/********************************************
*  Custom template tags for this theme.     *
*********************************************/
require ( TB_LIB.'/template-tags.php' );

/********************************************
*  Function based on redux settings         *
*********************************************/
require ( TB_LIB.'/redux-function.php' );

/********************************************
*        Customizer additions.              *
*********************************************/
require ( TB_LIB.'/customizer.php' );

/********************************************
*          Ajax functions                   *
*********************************************/
require ( TB_LIB.'/ajax-functions.php' );

/********************************************
*       shortcodes functions                *
*********************************************/
require ( TB_LIB.'/shortcodes-functions.php' );

/********************************************
*       transients functions                *
*********************************************/
require ( TB_LIB.'/transients-functions.php' );

/********************************************
*   Removing and adding hooked elements     *
*********************************************/
require ( TB_LIB.'/remove-add-functions.php' );

/********************************************
*     Extend wordpress standard gallery     *
*********************************************/
require ( TB_LIB.'/gallery-extend/media-gallery-extend.php' );


/********************************************
*     BUILD IN PLUGINS INIT FUNCTIONS       *
*********************************************/
require ( TB_LIB.'/build-in-plugins/plugins-functions.php' );

/*
* Load redux Admin elements and redux etc.
* And load Redux only for Administrator or in Admin panel.
*/

function tb_rsx_declarate_glboal() {
    global $option;
    $option = get_option('option');
    if( isset( $_GET['dump_opt'] ) ){
        echo "<pre>";
        print_r( $option );
        echo "</pre>";
    }
}
if( is_admin() /*|| (current_user_can( 'manage_options' ))*/ ){
    if ( file_exists( TB_ADMIN.'/admin-init.php' ) ) {
        require_once TB_ADMIN.'/admin-init.php';
    }
}else{

    add_action( 'init', 'tb_rsx_declarate_glboal' );
}


/********************************************
*    EXTENSIONS/FUNCTIONS FOR ACF PLUGIN    *
*********************************************/

if( class_exists('acf') ) {
/*   Load ACF Custom Function.   */
    require ( TB_ACF.'/acf-functions.php' );

}


/********************************************
*  EXTENSIONS/FUNCTIONS FOR VisualComposer  *
*********************************************/
if( function_exists('add_shortcode_param') && function_exists('vc_map') ) {
/*   Load Visual Composer Custom Function.   */
    require ( TB_VC.'/vc-functions.php' );
}

/********************************************
*   EXTENSIONS/FUNCTIONS FOR WooCommerce    *
*********************************************/
/**
| Check if WooCommerce is active
 **/
if ( class_exists( 'WooCommerce' ) ) {
/*   Load WooCommerce Custom Function.   */
    require ( TB_WOO.'/woo-functions.php' );
}

/********************************************
*     EXTENSIONS/FUNCTIONS FOR BBPress      *
*********************************************/
// Check if bbPress plugin is loaded
if ( class_exists( 'bbPress' ) ) {
    // bbPress is enabled so let's begin
}

/********************************************
*     EXTENSIONS/FUNCTIONS FOR Jetpack      *
*********************************************/
// Check if Jetpack's Sharing module is available
if ( function_exists( 'sharing_display' ) ) {
/*   Load Jetpack compatibility file.  */
    require ( TB_JP.'/jetpack.php' );
}







