<?php
/*
    If you use short init by passing argument shortinit, the best way is now using
    WP custom queries using $wpdb, you have access to theme file ajax-functions.php, no more.
    You have base and a bit more wp components loaded, like get_meta, current user .. etc.
*/
    if (!isset( $_POST['action']))
        die('-1');

    if ( isset( $_POST['shortinit']) ){
        $type = trim($_POST['shortinit']);
        if ( $type === 'true' ) {
               define('SHORTINIT',true);
        }
    }

    //mimic the actuall admin-ajax
    define('DOING_AJAX', true);

    error_reporting( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING | E_RECOVERABLE_ERROR );



    $wp_load = '../../../../../wp-load.php';
    if ( file_exists( $wp_load ) ) {
        require_once( $wp_load );
    }else{
        die();
    }



    //Typical headers
    header('Content-Type: text/html');
    send_nosniff_header();

    //Disable caching
    header('Cache-Control: no-cache');
    header('Pragma: no-cache');

    $action = trim($_POST['action']);
    //$action = 'ajx_tester';

    //A bit of security
    $allowed_actions = array(
        //'ajax_custom',
        'builder_load_posts',
        'ajx_tester'
    );




    //For logged in users
   // add_action('super_duper_ajax_custom', 'handler_ajax_text');
    add_action('super_duper_ajx_tester', 'ajx_tester');
    add_action('super_duper_builder_load_posts', 'builder_load_posts');

    //For guests
    //add_action('super_duper_nopriv_ajax_custom', 'handler_ajax_text');
    add_action('super_duper_nopriv_ajx_tester', 'ajx_tester');
    add_action('super_duper_nopriv_builder_load_posts', 'builder_load_posts');


    if(in_array($action, $allowed_actions)) {
        if (SHORTINIT) {
            wp_not_installed();
            //require_once ( ABSPATH.'/wp-load.php');
            require_once ABSPATH . WPINC . '/theme.php';
            $theme_path =  get_template_directory();
            require_once $theme_path . '/inc/ajax-functions.php';

            wp_load_utils();
            wp_load_session();

        }
        if ( function_exists('is_user_logged_in') ) {

            if(is_user_logged_in()){
                do_action('super_duper_'.$action);
            }else{
                do_action('super_duper_nopriv_'.$action);
            }
        }
    } else {
        die('-1');
    }

function wp_load_utils ( ) {
   require( ABSPATH . WPINC . '/class-wp-walker.php' );
   require( ABSPATH . WPINC . '/l10n.php' );
   require( ABSPATH . 'wp-admin/includes/admin.php' );
   require( ABSPATH . WPINC . '/formatting.php' );
   require( ABSPATH . WPINC . '/pluggable.php' );
   require( ABSPATH . WPINC . '/script-loader.php' );
   require( ABSPATH . WPINC . '/general-template.php' );
   require( ABSPATH . WPINC . '/link-template.php' );
   wp_functionality_constants();
}

function wp_load_session ( ) {
   require( ABSPATH . WPINC . '/capabilities.php' );
   require( ABSPATH . WPINC . '/user.php' );
   require( ABSPATH . WPINC . '/meta.php' );
   require( ABSPATH . WPINC . '/post.php');
   require( ABSPATH . WPINC . '/class-wp-user.php' );
   require( ABSPATH . WPINC . '/class-wp-roles.php' );
   require( ABSPATH . WPINC . '/class-wp-role.php' );
   require( ABSPATH . WPINC . '/session.php' );
   wp_cookie_constants();
}

?>