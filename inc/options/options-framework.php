<?php
/**
 * Custom  options types for Redux Framework
 * @package WordPress
 * @subpackage TankenBak
 */


function tankenbak_options_layouts() {
    return array(
        "widewidth" => array('alt' => __('Wide Width', 'redux_option'), 'img' => TB_URI.'/inc/img/layouts/page_wide.svg'),
        "wide-left-sidebar" => array('alt' => __('Wide Left Sidebar', 'redux_option'), 'img' => TB_URI.'/inc/img/layouts/page_wide_left.svg'),
        "wide-right-sidebar" => array('alt' => __('Wide Right Sidebar', 'redux_option'), 'img' => TB_URI.'/inc/img/layouts/page_wide_right.svg'),
        "fullwidth" => array('alt' => __('Full Width', 'redux_option'), 'img' => TB_URI.'/inc/img/layouts/page_full.svg'),
        "left-sidebar" => array('alt' => __("Left Sidebar", 'redux_option'), 'img' => TB_URI.'/inc/img/layouts/page_full_left.svg'),
        "right-sidebar" => array('alt' => __("Right Sidebar", 'redux_option'), 'img' => TB_URI.'/inc/img/layouts/page_full_right.svg')
    );
}

function tankenbak_options_layouts_flex() {
    return array(
        "widewidth" => array('alt' => __('Wide Width', 'redux_option'), 'img' => TB_URI.'/inc/img/layouts/page_wide.svg'),
        "boxed" => array('alt' => __('Boxed Width', 'redux_option'), 'img' => TB_URI.'/inc/img/layouts/page_full.svg'),
    );
}
function tankenbak_archive_layouts() {
    return array(
        //"list-style" => array('alt' => __('List Style', 'redux_option'), 'img' => TB_URI.'/inc/img/layouts/list-style.svg'),
        "box-style" => array('alt' => __('Box Style', 'redux_option'), 'img' => TB_URI.'/inc/img/layouts/box-style.svg'),
        //"block-styl" => array('alt' => __('Box Style', 'redux_option'), 'img' => TB_URI.'/inc/img/layouts/block-style.svg'),
    );
}

function tankenbak_options_sidebars() {
    return array(
        'wide-left-sidebar',
        'wide-right-sidebar',
        'left-sidebar',
        'right-sidebar'
    );
}



function options_generator_field_type() {
    return array(
        'checkbox',
        'radio',
        'text',
        'textarea',
        'multi_text'
    );
}