<?php
/**
 * Custom functions based on redux framework settings
 * Used to compile SCSS to CSS
 * @package TankenBak
 */


/*****************************************************
*                Copile SCSS TO CSS                  *
*   There is 3 variationsm vased on $_POST['type']   *
*     Types are : 0 - for compile login screen-,     *
*                 1 - for admin area css             *
*                 2 - front page css                 *
*****************************************************/

if(is_admin() && current_user_can( 'manage_options' )){
    add_action( 'wp_ajax_tankenbak_compile_scss', 'tankenbak_compile_scss' );
    //add_action( 'wp_ajax_nopriv_tankenbak_compile_scss', 'tankenbak_compile_scss' );
    if ( file_exists( TB_LIB . '/scss.inc.php' ) ) {
        require TB_LIB . '/scss.inc.php';
    }
}

function tankenbak_compile_scss() {
    //status #0- compile error, #1- compile succes, #2- File not writtable

    wp_verify_nonce( 'this-is-ajax-settings', 'security' );
    $directory = TB_ABSP.'/assets/css';
    if(is_admin() && current_user_can( 'manage_options' )){
        $options = get_option('option'); // Load Redux option from Data Base (redux OPT_NAME);
        $type = htmlspecialchars(stripslashes(trim($_POST['type'])));
        $css_file = 'unknown-type.css';
        $code = null;
         $code =   $options['log-screen-extra-css'];
        switch ($type) { // change file for compile
            case 0:
                $css_file = 'custom-login-style.css';
                $code =   $options['css-login-scss'];
                break;
            case 1:
                $css_file = 'custom-admin-style.css';
                $code =   $options['css-admin-scss'];
                break;
            case 2:
                $css_file = 'custom-page-style.css';
                $code =   $options['css-page-scss'];
                break;
            default:
               $css_file = 'unknown-type.css';
        }

        $message = __( 'Compile info: ', 'redux_function' );
        $filename = $directory.'/'.$css_file;
        $error = [];
        $status = 1;
        /* Compile SCSS*/
        try {
            $scss = new scssc();
            $scss->setFormatter('scss_formatter_compressed');
            $code = $scss->compile($code);
        } catch (Exception $e) {
            // error catch
            $error[] = $e->getMessage();
            $status = 0;
        }
        if($error == null){ // if no compiling errors
            $error[] = '<br>'.__( '# There is no errors while compile SCSS to CSS', 'redux_function' );

            $file = fopen($directory."/".$css_file, "w") or $error_folder = true;
            if($error_folder == true){
                $error[] = '<br>'.__( '# Error while opening file', 'redux_function' );
                $path = realpath($directory);
                if($path !== false && is_dir($path))
                {
                    $error[] = '<br>'.__( '# There is folder', 'redux_function' );
                    $error_folder = false;
                }else{
                    $error[] = '<br>'.__( '# <b>There is NO folder CSS in your current theme folder :( </b> <br>-----Trying to make Folder!-----', 'redux_function' );
                    mkdir($directory, 0775);
                    $path = realpath($directory);
                    if($path !== false && is_dir($path)){
                        $error_folder = false;
                        $error[] = '<br>'.__( '# YAY! we make Folder<br>-----Trying write to file again!-----', 'redux_function' );
                        $file = fopen($directory."/".$css_file, "w") or $error_folder = true;
                    } else{
                        $error[] = '<br>'.__( '# We cant make folder, try to do it manually, <br> Make css folder in current actived theme, and add 0775 permissions', 'redux_function' );
                        $error_folder = true;
                    }
                }
            }

            if($error_folder == false){
                if (is_writable(dirname($filename)) == false){
                    $error[] = '<br>'.__( '# Unable to write in folder, trying to add some <b>permissions</b> to file, <b>Try Again compile your code.</b><br> If this error occur more times, make soure, you have <b>permissions</b> on server, or add rights to 0755 to CSS folder in current theme. ', 'redux_function' );
                    @chmod(dirname($filename), 0755);
                    $status = 2;
                 }else{
                    fwrite($file, $code);
                    fclose($file);
                    $error[] = '<br>'.__( '# Compiled to File successfull', 'redux_function' );
                 }
            }else{
                    $status = 2;
                    $error[] = '<br>'.__( '# There is error, probabbly css folder dont exist or bad premissions, Try again!', 'redux_function' );
            }
        }


        if($error == null){
            $error[] = __( 'There is no errors', 'redux_function' );
        }

        $json = array('message' => $message, 'error' => $error, 'status' => $status);
        echo json_encode($json);
        die();
    }else{
        $message = __( 'You cant do it MAAAANNN!!!: ', 'redux_function' );
        $error = __( 'Lorem ipsim biacz!', 'redux_function' );
        $status = __( '10', 'redux_function' );
        $json = array('message' => '$message', 'error' => $error, 'status' => $status);
        echo json_encode($json);
        die();
    }
}
