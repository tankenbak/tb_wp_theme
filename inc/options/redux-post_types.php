<?php
/**
 * Custom Function file
 * Function for custom post types, and taxonomy
 * @package WordPress
 * @subpackage Norges Swiming
 */
add_action( 'init', 'create_post_types', 0 );
function create_post_types() {

/***********************************
*
* Register post type - Poem
*
 ***********************************/
    $labels = array(
        'name'                => _x( 'Poems', 'Post Type General Name', 'tankenbak' ),
        'singular_name'       => _x( 'Poem', 'Post Type Singular Name', 'tankenbak' ),
        'menu_name'           => __( 'Poem', 'tankenbak' ),
        'name_admin_bar'      => __( 'Poem', 'tankenbak' ),
        'parent_item_colon'   => __( 'Parent Poem:', 'tankenbak' ),
        'all_items'           => __( 'All Poem', 'tankenbak' ),
        'add_new_item'        => __( 'Add New Poem', 'tankenbak' ),
        'add_new'             => __( 'Add Poem', 'tankenbak' ),
        'new_item'            => __( 'New Poem', 'tankenbak' ),
        'edit_item'           => __( 'Edit Poem', 'tankenbak' ),
        'update_item'         => __( 'Update Poem', 'tankenbak' ),
        'view_item'           => __( 'View Poem', 'tankenbak' ),
        'search_items'        => __( 'Search Poem', 'tankenbak' ),
        'not_found'           => __( 'Not found', 'tankenbak' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'tankenbak' ),
    );
    $args = array(
        'label'               => __( 'Poems', 'tankenbak' ),
        'description'         => __( 'Poems', 'tankenbak' ),
        'labels'              => $labels,
        'supports'            => array( ),
        //'taxonomies'          => array( 'category', 'post_tag' ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-playlist-audio',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        //'rewrite'             => array( 'slug' => 'dikt' ),
        'capability_type'     => 'page',
    );
    //$obj = get_option('option_admin');

    register_post_type( 'test', $args );

    register_post_type( 'test2', $args );

}


// add_action( 'init', 'create_taxonomies', 0 );
function create_taxonomies() {
     /**********************************************
    *
    * Register taxonomy - Ceremonies
    * This taxonomy is for two post types - poem and musics
    *
     ************************************************/
    $labels = array(
        'name'              => _x( 'Ceremonies', 'taxonomy general name' ),
        'singular_name'     => _x( 'Ceremony', 'taxonomy singular name' ),
        'search_items'      => __( 'Search ceremony' ),
        'all_items'         => __( 'All ceremonies' ),
        'parent_item'       => __( 'Parent ceremony' ),
        'parent_item_colon' => __( 'Parent ceremony:' ),
        'edit_item'         => __( 'Edit ceremony' ),
        'update_item'       => __( 'Update ceremony' ),
        'add_new_item'      => __( 'Add New ceremony' ),
        'new_item_name'     => __( 'New Genre ceremony' ),
        'menu_name'         => __( 'Ceremonies' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'seremoni' ),
    );

    $post_types = array('dikt', 'musikk');
    register_taxonomy( 'ceremony', $post_types , $args );

}



?>