<?php
//Add html to widget,
//br [br]
//span [span]title[/span],
//anchor url="http://domain.com/"]something[/anchor]
add_filter('widget_title', 'do_shortcode');

add_shortcode('br', 'wpse_shortcode_br');
function wpse_shortcode_br( $attr ){ return '<br />'; }

add_shortcode('span', 'wpse_shortcode_span');
function wpse_shortcode_span( $attr, $content ){ return '<span>'. $content . '</span>'; }

add_shortcode('anchor', 'wpse_shortcode_anchor');
function wpse_shortcode_anchor( $attr, $content ){
    return '<a href="'. ( isset($attr['url']) ? $attr['url'] : '' ) .'">'. $content . '</a>';
}
