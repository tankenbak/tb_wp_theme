<?php
/**
 * Custom functions based on redux framework settings
 *
 * @package TankenBak
 */

/*********************************************************
*   Load Redux option from Data Base (redux OPT_NAME)    *
**********************************************************/
$options = get_option('option');

/*********************************************************
*     Load  option fole for Redux framework settings     *
**********************************************************/
if ( file_exists( TB_LIB . '/options/options-framework.php' ) ) {
    require TB_LIB . '/options/options-framework.php';
}
/*********************************************************
*       Load  compile file for tankenbak settings        *
**********************************************************/
if ( file_exists( TB_LIB . '/options/tankenbak-compile-scss.php' ) ) {
    require TB_LIB . '/options/tankenbak-compile-scss.php';
}
/*---------------------------------------------------------/
 |                                                         |
 |              CUSTOM CSS OPTION PAGE                     |
 |                 TankenBak Options                       |
/---------------------------------------------------------*/


/****************************************************
* Add styles for page, admin page, login page       *
*****************************************************/
//echo TB_ABSP;

if($options['css-enable'][1] == true){
    function tankenbak_style_custom_site() {
        wp_enqueue_style( 'user-site-style', TB_CSS.'/custom-page-style.css' );
    }
    add_action( 'wp_enqueue_scripts', 'tankenbak_style_custom_site' );
}
if($options['css-enable'][2] == true){
    function tankenbak_style_custom_login() {

        wp_enqueue_style( 'user-login-style', TB_CSS.'/custom-login-style.css' );
    }
    add_action( 'login_enqueue_scripts', 'tankenbak_style_custom_login' );
}
if($options['css-enable'][3] == true){
    function tankenbak_style_custom_admin() {
        wp_enqueue_style( 'user-admin-style', TB_CSS.'/custom-admin-style.css' );
    }
    add_action( 'admin_enqueue_scripts', 'tankenbak_style_custom_admin' );
}

//var_dump($options);


/*---------------------------------------------------------/
 |                 GENERAL OPTION PAGE                     |
 |                  TankenBak Options                      |
/---------------------------------------------------------*/


/***********************************************************
 *               LAZY LOADING CONTENT IMAGES
************************************************************/

if(isset($options['general-lazy-content-images']) && $options['general-lazy-content-images'] == TRUE){
   // new jQueryLazyLoad();
}

class jQueryLazyLoad {
  var $do_footer = false;

  function __construct() {
    add_filter('the_content', array($this, 'filter_the_content'));
    add_filter('wp_get_attachment_link', array($this, 'filter_the_content'));
    add_action('wp_footer', array($this, 'action_footer'));
  }

  function filter_the_content($content) {
    if (is_feed()) return $content;
    return preg_replace_callback('/(<\s*img[^>]+)(src\s*=\s*"[^"]+")([^>]+>)/i', array($this, 'preg_replace_callback'), $content);
  }

  function preg_replace_callback($matches) {
    // set flag indicating there are images to be replaced
    $this->do_footer = false;
    //$class_attr= '';
    // alter original img tag:
    //   - add empty class attribute if no existing class attribute
    //   - set src to placeholder image
    //   - add back original src attribute, but rename it to "data-original"
    if (!preg_match('/class\s*=\s*"/i', $matches[0])) {
      $class_attr = 'class="" ';
    }
    $replacement[1] = $matches[1] . $class_attr . ' src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== data-src' . substr($matches[2], 3) . $matches[3];
    preg_match('/class\s*=\s*"[^"]+"/i', $replacement[1], $image_class);
    if (!preg_match('/ali+[^"]+/i', $image_class[0], $image_class)){
      $image_class[0] = '';
    }
    // add wrapper to image, to create fade and image loader
    $replacement[0] = '<span class="b-wrapper '.$image_class[0].' base-border-color">';
    $replacement[2] ='</span>';
    // add "b-lazy" class to existing class attribute
    $replacement[1] = preg_replace('/class\s*=\s*"/i', 'class="b-lazy ', $replacement[1]);
    $replacement = $replacement[0].$replacement[1] .$replacement[2];
    // add noscript fallback with original img tag inside
    $replacement .= '<noscript>' . $matches[0] . '</noscript>';
    return $replacement;

  }

  function action_footer() {
    if (!$this->do_footer) {
      return;
    }
    echo <<<EOF
<script type="text/javascript">
</script>
EOF;
  }
}



/***********************************************************
 *                    Generate Sidebar                     *
 *               based on redux tab "Sidebars"             *
 *             $name -> name for sidebard,                 *
 *            Sidebar ID -> 'id'+lovercase(name)           *
************************************************************/

function tankenbak_widgets_redux($name) {
    $id = friendly_name($name);
    $args =  array(
        'name'          => esc_html__($name),
        'id'            => $id,
        'description'   => 'Custom Sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    );
    return $args;
}

add_action( 'widgets_init', function() use ($options) {
    if( isset($options['general-sidebars']) ){
        $sidebar = $options['general-sidebars'];
        if(!empty($sidebar)){
            foreach ($sidebar  as $key => $value) {
                if ($value == '' || $value == null ) {
                    $sidebar = null;
                    return;
                }
                $attr = tankenbak_widgets_redux($value);
                register_sidebar($attr);
            }
        }else{
            $sidebar = null;
            return;
        }
    }
});

/*---------------------------------------------------------/
 |              LOGIN SCREEN OPTION PAGE                   |
 |                 TankenBak Options                       |
/---------------------------------------------------------*/

/********************************************
*            Custom  Login Screen           *
*********************************************/
      /*---- Login Form Shake ----*/

add_action( 'login_head', function() use ($options) {
    if(!$options['log-screen-form-shake']){
        remove_action('login_head', 'wp_shake_js', 12);
    }
    if(!$options['log-screen-form-error']){
    add_filter('login_errors', create_function('$a', "return null;"));
    }

});
         /*---- Logo title ----*/
add_action( 'login_headertitle', function() use ($options) {
    return $options['log-screen-logo-title'];
});
         /*---- Logo Url ----*/
add_action( 'login_headerurl', function() use ($options) {
    return $options['log-screen-logo-url'];
});


add_action( 'login_enqueue_scripts', function() use ($options) {
    /*---    LOGO SETTINGS     ---*/
    $image   =  $options['log-screen-logo-image'];
    $margin  =  $options['log-screen-logo-margin'];
    $padding =  $options['log-screen-logo-padding'];
    $height  =  $options['log-screen-logo-height'];
    $width   =  $options['log-screen-logo-width'];
    /*---    Background SETTINGS     ---*/
    $bg =   $options['log-screen-page-background'];
    /*---    CSS SETTINGS     ---*/
    ?>
    <style type="text/css">
        body.login{
            background-image: url('<?php echo $bg['background-image'] ?>') ;
            -webkit-background-size: <?php echo $bg['background-size'] ?>;
        <?php
            foreach($bg as $key => $value){
                if( TRUE === ( $key == 'background-image'  || $value == null || $key == 'media' )   )  continue;
              echo $key.': '.$value.'; ';
            };
        ?>}
        body.login h1 a {
            background-image: url('<?php echo $image['background-image'] ?>') ;
            height: <?php echo $height['top'] ?>;
            width: <?php echo $width['right'] ?>;
            <?php
            foreach($image as $key => $value){
                if( TRUE === ( $key == 'background-image'  || $value == null || $key == 'media' )   )  continue;
              echo $key.': '.$value.'; ';
            };
            foreach($margin as $key => $value){
                if( TRUE === ( $key == 'units' || $value == null)   )  continue;
              echo $key.': '.$value.';';
            };
            foreach($padding as $key => $value){
                if( TRUE === ( $key == 'units' || $value == null )   )  continue;
              echo $key.': '.$value.';';
            };
            ?>
        }
    </style>
    <?php
});

/**
 * function for disabling wordpress garbage dash widgets.
 *
 * @since 0.8.4
 *
 * @link #remove_dashboard_meta_tb_rsx
 *
 */
function remove_dashboard_meta_tb_rsx() {
    if( is_admin() ){
        global $option;
        if($option['wp_welcome_panel']){
            remove_action( 'welcome_panel', 'wp_welcome_panel' );
        }
        $disabled_dwidgets = $option['wp_dashwidgets_unset'];
        if( $disabled_dwidgets ){
            $dash_widgets = get_das_widgets_list_tb();
            foreach ($disabled_dwidgets as $key => $disable) {
                if($disable){
                    remove_meta_box( $dash_widgets[$key]['id'], 'dashboard', $dash_widgets[$key]['type'] );
                }
            }
            //remove_meta_box('dashboard_quick_press', 'dashboard', 'side' );
        }
        // if($option['wp_dashboard_old']){
        //     // OLD
        //     remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        //     remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
        //     remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
        //     remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' ); // ostatnie szkice
        //     remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' ); // ostatnie komentarze
        // }
    }
}
add_action( 'wp_dashboard_setup', 'remove_dashboard_meta_tb_rsx' );

/* remove redux menu under the tools */
add_action( 'admin_menu', 'tb_rsx_remove_redux_menu',12 );
function tb_rsx_remove_redux_menu() {
    remove_submenu_page('tools.php','redux-about');
}
/*---------------------------------------------------------/
 |         UNREGISTER BASE WORDPRESS WIDGETS              |
/---------------------------------------------------------*/
function tb_rsx_unregister_default_widgets() {
    global $option;
    if ( isset( $option['wp_widgets_unset'] ) ) {
        $disabled_widgets = $option['wp_widgets_unset'];
        if( is_admin() && $disabled_widgets ){
            foreach ($disabled_widgets as $key => $enabled) {
                if($enabled){
                    unregister_widget($key);
                }
            }
        }
    }
}
add_action('widgets_init', 'tb_rsx_unregister_default_widgets', 11);

/*---------------------------------------------------------/
 |                ADMIN BAR OPTIONS                        |
/---------------------------------------------------------*/


/* Disable Admin Bar*/
function tb_rsx_remove_admin_bar() {
    global $option;
    if( !is_admin() && $option['wp_admin_bar_show'] ) {
        show_admin_bar(false);
    }
}
add_action('after_setup_theme', 'tb_rsx_remove_admin_bar');

/*---------------------------------------------------------/
 |                 CHANGE SEARCH URL SLUG                  |
/---------------------------------------------------------*/
function tb_rsx_change_search_url_rewrite() {
    global $option;
    if($option['wp_search_slug']){
        if ( is_search() && ! empty( $_GET['s'] ) ) {
            wp_redirect( home_url( '/search/' ) . urlencode( get_query_var( 's' ) ) );
            exit();
        }
    }

}
add_action( 'template_redirect', 'tb_rsx_change_search_url_rewrite' );


/*---------------------------------------------------------/
 |                      CLEAN HEADER                       |
/---------------------------------------------------------*/

if ( ! function_exists( 'tb_rsx_head_cleanup' ) ) {

function tb_rsx_head_cleanup() {
        remove_action('wp_head', 'feed_links', 2);  // Remove Post and Comment Feeds
        remove_action('wp_head', 'feed_links_extra', 3);  // Remove category feeds
        remove_action('wp_head', 'rsd_link'); // Disable link to Really Simple Discovery service
        remove_action('wp_head', 'wlwmanifest_link'); // Remove link to the Windows Live Writer manifest file.
        remove_action('wp_head', 'index_rel_link' );  // index link
        remove_action('wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
        remove_action('wp_head', 'start_post_rel_link', 10, 0 ); // start link
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);  // Remove relation links for the posts adjacent to the current post.
        remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0); // remove shortlink

       // global $wp_widget_factory;
       // emove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));

        add_filter('use_default_gallery_style', '__return_null');
    }
    if( $options['wp_cleanup_head'] ){
        add_action('init', 'tb_rsx_head_cleanup');
    }

}

/*---------------------------------------------------------/
 |               COLORS FOR POST STATUS                   |
/---------------------------------------------------------*/
function tb_rsx_posts_status_color(){
    global $option;
    if( isset($option['wp_post_status_colors']) ){
        if( $option['wp_post_status_colors'] ){
        ?>
          <style>
          .status-draft .check-column {box-shadow: -10px 0 0 -3px #F2C300 !important;}
          .status-pending .check-column {box-shadow: -10px 0 0 -3px #E87C03 !important;}
          .status-publish .check-column {box-shadow: -10px 0 0 -3px #27CD6F !important;}
          .status-future .check-column {box-shadow: -10px 0 0 -3px #3099DE !important;}
          .status-sticky .check-column {box-shadow: -10px 0 0 -3px #51ADD4 !important;}
          .status-private .check-column {box-shadow: -10px 0 0 -3px #9D58BA !important;}
          .post-password-required .check-column {box-shadow: -10px 0 0 -3px royalblue !important;}
          </style>
        <?php
        }
    }
}
add_action('admin_footer','tb_rsx_posts_status_color');


/*---------------------------------------------------------/
 |          Sharpen Uploaded Images when Resized           |
/---------------------------------------------------------*/
//var_dump(  wp_prepare_attachment_for_js( 196 ) );
function ajx_sharpen_resized_files( $resized_file ) {
    global $option;
    //var_dump($resized_file);
    if( $option['wp_image_quality'] ){
        $quality = ( intval($option['wp_image_quality']) * 10) ;
    }else{
        $quality = 90;
    }
    $image = wp_load_image( $resized_file );
    //var_dump($image);
    if ( !is_resource( $image ) )
        return new WP_Error( 'error_loading_image', $image, $file );

    $size = @getimagesize( $resized_file );
    if ( !$size )
        return new WP_Error('invalid_image', __('Could not read image size'), $file);
    list($orig_w, $orig_h, $orig_type) = $size;

    switch ( $orig_type ) {
        case IMAGETYPE_JPEG:
            $matrix = array(
                array(-1, -1, -1),
                array(-1, 16, -1),
                array(-1, -1, -1),
            );

            $divisor = array_sum(array_map('array_sum', $matrix));
            $offset = 0;
            imageconvolution($image, $matrix, $divisor, $offset);
            imagejpeg($image, $resized_file,apply_filters( 'jpeg_quality', $quality, 'edit_image' ));
            break;
        case IMAGETYPE_PNG:
            return $resized_file;
        case IMAGETYPE_GIF:
            return $resized_file;
    }

    return $resized_file;
}

//add_filter('image_make_intermediate_size', 'ajx_sharpen_resized_files',900);

/*---------------------------------------------------------/
 |               Disable comments lins                     |
/---------------------------------------------------------*/
if( $options['wp_remove_comments_links'] ){
    remove_filter('comment_text', 'make_clickable', 9);
}

/*--------------------------------------------------------------------------
| Add info about used page template to "page title" in page list table
 ------------------------------------------------------------------------- */
if( $options['wp_display_templates'] ){
    add_action(
        'admin_head-edit.php',
        'tankenbak_edit_post_change_title_in_list'
    );
}

function tankenbak_edit_post_change_title_in_list() {
    add_filter( 'display_post_states', 'tankenbak_use_template_as_post_state', 100, 2);
}
function tankenbak_use_template_as_post_state( $post_states, $post ) {
    $available_templates = get_page_templates($post);
    $post_template_file = get_post_meta( $post->ID, '_wp_page_template', true );
    ksort( $available_templates );
    foreach ( array_keys( $available_templates ) as $template ) {
        if($available_templates[ $template ] == $post_template_file) $post_states[] = __('Template: ', 'tankenbak').$template;
    }
    return $post_states;
}

/*---------------------------------------------------------/
 |                TANKENBAK ADMIN PAGE                     |
 |                 TankenBak Options                       |
/---------------------------------------------------------*/
if(is_admin() && current_user_can( 'manage_options' )){
    add_action( 'wp_ajax_tankenbak_show_registers', 'tankenbak_show_registers' );
    add_action( 'wp_ajax_nopriv_tankenbak_show_registers', 'tankenbak_show_registers' );
}

add_action( 'wp_enqueue_scripts',  function() use ($options) {
   // wp_dequeue_style( '' );
    if(isset($options['admin-deregister-css']) && $options['admin-deregister-css'] !=null ){
        $element_style = $options['admin-deregister-css'];
        if($element_style != null){
            foreach ($element_style  as $key => $value) {
                    wp_dequeue_style( $value );
                }
            }

        $element_script = $options['admin-deregister-js'];
        if($element_script != null){
            foreach ($element_script  as $key => $value) {
                    wp_dequeue_script( $value );
            }
        }
    }
});


add_action('init', function() use ($options) {
    if( !is_admin() && !is_login_page() && $options['custom-js-enable-js'][1] == '1' ){
            add_action( 'wp_footer', 'tankenbak_page_script_custom', 99 );
    }
});
function tankenbak_page_script_custom(){
    global $option;
    //wp_register_style( 'rxes-page-style', RXSEDITOR_BASE_URL . '/css/rxes-page-style.css', array(), '1.0', 'all' );
   // wp_enqueue_style( 'rxes-page-style' );
    if( wp_script_is( 'jquery', 'done' ) ) {
        //var_dump('jquery');
        echo '<script type="text/javascript">';
        echo $option['custom-js-page-jq'];
        echo "</script>";

    }
}

/*
function my_login_logo() { ?>
    <style type="text/css">
        .login h1 a {
            background-image: url(<?php echo get_theme_mod('logo_image') ?>);
            padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );*/
    /* Change logo url *//*
function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );*/

/*---------------------------------------------------------/
 |       DISPLAY QR CODE ON LOCALLHOST ON CODEKIT          |
/---------------------------------------------------------*/
add_action('init', 'tankenbak_qr_localhost');
function tankenbak_qr_localhost(){
    if( strpos($_SERVER['DOCUMENT_ROOT'], 'projects') > 0 ){
            add_action( 'wp_footer', 'tankenbak_qr_locallhost_script', 99 );
    }
}
function tankenbak_qr_locallhost_script(){
    if( wp_script_is( 'jquery', 'done' ) ) {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                if (typeof ckrpLibrary !== 'undefined') {
                    var ip = ckrpLibrary['ckrislocal1'];
                    console.log('Local:'+ ip);
                    img_link = 'http://api.qrserver.com/v1/create-qr-code/?size=150x150&data=http'+ip;
                    $('body').append('<span id="codekit_QR" style="position: fixed; top: 30%; left: 0; border-style: solid; border-width: 15px 0 15px 26.6px; border-color: transparent transparent transparent #007bff;" ><img style="display: none"; src="'+img_link+'"/></span>');
                    $('body').find('#codekit_QR').on('click', function(){
                            $(this).find('img').toggle();
                            console.log('click');
                    });
                }
            });
        </script>
        <?php
    }
}






