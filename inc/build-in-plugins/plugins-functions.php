<?php
if ( ! defined( 'ABSPATH' ) ) exit; // we dont like direct access



// We eed those only in admin area :)
if ( is_admin() ) {

    /********************************************
    *            ACF FIELD SNITCH               *
    *********************************************/
           // normal ACF                               // ACF PRO
    if( function_exists('register_field_group') || function_exists('acf_add_local_field_group') ){
        require ( TB_BIP.'/advanced-custom-fields-field-snitch/plugin.php' );
    }

    /********************************************
    *     TINY MCE FLOAD CLEAR BUTTON           *
    *********************************************/
    require ( TB_BIP.'/clear-floats-button/clear-floats-button.php' );
}