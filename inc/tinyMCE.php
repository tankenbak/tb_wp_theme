<?php


/***************************************************
 * EXTENDING TinyMCE FUNCTIONALITY
/***************************************************/

 /* Adding font size and font familly dropdowns */

if (!function_exists( 'custom_mce_buttons' ) ) {
    function custom_mce_buttons( $buttons ) {
        //array_unshift( $buttons, 'fontselect' ); // Add Font Select
        array_unshift( $buttons, 'fontsizeselect' ); // Add Font Size Select
        return $buttons;
    }
}
add_filter( 'mce_buttons_2', 'custom_mce_buttons' );
 /* Adding custom font sizes to font size dropdown */
if (!function_exists( 'custom_mce_text_sizes' ) ) {
    function custom_mce_text_sizes( $initArray ){
        $initArray['fontsize_formats'] = "8px 10px 12px 14px 16px 18px 20px 24px 28px 32px 36px 40px 45px 50px 55px 60px 65px 70px 75px 80px 85px 90px";
        return $initArray;
    }
}
add_filter( 'tiny_mce_before_init', 'custom_mce_text_sizes' );



add_action('media_buttons','add_sc_select',11);
function add_sc_select(){
    global $shortcode_tags;
     /* ------------------------------------- */
     /* enter names of shortcode to exclude bellow */
     /* ------------------------------------- */
    $exclude = array("wp_caption", "embed");
    echo '&nbsp;<select id="sc_select"><option>'.__('Select shortcode', 'tankenbak').'</option>';
    $shortcodes_list = '';
    foreach ($shortcode_tags as $key => $val){
            if(!in_array($key,$exclude)){
            $shortcodes_list .= '<option value="['.$key.'][/'.$key.']">'.$key.'</option>';
            }
        }
     echo $shortcodes_list;
     echo '</select>';
}
add_action('admin_head', 'button_js');
function button_js() {
        echo '<script type="text/javascript">
       jQuery(document).ready(function(){
          jQuery("#sc_select").change(function() {
                         send_to_editor(jQuery("#sc_select :selected").val());
                         return false;
               });
       });
       </script>';
}