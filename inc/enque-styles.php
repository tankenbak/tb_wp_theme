<?php
/********************************************
*       Include website front styles.       *
*********************************************/
add_action( 'wp_enqueue_scripts', 'tankenbak_enqueue_styles' );
function tankenbak_enqueue_styles() {
    wp_register_style( 'theme-styles', TB_CSS.'/style.css', false, '1.0', 'all' );
    wp_enqueue_style( 'theme-styles' );

}

/********************************************
*       Elements for admin dashboard        *
*********************************************/
add_action( 'admin_enqueue_scripts', 'theme_admin_enqueue_styles' );
function theme_admin_enqueue_styles() {
    wp_register_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/'.FONT_AWESOME.'/css/font-awesome.min.css', '', FONT_AWESOME , 'all');
    wp_register_style('custom-admin-style', TB_CSS .'/style-admin.css', '', '1.0', 'all');

    wp_enqueue_style( 'font-awesome' );
    wp_enqueue_style( 'custom-admin-style' );
}
