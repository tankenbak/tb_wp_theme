<?php
/********************************************
 *   Custom functions where code is used
 *   for remover or add elements
 ********************************************/

// function dequeue_jquery_migrate( &$scripts){
//     if( !is_admin() ){
//         $scripts->remove( 'jquery');
//         $scripts->add( 'jquery', false, array( 'jquery-core' ), '1.12.3' );
//     }
// }
// add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );

add_action( 'wp_default_scripts', function( $scripts ) {
    if(!is_admin()){
        if ( ! empty( $scripts->registered['jquery'] ) ) {
            $jquery_dependencies = $scripts->registered['jquery']->deps;
            $scripts->registered['jquery']->deps = array_diff( $jquery_dependencies, array( 'jquery-migrate' ) );
        }
    }
});

/********************************************
 *           REMOVE EMJOICONS SCRIPT
 ********************************************/
function pw_remove_emojicons(){
    // Remove from comment feed and RSS
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

    // Remove from emails
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

    // Remove from head tag
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );

    // Remove from print related styling
    remove_action( 'wp_print_styles', 'print_emoji_styles' );

    // Remove from admin area
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
}
add_action( 'init', 'pw_remove_emojicons' );


/* WP REMOVE IMAGE COMPRESSION */
// add_filter('jpeg_quality', function($arg){return 100;});



