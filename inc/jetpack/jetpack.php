<?php
/**
 * Jetpack Compatibility File.
 *
 * @link https://jetpack.me/
 *
 * @package TankenBak
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */

function tankenbak_jetpack_setup() {
    add_theme_support( 'infinite-scroll', array(
        'type'      => 'scroll',
        'container' => 'main',
        'render'    => 'tankenbak_infinite_scroll_render',
        'footer'    => 'page',
    ) );
} // end function tankenbak_jetpack_setup
add_action( 'after_setup_theme', 'tankenbak_jetpack_setup' );

/**
 * Custom render function for Infinite Scroll.
 */
function tankenbak_infinite_scroll_render() {
    while ( have_posts() ) {
        the_post();
        //var_dump('INFINITY');
        //get_template_part( 'template/template-parts/block', get_post_format() );
        get_template_part( 'template/template-parts/block', 'content' );
    }
} // end function tankenbak_infinite_scroll_render
