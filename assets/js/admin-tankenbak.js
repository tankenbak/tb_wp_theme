jQuery(document).ready(function($) {
    /* E - for clicked element, button! */
    function StartAjaxLoad_Redux(item){
        item.prop('disabled', true);
        $('#redux_ajax_overlay').fadeIn(100);
        $('.redux-ajax-loading').fadeIn(10);
    }


    /***********************************
           Call compile Functions
    ************************************/

    function Ajax(item, type){
        //console.log('sobie ajaxuje');
        var element = item.closest('div').children('#compile_info'),
            container = item.closest('div');
        var data = {
                action: 'tankenbak_compile_scss',
                security : TankenbakSettings.security,
                type: type,
         };
         var compile = $.post(TankenbakSettings.ajaxurl, data);
            compile.done(function(response){
                $('#redux_ajax_overlay').fadeOut(100);
                $('.redux-ajax-loading').fadeOut(10);
                var parsed_json = jQuery.parseJSON(response);
                if(parsed_json == null || parsed_json == false){
                    console.log('no data send!');
                        if(!container.hasClass('error')){
                            container.toggleClass('error');
                        }
                        if(container.hasClass('success')){
                            container.toggleClass('error');
                        }
                        if(container.hasClass('warning')){
                            container.toggleClass('error');
                        }
                        element.html('<b>There is Horrible and terrible error</b><br> while trying using function, refresh page, and try again to compile!<br> There is no respond from compiling function!');
                }else{
                    //console.log(parsed_json);
                    item.prop('disabled', false);
                    var
                        message = parsed_json['message'],
                        error = parsed_json['error'],
                        status = parsed_json['status'];
                        if(status == 1){ // compile success
                            if(container.hasClass('warning')){
                                container.toggleClass('warning success');

                            }else if(container.hasClass('error')){
                                container.toggleClass('error success');

                            }else if(!container.hasClass('success')){
                                container.toggleClass('success');
                            }
                        }
                        if(status == 0){ // compile fail
                            console.log(status);
                            if(container.hasClass('success')){
                                container.toggleClass('success warning');

                            }else if(container.hasClass('error') ){
                                container.toggleClass('error warning');

                            }else if(!container.hasClass('warning') ){
                                container.toggleClass('warning');
                            }
                        }
                        if(status == 2){ // File fail
                            if(container.hasClass('success')){
                                container.toggleClass('success error');

                            }else if(container.hasClass('warning') ){
                                container.toggleClass('warning error');

                            }else if(!container.hasClass('error') ){
                                container.toggleClass('error');
                            }
                        }
                    element.html('<h3>'+message+'</h3><span class="compile-info">'+error+'</span>' );
                }
            });

          compile.fail(function() {
                $('#redux_ajax_overlay').fadeOut(100);
                $('.redux-ajax-loading').fadeOut(10);
                alert('Sending ajax error');
                message = 'There Is some errors!';
                error = 'Fail to send ajax, reload page and try again!';
                if(!element.hasClass('error')){
                    element.toggleClass('error');
                }

                element.html('<h3>'+message+'</h3><span class="compile-info">'+error+'</span>' );
            });

        return false;
    }


    $(document).on( 'click', '#SaveCode', function(e) {
            e.preventDefault();
            $('#redux_save').click();
    });

    $(document).on( 'click', '#SaveScssPage', function(e) {
            e.preventDefault();
            var item = $(this),
                type = 2;
            StartAjaxLoad_Redux(item);
            Ajax(item, type);
    });

    $(document).on( 'click', '#SaveScssLogin', function(e) {
            e.preventDefault();
            var item = $(this),
                type = 0;
            StartAjaxLoad_Redux(item);
            Ajax(item, type);
    });

    $(document).on( 'click', '#SaveScssAdmin', function(e) {
            e.preventDefault();
            var item = $(this),
                type = 1;
            StartAjaxLoad_Redux(item);
            Ajax(item, type);
    });

    $(document).on( 'click', '#CleanTransients', function(e) {
            e.preventDefault();
            var item = $(this);
            StartAjaxLoad_Redux(item);

            var element = item.closest('div').children('#compile_info'),
                container = item.closest('div');
            var data = {
                    action: 'tb_clear_transients',
                    security : TankenbakSettings.security,
             };

             var compile = $.post(TankenbakSettings.ajaxurl, data);
             compile.done(function(response){
                 $('#redux_ajax_overlay').fadeOut(100);
                 $('.redux-ajax-loading').fadeOut(10);
                 var parsed_json = jQuery.parseJSON(response);
                 console.log(parsed_json);
                 if(parsed_json == null || parsed_json == false){
                     console.log('no data send!');
                         container.toggleClass('error');
                         element.html('<b>There is Horrible and terrible error</b><br> while using function, refresh page, and try again to compile!<br> There is no respond from compiling function!');
                 }else{
                     item.prop('disabled', false);
                     container.toggleClass('success');
                     element.html('<span class="compile-info">'+parsed_json.message+'</span>' );
                    }
            });
    });

    /***********************************
           Svg Support for Upload image
    ************************************/
    function fix_redux_svg() {
        $('.screenshot .of-uploaded-image').each(function() {
            var $this = $(this),
                image = $this.find('.redux-option-image'),
                link = $this.attr('href');
                console.log(image.attr('href'));
                console.log(link);
                if(link != null || ''){
                    image.attr('src', link);
                }

        });
    };
    fix_redux_svg();





});