/*
Use other scripts, file.
 */
 /*  INTERNAL SCRIPTS */
// @codekit-append "skip-link-focus-fix.js"
// @codekit-append "navigation.js"
// @codekit-append "tankenbak.js"
// @codekit-append "sweetalert2.js"


//BOOTSTRAP ELEMENTS
// @codekit-append "bootstrap/transition.js"
// @codekit-append "bootstrap/collapse.js"


 /*  EXTERNAL BIBLIOTECS */
// @codekit-append "jquery.matchHeight.js"
// @codekit-append "blazy.js"


// @codekit-append "js.cookie.js"
/*************************************************************
#                       Owl Carousel 2
#                       Has SCSS file
# @ http://www.owlcarousel.owlgraphic.com/
/*************************************************************/
// @codekit-append "owl.carousel.js"

/**************************************************************
#                       lightgallery
#                       Has SCSS file
# @ https://sachinchoolur.github.io/lightgallery.js/
/*************************************************************/
// codekit-append "lightgallery/lightgallery.js"


//https://osvaldas.info/image-lightbox-responsive-touch-friendly
// @codekit-append "little-lightbox.js"
// @codekit-append "little-lightbox-custom.js"
/* FLEXIBLE CONTENT */
// @codekit-append "../../inc/flexible-content/flexible.js"
