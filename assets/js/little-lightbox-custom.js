function setup_lightbox_galleries(){

        var
            // ACTIVITY INDICATOR

            activityIndicatorOn = function()
            {
                jQuery( '<div id="imagelightbox-loading"><div></div></div>' ).appendTo( 'body' );
            },
            activityIndicatorOff = function()
            {
                jQuery( '#imagelightbox-loading' ).remove();
            },


            // OVERLAY

            overlayOn = function()
            {
                jQuery( '<div id="imagelightbox-overlay"></div>' ).appendTo( 'body' );
            },
            overlayOff = function()
            {
                jQuery( '#imagelightbox-overlay' ).remove();
            },


            // CLOSE BUTTON

            closeButtonOn = function( instance )
            {
                jQuery( '<button type="button" id="imagelightbox-close" title="Close"></button>' ).appendTo( 'body' ).on( 'click touchend', function(){ jQuery( this ).remove(); instance.quitImageLightbox(); return false; });
            },
            closeButtonOff = function()
            {
                jQuery( '#imagelightbox-close' ).remove();
            },


            // CAPTION

            captionOn = function()
            {
                var description = jQuery( 'a[href="' + jQuery( '#imagelightbox' ).attr( 'src' ) + '"] img' ).attr( 'alt' );
                if( description.length > 0 )
                    jQuery( '<div id="imagelightbox-caption">' + description + '</div>' ).appendTo( 'body' );
            },
            captionOff = function()
            {
                jQuery( '#imagelightbox-caption' ).remove();
            },


            // NAVIGATION

            navigationOn = function( instance, selector )
            {
                var images = jQuery( selector );
                if( images.length )
                {
                    var nav = jQuery( '<div id="imagelightbox-nav"></div>' );
                    for( var i = 0; i < images.length; i++ )
                        nav.append( '<button type="button"></button>' );

                    nav.appendTo( 'body' );
                    nav.on( 'click touchend', function(){ return false; });

                    var navItems = nav.find( 'button' );
                    navItems.on( 'click touchend', function()
                    {
                        var jQuerythis = jQuery( this );
                        if( images.eq( jQuerythis.index() ).attr( 'href' ) != jQuery( '#imagelightbox' ).attr( 'src' ) )
                            instance.switchImageLightbox( jQuerythis.index() );

                        navItems.removeClass( 'active' );
                        navItems.eq( jQuerythis.index() ).addClass( 'active' );

                        return false;
                    })
                    .on( 'touchend', function(){ return false; });
                }
            },
            navigationUpdate = function( selector )
            {
                var items = jQuery( '#imagelightbox-nav button' );
                items.removeClass( 'active' );
                items.eq( jQuery( selector ).filter( '[href="' + jQuery( '#imagelightbox' ).attr( 'src' ) + '"]' ).index( selector ) ).addClass( 'active' );
            },
            navigationOff = function()
            {
                jQuery( '#imagelightbox-nav' ).remove();
            },


            // ARROWS

            arrowsOn = function( instance )
            {
                var jQueryarrows = jQuery( '<button type="button" class="imagelightbox-arrow imagelightbox-arrow-left"></button><button type="button" class="imagelightbox-arrow imagelightbox-arrow-right"></button>' );

                jQueryarrows.appendTo( 'body' );

                jQueryarrows.on( 'click touchend', function( e )
                {
                    e.preventDefault();
                    e.stopPropagation();
                    var jQuerythis   = jQuery( this );

                    if( jQuerythis.hasClass( 'imagelightbox-arrow-left' ) )
                    {
                        instance.nextImage();
                    }
                    else
                    {
                        instance.prevImage();
                    }

                    //instance.switchImageLightbox( index );
                    return false;
                });
            },
            arrowsOff = function()
            {
                jQuery( '.imagelightbox-arrow' ).remove();
            };



        var groups = {};
        jQuery('.lightbox-picture').each(function(e){
            var id =  jQuery(this).attr('data-group');

            if(!groups[id]) {
                groups[id] = [];
            }
            groups[id].push( this );
        });
        jQuery.each(groups, function() {
            var selectorF = jQuery(this);
            //console.log( selectorF );
            var instanceF = jQuery( selectorF ).imageLightbox(
                {
                    onStart: function() {
                        overlayOn();
                        closeButtonOn( instanceF );
                        arrowsOn( instanceF );
                    },
                    onEnd: function() {
                        overlayOff();
                        captionOff();
                        closeButtonOff();
                        arrowsOff();
                        activityIndicatorOff();
                    },
                    onLoadStart:    function() {
                        //captionOff();
                        activityIndicatorOn();
                    },
                    onLoadEnd:      function() {
                        //captionOn();
                        activityIndicatorOff();
                        jQuery( '.imagelightbox-arrow' ).css( 'display', 'block' );
                    }
                });
        });
}


jQuery(document).ready(function($) {
    if( !$('.gallery-container').length ){
        setup_lightbox_galleries();
    }
});