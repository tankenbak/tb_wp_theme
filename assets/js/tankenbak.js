function setup_lightbox(){

}
jQuery(document).ready(function($) {
    //add class when document is loded, and  if js is enabled
    $('html').addClass('document-ready js-enabled');
    $('html').removeClass('no-js');

    // $(document).keyup(function(e) {
    //     if ( $('html').hasClass('menu-open') && (e.keyCode == 27) ) {
    //         $('.menu-toggle').trigger('click');
    //     }
    // });

    var masthead = $('#masthead');
    maybe_tiny_header();
    $(window).on('scroll', function() {
        maybe_tiny_header();
    });

    function maybe_tiny_header() {
        if($(window).scrollTop() > 0) {
            masthead.addClass('tiny');
        } else {
            masthead.removeClass('tiny');
        }
    }

    $('.menu-toggle').on('click', function(e){
        $(this).toggleClass('active');
    })

    /* FOR EXTENDED GALLERIES */
    if ( $('.gallery-type-carousel').length ) {
        $('.gallery-type-carousel').each( function(e){
            var items = parseInt($(this).attr('data-cols'));
            $(this).owlCarousel({
                        loop:false,
                        margin: 0,
                        nav:true,
                        items: items,
                        dots: false,
                        fluidSpeed: 5,
                        navText: ['‹','›'],
                        responsive : {
                            0 : {
                                items:1,
                            },
                            480 : {
                                items: (items - 1) >= 2 ? (items - 1) : items,
                            },
                            767 : {
                                items: items,
                            },
                        },
                    });
        });
    }

    var doing_ajax = false;
    $(document).on('click', '.ajax-test', function(){
        console.log('start');

        if (doing_ajax) {
           // return;
        }
        ajax_tester( $(this).attr('data-action') );
    });

    function ajax_tester( action ){
       // var url = theme_data.ajaxurl;
       // var url = theme_data.theme_ajax;
       // var url = 'https://fysak.net/wp-json/wp/v2/posts/49';
       // console.log(url);

        $.ajax({
            type: 'GET',
            url: url,
            data: {
                action: action,
                text: 'somedata',
                shortinit: 'true',
            },
            beforeSend: function(){
                doing_ajax = true;
            },
            success: function(data, textStatus, jqXHR) {
                console.log(data);
                doing_ajax = false;
            }
        });
    }


    // lightGallery(document.getElementById('lightgallery'), {
    //     mode: 'lg-fade',
    //     cssEasing : 'cubic-bezier(0.25, 0, 0.25, 1)',
    //     loop: false,
    //     slideEndAnimatoin: false,
    // })

/* ---------------------------------------
| Lightbox picture to whole website
| @markup CoomingSoon
| @doc : http://dinbror.dk/blog/blazy/#Usage
-----------------------------------------*/

    // var groups = {};
    // $('.lightbox-picture').each(function(e){
    //     var id =  $(this).attr('data-group');

    //     if(!groups[id]) {
    //         groups[id] = [];
    //     }
    //     groups[id].push( this );
    // });

    // $.each(groups, function() {
    //    // console.log( $(this) );
    //     $(this).magnificPopup({
    //         type: 'image',
    //         closeBtnInside: false,
    //         closeOnContentClick: true,
    //         mainClass: 'mfp-fade',
    //         removalDelay: 300,
    //         gallery: {
    //             enabled:true,
    //             preload: [1,2],
    //         },
    //         image: {
    //                 verticalFit: true,
    //                 titleSrc: function(item) {
    //                     return item.el.attr('data-title') + ' &middot; <a class="image-source-link" href="'+item.src+'" target="_blank">' + item.el.attr('data-source_text')+ '</a>';
    //                 }
    //             }

    //     });
    // });

/* ---------------------------------------
| Lazy load init and option
| @doc : http://dinbror.dk/blog/blazy/#Usage
-----------------------------------------*/
    // Initialize
    var bLazy = new Blazy({
        success: function(el){
            var p = $(el).closest('.b-wrapper');
            $(p).removeClass('b-wrapper');
        },
        error: function(el, msg){
            if(msg === 'missing'){ /* Data-src is missing*/ }
            else if(msg === 'invalid'){ /* Data-src is invalid */}
        },
        breakpoints: [
        {
            width: 420, // max-width
            src: 'data-src-small'
         },
         {
            width: 768, // max-width
            src: 'data-src-medium'
         },
        ] // end of breakpoints
    });

    /* ---------------------------------------
    | Init for custom function - "lazy_thumb"
    -----------------------------------------*/
    var bThumb = new Blazy({
        loadInvisible: true,
        selector: '.b-thumb',
        success: function(el){
            var p = $(el).closest('.b-wrapper');
            $(p).removeClass('b-wrapper');
        },
        error: function(el, msg){
            if(msg === 'missing'){ /* Data-src is missing*/ }
            else if(msg === 'invalid'){ /* Data-src is invalid */}
        },
        breakpoints: [
        {
            width: 420, // max-width
            src: 'data-src-small'
         },
         {
            width: 768, // max-width
            src: 'data-src-medium'
        },
        ] // end of breakpoints
    });
    var bThumb = new Blazy({
        loadInvisible: true,
        selector: '.b-blur',
        success: function(el){
            var p = $(el).closest('.b-wrapper');
            $(p).removeClass('b-wrapper');
        },
        error: function(el, msg){
            if(msg === 'missing'){ /* Data-src is missing*/ }
            else if(msg === 'invalid'){ /* Data-src is invalid */}
        },
        breakpoints: [
        {
            width: 420, // max-width
            src: 'data-src-small'
         },
         {
            width: 768, // max-width
            src: 'data-src-medium'
        },
        ] // end of breakpoints
    });

    // fallback if browser not load first visible elements
    var timeut_check_images = setInterval(function(){
        bLazy.revalidate();
        bThumb.revalidate();
    },250);


/* ---------------------------------------
| Add to link class .scroll-link
| to make it scroll to ID container
-----------------------------------------*/
    $(".scroll-link").click(function(event){
        event.preventDefault();
        $('html,body').animate({scrollTop: $(this.hash).offset().top-50 }, 500);
    });


/* ---------------------------------------
| Popup windows for share link page links
-----------------------------------------*/
    function popupCenter(url, title, w, h) {
        var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/2);
        return window.open(url, title, 'toolbar=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=yes, width='+w+', height='+h+', top='+top+', left='+left);
    }

    $(document).on('click', '.content-share', function(e) {
        e.preventDefault();
        var url = $(this).attr('data-share_link'),
            name = $(this).attr('data-share_name'),
            height = $(this).attr('data-share_height'),
            width = $(this).attr('data-share_width');
        popupCenter(url, name, width, height);
    });

    var $share_block_tb = $('#page-share-tb');
    if ($share_block_tb.length){
        // var $current_page = document.URL ;
        // $share_block_tb.find('li.share-link').each(function(){
        //     var $this_data = $(this).find('.content-share').attr('data-share_name'),
        //         $this = $(this), $this_link = $(this).find('.content-share'),
        //         $likes_count = null;

        //     switch($this_data) {
        //         case 'Facebook':
        //             break;
        //         case 'Twitter':
        //             break;
        //     }
        // });
    }




/* ---------------------------------------
| Cookie Law info Cooki setup
| @doc https://github.com/js-cookie/js-cookie/tree/v2.1.2#readme
-----------------------------------------*/

    if($('#cookie-law').length > 0) {
        var accepted = Cookies.get("cookie_law");
        if (accepted == "yes") {
            $('#cookie-law').remove();
        } else {
            var cookie_html  = $('#cookie-element').html();
            $('#cookie-law.empty').replaceWith(cookie_html);
            $('#cookie-law').fadeIn();
        }
        $('#accept-cookie').on('click', function(e) {
            e.preventDefault();
            Cookies.set('cookie_law', 'yes', { expires: 365, path: '/' });
            $('#cookie-law').fadeOut();
        });
    }

/* Eual function for every container, that have class "equal-height-byrow", and elements have calss "equal"*/
$('.equal-height-byrow').each(function() {
    $(this).children('.equal').matchHeight({
        byRow: true,
        property: 'height',
    });
});
$('.equal-height').each(function() {
    $(this).children('.equal').matchHeight({
        byRow: false,
        property: 'height',
    });
});
/*-----------------------------------------------
|              Stretch Row Function
|     This function is used for every element
|            using class: @stretchRow,
|        Redux add it to element if needed
-----------------------------------------------*/
var $cont = $('.stretchRow') // item that you want Stretch
function stretchRow($container){
    $container.find(function(){
        var $el = $container , $offset = $el.offset(),
            $window = $(window).width(), $parent = $el.parent().width();

        $window = $window - $parent;
            $el.css({
                'margin-left': (-1*($window/2)),
                'margin-right': (-1*($window/2)),
             });
    });
}

/*--------------------------------------
| Window Resize Functions Triggers
--------------------------------------*/
    //stretchRow($cont);
    $(window).resize(function() {
     //   streatchRow($cont);
    });





});