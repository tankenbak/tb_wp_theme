=== TankenBak ===

Contributors: Isu
Tags: translation-ready,

Requires at least: 4.0
Tested up to: 4.6
Stable tag: 1.0.0
License:

A starter theme called TankenBak, or underscores.

== Description ==

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

== Theme Contain JQ Scripts / CSS :  ==

1. Bootstrap version 3.3.6
2. Andy Mixins version: 1.1   @link https://github.com/gillesbertaux/andy
3. bLazy version 1.5.4 (2016.3.06) // Lazy load images @link http://dinbror.dk/blog/blazy/
4. JavaScript Cookie version 2.1.2 // JS Cookie @link https://github.com/js-cookie/js-cookie/tree/v2.1.2#readme
5. matchHeight  // https://github.com/liabru/jquery-match-height
6. OwlCarousel 2.1.0 @link https://owlcarousel2.github.io/OwlCarousel2/
7. mCustomScrollbar 3.1.3 @link http://manos.malihu.gr/jquery-custom-content-scroller/

== TO DO ==

## !!!! MULTISITE OPTION CHECK !!!!!

## CSS/SCSS/JS/JQ
4. Dodac konrada fcje do popup

## Page Styles/ Tempaltes
7. Image sizes revalidate

## REDUX OPT
4. Add Placeholder image custom select
7. Reset transients ( transient list add )
8. Add stage switch (?!?!)

## New Elements

## WP FIXES !
1. Fix WP slow querries !!!!!!
2. Add WP Dash widget (front page, blog page, admin email, post per page, etc)
3. TinyMCE Font weight select ?

== Changelog ==

= 0.8.6 - 2016.09.01 =

##
1. Fix `tinyMCE` include file (wrong letter)
2. Update `bLazy` script
3. Update `OwlCarousel` script
4. Update `jsCookie` script
5. Added files for flexible content, files can be found in `inc/flexible-content` folder,
    Folder conain base function file, style, scripts and style and scripts for admin, where should be
    included scripts/styles/functions for added sections.
    In file `flexible-functions.php` should be placed added by php code flex section codes.
    `flexible-content` content contain folder for `sections` please put them there.




= 0.8.5 - 2016.05.31 =
* OOOOOUH HELL REBUILD LAYOUT AGAIN !

## Changes
1. Rebuild function `tankenbak_get_template` - now this return array of default data for layout
2. Change displaing layout to easy menage by stanrard wp style
3. add after i before loop template parts
4. Added new function `get_sidebar_tb` , similar to `get_sidebar` but now data can be added to this function as second arg.
5. Update missplaced classes and divs
6. fix many php notices and errors
7. remove garbage data from redux option
8. Rebuilded all templates files and structore!!!!!
9. Fix some social share garbage elements

= 0.8.4 - 2016.05.26 =
##
1. Cookie Js updated to 2.1.2 from 1.5.1.
2. Fix Cookie js invalid cookie setup..
= 0.8.4 - 2016.05.30 =

## Redux options
1. Update options where you can disable wp dashwidgets, now can be disabled more than base.

##Functions and actions
1. new functions `manage_dash_setup_tb` , `get_das_widgets_list_tb` and `get_das_widgets_list_tb_opt`
   for better menaging dashwidgets.

= 0.8.3 - 2016.05.25 =

## Redux options
1. on/of Base Dashwidgets
2. on/off base wordpress widgets
3. Disable admin bar on front opction
4. Enable change quality for uplading and resized jpegs
5. Option for celan unnecessery header information.
6. Disable Links from User Comments

## CSS/SCSS/JavaScript
1. Added mixin `font-smoothing` as standard in `mixins-masters.scss`
2. Added varribles passed to JS file in `enque-scripts.php`
3. Added for Konrad `mquery dot` and gis colored `mquery mixins`

##Functions and actions
1. Added `get_post_types_tb` function to show post types
2. Create function, that make thumbs and all resized images sharpen,
3. Added function to celan unnecessery header information.
4. Added new function `get_template_part_tb`  @docs `#get_template_part_tb`

## FOLDERS AND FILES

* Added new folders and files in `lib` folder for menaging extensions for theme.

## OTHER
1. Const names updated
2. New const @docs `#const-names`

= 0.8.2 - 2016.05.23 =

## YAYYYY
1. FINNALLY WE HAVE READ ME ! :D

## OTHERS
1. New placeholder - `/img/no_image.jpg`
2. Footer now is made using 3 widgets areas

## STYLES
1. New color varibles for Cookie bar in `varibles-site.scss`
2. Varribles for scroll bar and select colors.

## Scripts
1. Added Snippet, that allow you to add just `.scroll-link` to  link, and it scroll to ID content.

## Custom styles
1. `404` - page has now base style
2. `Cookie law bar` - now has own base style.

## Functions and actions
1. `lazy_thumb` - is now supported.
2. `lazy_thumb_by_id` - removed
3. Added Hook to remove WP upload Compression in `remove-add-functions`

## Independet Functions
1. `tankenbak_use_template_as_post_state` - New function hooked in Backend to show current template of page next to name in file `custom-functions.php`

## NEW FILES
1. SCRIPTS enque moved to libs file -                   `enque-scripts.php`
2. STYLES enque moved to libs file -                    `enque-styles.php`
3. Shortocdes functions moved to libs file -            `shortcodes-functions.php`
4. TinyMCE extension functions moved to libs file -     `tinyMCE.php`
5. Sidebar init functions moved to libs file -          `sidebars.php`
6. Transients functions are stored in libs file -       `transients-functions.php`
7. Ajax functions has own base lib file -               `ajax-functions.php`
8. Redux functions are stored in lib file -             `redux-function.php`
8. New file in libs for creating custom post types      `create_post_types.php`


## THEME Info
1. Theme is inited in lib file -                        `init-theme.php`
2. REDUX FRAMEWORK - is now loaded only on backend to minimalise Queries on frontend, all options are still in stanrard global $option.

## THEME FEATURES
1. SCSS/CSS writing and file compiling work correctly.
2. JS/JS Work correctly.
3. Disabled redux page sizes

## FIX
1. Custom Styles Fix to correct hook.
2. Remove unnecessery `normalize.scss`, now is used bootstrap based normalize.

= 0.1 - 2015 =
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
