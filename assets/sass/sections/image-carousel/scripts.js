/*******************************************************/
// This file is used to merage all flexible content JS
// File is used for frontend - file meraged in flexible.js
// This fiquired bLazy load and Owl Carousel
/*******************************************************/

jQuery(document).ready(function($) {

    $('.gallery-owl').each(function() {
        $(this).children('.gallery-carousel').owlCarousel({
            loop:false,
            margin: 7,
            nav:true,
            items: 4,
            dots: false,
            fluidSpeed: 5,
            navText: ['‹','›'],
            responsive : {
                0 : {
                    items:2,
                },
                620 : {
                    items:3,
                },
                992 : {
                    items:4,
                },
            },
            onTranslate: function(){
                var bLAzy_gallery = new Blazy({
                    selector: '.b-lazy-gallery',
                    success: function(el){
                        var p = $(el).closest('.b-wrapper');
                        $(p).removeClass('b-wrapper');
                    },
                });
            },
            onInitialized: function (){
                var bLAzy_gallery = new Blazy({
                    selector: '.b-lazy-gallery',
                    success: function(el){
                        var p = $(el).closest('.b-wrapper');
                        $(p).removeClass('b-wrapper');
                    },
                });
            },
        });
    });

});