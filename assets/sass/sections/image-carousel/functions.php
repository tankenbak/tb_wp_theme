<?php
// Image slaider based on owl carousel
if ( ! defined( 'ABSPATH' ) ) exit; // we dont like direct access

$sections_array[] = array (
    'key' => '57c52b8046fff',
    'name' => 'image-carousel',
    'label' => __('GALLERY', 'tankenbak'),
    'display' => 'table',
    'sub_fields' => array (
        array (
            'key' => 'field_57c52b8f47000',
            'label' => __('Gallery Images', 'tankenbak'),
            'name' => 'gallery_images',
            'type' => 'gallery',
            'instructions' => '',
            'required' => '',
            'conditional_logic' => '',
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'min' => 4,
            'max' => 35,
            'preview_size' => 'thumbnail',
            'insert' => 'append',
            'library' => '',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
    ),
    'min' => '',
    'max' => '',
);