<?php
    $images = $template_data['gallery_images'];
    $carousel_id = uniqid();
?>

<section class="flex-content gallery-owl">
        <div class="gallery-carousel owl-carousel">
            <?php foreach ($images as $image): ?>
                <?php
                    $image_data = wp_prepare_attachment_for_js( $image['ID'] );
                ?>
                <div class="b-wrapper lazy-wrapper base-border-color">
                    <div
                        href="<?php echo $image_data['url'] ?>"
                        data-lightbox="image-<?php echo $carousel_id ?>"
                        data-title="<?php echo $image_data['title'] ?><br><small><?php echo $image_data['caption'] ?></small>"
                        title="<?php echo $image_data['title'] ?>"
                        class="b-responsive bg-holder bg-covered b-lazy-gallery"
                        data-full="<?php echo $image_data['url'] ?>"
                        <?php if (isset( $image_data['sizes']['medium_large']['url'] ) && $image_data['sizes']['medium_large']['url'] != '' ): ?>
                            data-src="<?php echo $image_data['sizes']['medium_large']['url'] ?>"
                        <?php else:  ?>
                            data-src="<?php echo $image_data['url'] ?>"
                        <?php endif ?>
                        <?php if ( isset( $image_data['sizes']['post-thumbnail']['url'] ) && $image_data['sizes']['post-thumbnail']['url'] != '' ): ?>
                            data-medium="<?php echo $image_data['sizes']['post-thumbnail']['url'] ?>"
                        <?php endif ?>
                        >
                    </div>
                    <noscript>
                        <img src="<?php echo $image_data['url'] ?>" alt="<?php echo $image_data['alt'] ?>">
                    </noscript>
                </div>
            <?php endforeach ?>
        </div>
</section>