<?php
/**
 * TankenBak functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @package TankenBak
 */

/*
/-------------------------------
 * JQ MIGRATE IS DISABLED !
 * This theme contain Extra Styles and Js
 * Styles & Mixins & Fonts:
 * -- Bootstrap version 3.3.6
 * -- Font Awesome version 4.5.0 // Loaded from maxcdn
 * -- Andy Mixins version: 1.1   // @link https://github.com/gillesbertaux/andy
 * Java Scripts :
 * -- DePreLoad // https://github.com/wearede/DePreLoad.js
 * -- bLazy version 1.5.4 (2016.3.06) // Lazy load images @link http://dinbror.dk/blog/blazy/
 *    JavaScript Cookie version 1.5.1 // JS Cookie @link https://github.com/js-cookie/js-cookie/tree/v1.5.1
 *    matchHeight  @link https://github.com/liabru/jquery-match-height
 *    mCustomScrollbar 3.1.3 @link http://manos.malihu.gr/jquery-custom-content-scroller/
 /-------------------------------
 */

/*
|--------------------------------------------------------------------------
| Constants
|--------------------------------------------------------------------------
|
*/
	/*ABSP*/
$theme = wp_get_theme();
define('TB_LIB',                get_template_directory() . '/inc');    // library directory
define('TB_ADMIN',              TB_LIB . '/admin');                    // admin directory
define('TB_FUNCTIONS',          TB_LIB . '/functions');                // functions directory
define('TB_DIR',                get_template_directory());             // template directory
define('TB_ABSP',               get_stylesheet_directory());           //actual template directory absp
	/* Plugin extensions folders*/
define('TB_ACF',                TB_LIB . '/acf');            // Acf functions directory
define('TB_FCS',     		    TB_LIB . '/flexible-content/sections');    // Flexible content sections dir
define('TB_BIP',     		    TB_LIB . '/build-in-plugins/plugins');    // Build In plugins
define('TB_VC',                 TB_LIB . '/visualcomposer');             // Visual Composer functions directory
define('TB_WOO',                TB_LIB . '/woocommerce');            // WooCommerce functions directory
define('TB_JP',                 TB_LIB . '/jp-functions');             // Jetpack functions directory
define('TB_BBP',                TB_LIB . '/bbpress');        // bbPress functions directory
	/*HTTP*/
define('TB_CSS_DIR',            get_stylesheet_directory_uri());       //actual template url
define('TB_URI',                get_template_directory_uri());         // template directory uri
define('TB_CSS',                TB_URI . '/assets/css');                      // css uri
define('TB_JS',                 TB_URI . '/assets/js');                       // javascript uri
define('TB_IMG',                TB_URI . '/assets/img');                      // image uri
define('TB_PLACEHOLDER_IMG',    TB_IMG . '/no_image.jpg');             // Placeholder Image URI
define('TB_BIP_DIR',            TB_URI.'/inc/build-in-plugins/plugins');                      // Build in plugins dir

define('TB_VERSION',               $theme->get('Version') );            // get current version
define('FONT_AWESOME', '4.5.0'); // import version of font avesome, use to navigate what vesion from MaxCDN should we load,check @link https://fortawesome.github.io/Font-Awesome/get-started/



	/* OLD */
if ( ! function_exists( 'tankenbak_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */

/********************************************
*  Create new role for Mega Admin User      *
*     Hooked after activation theme!        *
*********************************************/
function add_theme_caps(){
	global $wp_roles;
	if ( isset( $wp_roles ) ){
		$adm = $wp_roles->get_role('administrator');
		$wp_roles = new WP_Roles();
		$wp_roles->add_role('devadmin', __('Dev Admin', 'tankenbak'), $adm->capabilities);
		$new_dev = $wp_roles->get_role('devadmin');
		$new_dev->add_cap( 'iron_admin' );
	}
	 // check current user caps
}
//add_theme_caps();
/*
$data = get_userdata( get_current_user_id() );
echo 'MA TE MOC ';
if ( is_object( $data) ) {
	$current_user_caps = $data->allcaps;
	echo '<pre>' . print_r( $current_user_caps, true ) . '</pre>';
}
*/
add_action( 'load-themes.php', 'add_theme_caps' );
function tankenbak_setup() {
	/*
	 * Load text domains
	 */
	load_theme_textdomain('tankenbak', TB_DIR.'/languages');
	/*
	 * Load template elements and functions
	 */
	if ( file_exists( TB_LIB.'/init-theme.php' ) ) {
		require_once TB_LIB.'/init-theme.php';
	}else{
		die('There is theme files missing!');
	}

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on TankenBak, use a find and replace
	 * to change 'tankenbak' to the name of your theme in all the template files.
	 */
		load_child_theme_textdomain('tankenbak', get_stylesheet_directory().'/languages');
		add_editor_style('/assets/css/editor.css');
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// allow shortcodes in widget text
		add_filter('widget_text', 'do_shortcode');

		/*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		* Add support for post thumbnails
		*/
		/* Specyfic sizes and names for it */
		if ( function_exists( 'add_image_size' ) ) {
			add_theme_support( 'post-thumbnails' );
			set_post_thumbnail_size( 648, 548 );

			add_image_size( 'mini-placeholder', 32 );

			//var_dump('asdasd');
			add_image_size( 'tb-extra_large', 1920 );
			add_image_size( 'tb-large', 1170 );
			add_image_size( 'tb-medium', 767 );
			add_image_size( 'tb-small', 460 );
		}
		add_filter('image_size_names_choose', 'tankenbak_image_sizes');
		function tankenbak_image_sizes( $sizes ) {
			return array_merge( $sizes, array(
				'tb-extra_large' => __('Extra large', 'tankenbak'),
				'tb-large'	=> __('Large', 'tankenbak'),
				'tb-medium'	=> __('Medium', 'tankenbak'),
				'tb-small'	=> __('Small', 'tankenbak'),
			) );
		}

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'tankenbak' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		//'aside',
		//'image',
		//'video',
		//'quote',
		//'link',
	) );
	// Set up the WordPress core custom background feature.
	/*
	 add_theme_support( 'custom-background', apply_filters( 'tankenbak_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
	*/

	add_theme_support( 'woocommerce' );
}
endif; // tankenbak_setup

add_action( 'after_setup_theme', 'tankenbak_setup', 0 );




/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 * Priority 0 to make it available to lower priority callbacks.
 * @global int $content_width
 */
function tankenbak_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tankenbak_content_width', 1140 );
}
add_action( 'after_setup_theme', 'tankenbak_content_width', 0 );


/********************************************
*   IN BUILD  Custom Upload types IN BUILD   *
*********************************************/
function my_myme_types($mime_types){
	$mime_types['svg'] = 'image/vector'; //Adding avi extension
	//unset($mime_types['pdf']); //Removing the pdf extension
	return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);





/*
|--------------------------------------------------------------------------
| Sidebar setting on pages
|--------------------------------------------------------------------------
|
*/
//add metabox
function add_sidebar_settings_meta_box() {
	$screens = array( 'page' );
	foreach ( $screens as $screen ) {
		add_meta_box(
			'sidebar_settings',
			'Page sidebar',
			'sidebar_settings_meta_box_callback',
			$screen,
			'side',
			'default'
		);
	}
}
add_action( 'add_meta_boxes', 'add_sidebar_settings_meta_box' );
//the metabox contant
function sidebar_settings_meta_box_callback( $post ) {
	wp_nonce_field('sidebar_settings', 'sidebar_settings_meta_box_nonce');
	$sidebar = get_post_meta( $post->ID, 'sidebar', true );
	?>
		<select id="sidebar" name="sidebar">
			<option value="0"><?php _e('No sidebar', 'tankenbak') ?></option>o
			<?php
			foreach($GLOBALS['wp_registered_sidebars'] as $s) {
				$sel = ($sidebar == $s['id'])? 'selected' : '';
				echo '<option '.$sel.' value="'.$s['id'].'">'.$s['name'].'</option>';
			}
			?>
		</select>
	<?php
}
//save the metabox settings
function save_sidebar_settings_meta_box_data( $post_id ) {
	if(!isset($_POST['sidebar_settings_meta_box_nonce'])) return;
	if(!wp_verify_nonce($_POST['sidebar_settings_meta_box_nonce'], 'sidebar_settings')) return;
	if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

	if(!isset($_POST['sidebar'])) {
		update_post_meta( $post_id, 'sidebar', 0);
	} else {
		update_post_meta( $post_id, 'sidebar', $_POST['sidebar']);
	}
}
add_action( 'save_post', 'save_sidebar_settings_meta_box_data' );




/*
|--------------------------------------------------------------------------
| Show / hide title setting for pages
|--------------------------------------------------------------------------
|
*/
//add metabox
function add_sh_title_settings_meta_box() {
	$screens = array( 'page' );
	foreach ( $screens as $screen ) {
		add_meta_box(
			'sh_title',
			'Show / hide title',
			'sh_title_settings_meta_box_callback',
			$screen,
			'side',
			'default'
		);
	}
}
add_action( 'add_meta_boxes', 'add_sh_title_settings_meta_box' );
//the metabox contant
function sh_title_settings_meta_box_callback( $post ) {
	wp_nonce_field('sh_title', 'sh_title_settings_meta_box_nonce');
	$display_title = get_post_meta( $post->ID, 'display_title', true );
	?>
	<select id="display_title" name="display_title" style="position: absolute; top: 4px; right: 4px;">
		<option value="yes" <?php if($display_title == 'yes') echo 'selected' ?>><?php _e('Show title', 'tankenbak') ?></option>
		<option value="no" <?php if($display_title == 'no') echo 'selected' ?>><?php _e('Hide title', 'tankenbak') ?></option>
	</select>
	<?php
}
//save the metabox settings
function save_sh_title_settings_meta_box_data( $post_id ) {
	if(!isset($_POST['sh_title_settings_meta_box_nonce'])) return;
	if(!wp_verify_nonce($_POST['sh_title_settings_meta_box_nonce'], 'sh_title')) return;
	if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

	if(!isset($_POST['display_title'])) {
		update_post_meta( $post_id, 'display_title', 'yes');
	} else {
		update_post_meta( $post_id, 'display_title', $_POST['display_title']);
	}
}
add_action( 'save_post', 'save_sh_title_settings_meta_box_data' );


// $pluginy = array(
// 		array(
// 			'name'	=> 'super plugin z githuba',
// 			'url'	=> '@git cos tam cos',
// 		),
// 		array(
// 			'name'	=> 'slaby plugin z githuba',
// 			'url'	=> 'ala kisi trawe',
// 		),
// 		array(
// 			'name'	=> 'pro plugin z wodpressa',
// 			'url'	=> '@git cos tam cos',
// 		),
// 		array(
// 			'name'	=> 'pro plugin z wodpressa',
// 			'url'	=> '@git cos tam cos',
// 		),
// 		array(
// 			'name'	=> 'pro plugin z wodpressa',
// 			'url'	=> '@git cos tam cos',
// 		),
// 	);



// $i= 0;
// foreach ( $pluginy as $plugin ) {
// 	echo '['.$i.'] : <code>'.$plugin['name'].'</code><br>' ;
// 	$i++;
// }
// echo  $pluginy[1]['url'];


