<?php
/**
 * The template for displaying 404 page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 *
 * @since 0.8.5
 * Rebuild to use only this fiel as base template, and tankenbak_get_template as setup for data display;
 */

get_header(); ?>
<?php
$args = array(
    'sidebar'   => false,
    );
$template_data['layout'] =  tankenbak_get_template( $args ); // setup all default data for layout
$template_data['post'] = $post;
$page_class = get_page_classes_tb( $template_data );

?>
<div class="row <?php echo $page_class['page']['class'] ?>">
        <?php
        //sidebar load
        if( $template_data['layout']['sidebar'] && ( $page_class['sidebar']['type'] == 'first' ) ): ?>
                <?php get_sidebar_tb( $template_data['layout']['sidebar'], $template_data ); ?>
        <?php endif; ?>

             <div id="primary" class="content-area <?php echo $page_class['container']['class'] ?>">
                <main id="main" class="site-main" role="main">
                    <article id="post-404" <?php post_class('error_404_page'); ?>>
                        <header class="page-header">
                            <h4 class="error_title"><span>
                                <?php _e('404', 'tankenbak') ?>
                           </span></h4>
                           <p>
                            <?php
                                $request= $_SERVER['REQUEST_URI'];
                                printf( __( 'Oops! That page can&rsquo;t be found. <b> %s </b>', 'tankenbak' ), $request );
                            ?>
                            <p><a class="home_btn" href="<?php echo get_bloginfo('url'); ?>"> <?php _e('Home', 'tankenbak') ?></a></p>
                            </p>
                        </header><!-- .page-header -->
                    </article><!-- #post-## -->
                </main><!-- #main -->
            </div><!-- #primary -->

        <?php
        //sidebar load
            if( $template_data['layout']['sidebar'] && ( $page_class['sidebar']['type'] == 'second' ) ): ?>
                <?php get_sidebar_tb( $template_data['layout']['sidebar'], $template_data ); ?>
        <?php endif; ?>

</div>

<?php get_footer(); ?>

