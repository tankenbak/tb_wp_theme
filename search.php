<?php
/**
 * The template for displaying search page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 *
 * @since 0.8.5
 * Rebuild to use only this fiel as base template, and tankenbak_get_template as setup for data display;
 */

get_header(); ?>


<?php
$args = array(
    'content'   => 'search-block',
    'sidebar'   => false,
    );
$template_data['layout'] =  tankenbak_get_template( $args ); // setup all default data for layout
$template_data['post'] = $post;

get_template_part_loop( 'before', 'archive-loop', $template_data );
?>

<header class="page-header col-xs-12">
    <h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'tankenbak' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
</header><!-- .page-header -->
<?php
// this structure is used for singular - it is default template so we must catch different types :)
    if( have_posts() ):
        while ( have_posts() ) : the_post();
            // you can change from where content should be loaded
            fc_post($post, 'grid' );

        endwhile; // End of the loop.
        ?>
        <footer class="col-xs-12 navi-block">
        <?php
        $pagination_type = $option['general-pagination'];
        switch ($pagination_type) {
            case '1':
                vb_pagination();
                break;
            default:
                the_posts_navigation();
                break;
        }
        ?>
        </footer>
        <?php
    else:
        get_template_part_content( 'content', 'none' ); // post/s not found
    endif;

get_template_part_loop( 'after', 'archive-loop',  $template_data );

?>
<?php get_footer(); ?>