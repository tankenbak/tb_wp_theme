<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TankenBak
 */
    //209/8

    $thumbs = [ 209, 208, 210 ];

    foreach ( $thumbs as $key => $value ) {
        update_post_thumbnail_cache();
        get_the_post_thumbnail( $value );
    }


    global $option;
    $footer_class = ' site-footer';

    $wp_query = $GLOBALS['wp_query'];
    echo "<div class='btn btn-success ajax-test' data-action='ajx_tester' >Ajax - Extra function call</div>";
    //var_dump($option);
?>

<!-- Root element of PhotoSwipe. Must have class pswp. -->

    </div><!-- #content -->

    <footer id="colophon" class="<?php echo $footer_class; ?>" role="contentinfo">
        <div class="footer-background" style="background: <?php echo $option['footer-background-image']['background-color'] ?> ">
            <?php
            if ( isset( $option['footer-background-image']['media']['id'] ) &&  $option['footer-background-image']['media']['id'] != '' ) {
                # code...
                lazy_thumb(array(
                    'id' => $option['footer-background-image']['media']['id'],
                    'proportion' => 'none',
                    'size'     => array(
                          'big'       => 'tb-extra_large',
                          'medium'    => 'tb-large',
                          'small'     => 'tb-small',
                      )
                ));
            }
            ?>
        </div>
        <div class="footer-overlay"></div>
        <div class="footer-content container">
            <div class="site-info">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                    <?php
                    if ( ! is_active_sidebar( 'footer-1' ) ) {
                        echo __('Set up footer widget!', 'tankenbak');
                    }else{
                        dynamic_sidebar( 'footer-1' );
                    }
                    ?>

                    </div>
                    <div class="col-xs-12 col-sm-4">
                    <?php
                    if ( ! is_active_sidebar( 'footer-2' ) ) {
                        echo __('Set up footer widget!', 'tankenbak');
                    }else{
                        dynamic_sidebar( 'footer-2' );
                    }
                    ?>

                    </div>
                    <div class="col-xs-12 col-sm-4">
                    <?php
                    if ( ! is_active_sidebar( 'footer-3' ) ) {
                        echo __('Set up footer widget!', 'tankenbak');
                    }else{
                        dynamic_sidebar( 'footer-3' );
                    }
                    ?>

                    </div>
                </div>
            </div><!-- .site-info -->
        </div>
        <div class="cpr-block">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <?php printf( esc_html__( 'Copywright &copy; '.date('Y').' %1$s', 'tankenbak' ), get_bloginfo('name') ); ?>
                        </div>
                        <div class="pull-right">
                            <?php printf( esc_html__( 'Designed & delivered by %1$s.', 'tankenbak' ), '<a href="http://tankenbak.no/" rel="designer">Tanken Bak</a>' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer><!-- #colophon -->
</div><!-- #page -->

 <?php
  // cookie law info
  if (isset($option['general-cookie-law']) && $option['general-cookie-law'] == true)
    get_template_part_part( 'block', 'cookie-law' );
?>

<?php wp_footer(); ?>


</body>
</html>
