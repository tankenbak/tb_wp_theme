<div class="archive-title col-xs-12">
    <h1 class="page-title">
        <?php if ( is_day() ) : ?>
            <?php printf( __( 'Daily Archives: <span>%s</span>', 'tankenbak' ), get_the_date() ); ?>
        <?php elseif ( is_month() ) : ?>
            <?php printf( __( 'Monthly Archives: <span>%s</span>', 'tankenbak' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'tankenbak' ) ) ); ?>
        <?php elseif ( is_year() ) : ?>
            <?php printf( __( 'Yearly Archives: <span>%s</span>', 'tankenbak' ), get_the_date( _x( 'Y', 'yearly archives date format', 'tankenbak' ) ) ); ?>
        <?php elseif ( is_category() ) : ?>
            <?php  printf( __( 'Category Archives: <span>%s</span>', 'tankenbak' ), single_cat_title( '', false ) ); ?>
        <?php elseif ( is_tag() ) : ?>
            <?php  printf( __( 'Tag Archives: <span>%s</span>', 'tankenbak' ), single_tag_title( '', false ) ); ?>
        <?php else : ?>
            <?php _e( 'Blog', 'tankenbak' ); ?>
        <?php endif; ?>
    </h1>
</div>
