<?php
/**
 * Template part for displaying Entry Meta
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 */
$post_type = get_post_type();
?>
<div class="entry-meta <?php echo $post_type ?>-block">
    <?php tankenbak_posted_on(); ?>
</div><!-- .entry-meta -->
