<?php
/**
 * Template part for displaying Entry Title
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 */
$post_type = get_post_type();
if(get_post_meta(get_the_ID(), 'display_title', true) != 'no') :
    ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <header class="entry-header <?php echo $post_type ?>-block">
                    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                </header><!-- .entry-header -->
            </div>
        </div>
    </div>

<?php endif ?>
