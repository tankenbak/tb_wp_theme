<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 */
global $option;
$cols = $option['blog_page-blog-box-count'];
$class = null;
switch ($cols) {
	case '1':
		$class = 'col-xs-12';
		break;
	case '2':
		$class = 'col-md-6 col-sm-12 col-xs-12 col-ms-6';
		break;
	case '3':
		$class = 'col-md-4 col-sm-6 col-xs-12';
		break;
	case '4':
		$class = 'col-md-3 col-sm-6 col-xs-12';
		break;

	default:
		$class = 'col-sm-3 col-xs-12';
		break;
$class .= ' post-outer ';
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class($class); ?> >
	<div class="post hentry">
		<div class="entry-body entry-content">
			<div class="entry-container">
					<a title="" href="<?php echo get_permalink() ?>">
						<?php lazy_thumb(); ?>
					</a>
				<div class="entry-inner">
					<div class="entry-header">
						<h2>
							<a href="<?php echo get_permalink() ?>">
								<?php the_title(); ?>
							</a>
						</h2>
						<div class="entry-meta">
							<?php tankenbak_posted_on(); ?>
							<?php tankenbak_entry_footer(); ?>
						</div><!-- .entry-meta -->
					</div>
					<div class="entry_excerpt clearfix">
						<p>Donec congue orci eget sem cursus, ac vehicula nisi ornare. Nulla elit nibh, dignissim nec leo non,</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>



