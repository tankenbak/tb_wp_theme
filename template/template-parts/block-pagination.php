<?php
/**
 * Pagination  markup
 *
 */
global $option;
?>
<footer class="col-xs-12 navi-block">
    <?php
    $pagination_type = $option['general-pagination'];
    switch ($pagination_type) {
        case '1':
            vb_pagination();
            break;
        default:
            the_posts_navigation();
            break;
    }
    ?>
</footer>