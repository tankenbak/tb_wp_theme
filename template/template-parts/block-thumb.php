<?php
/**
 * Template part for displaying Title
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 */
global $post;
$post_type = $post->post_type;
?>
<div class="entry-thumb <?php echo $post_type ?>-block">
<?php
    if (has_post_thumbnail( $post->ID ) ){
        $args = array(
            'proportion'  =>  '16x9'
        );
       lazy_thumb();

    }
 ?>
</div>