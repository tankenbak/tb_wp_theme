<?php
/**
 * Template part for displaying Entry Content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 */
$post_type = get_post_type();
?>
<div class="entry-content <?php echo $post_type ?>-block">
    <?php the_content(); ?>
    <?php
        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'tankenbak' ),
            'after'  => '</div>',
        ) );
    ?>
</div><!-- .entry-content -->
