<?php
/**
 * Template part for displaying Share Content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 */

?>
<?php
$post_type = get_post_type();
$author_id = get_the_author_meta('ID');

$size = 'thumbnail';
$imgURL = get_avatar( $author_id );
$auth_name = get_the_author_meta('display_name');
$auth_biogr = get_the_author_meta('description');
?>
<div class="entry-author row <?php echo $post_type ?>-block">
    <div class="author-contant col-xs-12">
        <div class="author-avatar">
            <?php echo $imgURL ?>
        </div>
        <div class="author-name"><?php echo $auth_name; ?></div>
        <div class="author-info">
            <?php echo $auth_biogr; ?>
        </div>
    </div>
</div>
