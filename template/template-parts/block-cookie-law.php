<?php
/**
 * Template part is for displaying Cookie law information bar
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 * @since  TankenBak 0.6.2
 */
global $option;
  if (isset($option['general-cookie-text']) && $option['general-cookie-text'] != ''){
    $cookie_text = $option['general-cookie-text'];
  }else{
    $cookie_text = __('This site uses cookies. By continuing to browse the site, you are agreeing to our use of cookies. Find out more.','tankenbak');
  }

  if (isset($option['general-cookie-accept']) && $option['general-cookie-accept'] != ''){
    $leabel_ok = $option['general-cookie-accept'];
  }else{
    $leabel_ok = _x('Ok', 'Cookie law default leabel Button', 'tankenbak');
    }

  if (isset($option['general-cookie-more']) && $option['general-cookie-more'] != ''){
    $leabel_more = $option['general-cookie-more'];
  }else{
   $leabel_more = _x('Read more', 'Cookie law default leabel Button', 'tankenbak');
  }

  if (isset($option['general-cookie-link']) && $option['general-cookie-link'] != ''){
    $more_link = $option['general-cookie-link'];
  }else{
    $more_link = 'https://en.wikipedia.org/wiki/HTTP_cookie';
  }



?>

<div id="cookie-law" class="empty"></div>
<script type="text/html" id="cookie-element">
<div id="cookie-law" class="cookie-container container-fluid cookie-pop">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
              <table class="cookie-table">
                  <tr>
                    <td class="cookie-body"><?php echo $cookie_text ?></td>
                    <td class="cookie-buttons">
                        <a id="accept-cookie" class="cookie-accept " href="#"><?php echo $leabel_ok; ?></a>
                        <a class="cookie-read_more" targer="_blank" href="<?php echo $more_link ?>"><?php echo $leabel_more; ?></a>
                    </td>
                  </tr>
              </table>
            </div>
        </div>
    </div>
</div>
</script>
<?php
  if (isset($option['general-cookie-debug']) && $option['general-cookie-debug'] == true){
    ?>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      Cookies.remove('cookie_law');
    });
  </script>
    <?php
  }
 ?>


