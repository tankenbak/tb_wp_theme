<?php
/**
 * Template part for displaying Share Content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 */
$post_type = get_post_type();
?>
<div class="entry-share row <?php echo $post_type ?>-block">
    <div class="col-xs-12">
        <h5 class="share-header"><?php echo __('Share It!', 'tankenbak'); ?></h5>
        <div class="share-contant">
            <?php
                do_social_share( $args );
            ?>
        </div>
    </div>
</div>
