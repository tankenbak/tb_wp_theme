<?php
/**
 * Template part for displaying Entry Categories and Tags
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 */
$post_type = get_post_type();
?>

<footer class="entry-footer <?php echo $post_type ?>-block">
    <?php tankenbak_entry_footer(); ?>
</footer><!-- .entry-footer -->
