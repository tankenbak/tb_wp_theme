<?php
/**
 * Custom class and container use before displaing loop in post types
 * This is for single item
 *
 * @package TankenBak
 *
 * @since 0.8.5
 *
 * get_page_classes_tb - return classes for page, located in custom-function.php
 */

$page_class = get_page_classes_tb( $template_data );

// create container for sidebar
if ( $template_data['layout']['sidebar'] ) {
    echo "<div class='container sidebar-enabled'>";
    $page_class['page']['class'] .= ' row';
}else{
    echo "<div class='sidebar-disabled'>";
}
?>
<div class=" <?php echo $page_class['page']['class'] ?>">
<?php
//sidebar load
if( $template_data['layout']['sidebar'] && ( $page_class['sidebar']['type'] == 'first' ) ): ?>
    <?php get_sidebar_tb( $template_data['layout']['sidebar'], $template_data ); ?>
<?php endif; ?>
    <div id="primary" class="content-area <?php echo $page_class['container']['class'] ?>">
        <main id="main" class="site-main" role="main">


<?php
// additional for post
if ( get_post_type() != 'page' ): ?>
    <div class="container">
        <div class="<?php //row ?> equal-height-byrow">
<?php endif ?>