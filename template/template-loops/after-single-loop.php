<?php
/**
 * Custom class and container use after displaing loop in post types
 * This is for single item
 *
 * @package TankenBak
 *
 * @since 0.8.5
 *
 * get_page_classes_tb - return classes for page, located in custom-function.php
 */
$page_class = get_page_classes_tb( $template_data );
?>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
//sidebar load
    if( $template_data['layout']['sidebar'] && ( $page_class['sidebar']['type'] == 'second' ) ): ?>
        <?php get_sidebar_tb( $template_data['layout']['sidebar'], $template_data ); ?>
<?php endif; ?>

</div> <!-- .row -->
</div> <!-- .container -->

<?php
// single post
if ( get_post_type() != 'page' ): ?>
</div></div>
<?php endif ?>