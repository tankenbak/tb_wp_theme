<?php
/**
 * Template part for displaying Entry posts.
 * You're inside the loop
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 */
?>
<?php
global $option;
$post_type = get_page_type_tankenbak();?>
<article id="post-<?php the_ID(); ?>" <?php post_class($post_type); ?>>
<?php
        get_template_part( 'template/template-parts/block', 'thumb' );

        get_template_part( 'template/template-parts/block', 'title' );

        get_template_part( 'template/template-parts/block', 'content' );

       // get_template_part( 'template/template-parts/block', 'author' );

        //get_template_part( 'template/template-parts/block', 'share' );

        //get_template_part( 'template/template-parts/block', 'meta' );

       /// get_template_part( 'template/template-parts/block', 'cats-tags' );

?>

</article><!-- #post-## -->
