<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 *
 * @since 0.8.5
 * Rebuild to use only this fiel as base template, and tankenbak_get_template as setup for data display;
 */
global $post;
get_header(); ?>
<?php

$sidebar = get_post_meta(  $post->ID , 'sidebar',true );
$args = array(
    'sidebar_collapse' => 'bottom',
    'sidebar_position' => 'right',
    'sidebar'       => true,
    );
if ( isset($sidebar) && $sidebar != '' && $sidebar != '0' ) {
    $args['sidebar'] = true;
    $args['sidebar_name'] = $sidebar;
}

$template_data['layout'] =  tankenbak_get_template( $args ); // setup all default data for layout

//$template_data['post'] = $post;

get_template_part_loop( 'before', 'single-loop', $template_data );


if( have_posts() ):
    while ( have_posts() ) : the_post();

        // you can change from where content should be loaded
        get_template_part_content( 'content', 'single', $template_data );

        if( $template_data['layout']['navigation'] ):
            // post navigation
            the_post_navigation();
        endif;
        if($template_data['layout']['comments']):
            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;
        endif;

    endwhile; // End of the loop.
else:
    get_template_part_content( 'content', 'none' ); // post/s not found
endif;

get_template_part_loop( 'after', 'single-loop',  $template_data );

?>
<?php get_footer(); ?>
