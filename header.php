<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TankenBak
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<?php
/*
//save wp Queries !
<meta charset="<?php bloginfo( 'charset' ); ?>">
// we dont need pingback
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
*/
?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />


<meta name="application-name" content="<?php echo bloginfo( 'name' ); ?>"/>
<meta name="apple-mobile-web-app-title" content="<?php echo bloginfo( 'name' ); ?>">

<meta name="theme-color" content="#0D52A0">
<!-- Windows Phone -->
<meta name="msapplication-navbutton-color" content="#0D52A0">
<!-- Windows -->
<meta name="msapplication-TileColor" content="#0D52A0"/>
<!-- <meta name="msapplication-square150x150logo" content="square.png"/> -->

<!-- iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="#0D52A0">
<!--[if IE]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <link rel="stylesheet" id="ie-style" type="text/css" href="<?php echo TB_CSS ?>/ie.css">
<![endif]-->
<?php
	wp_head();
	global $option;
?>
</head>

<?php flush(); ?>
<body <?php body_class(); ?>>

<?php
	//echo '<code class="queries">(header.php) &nbsp;&nbsp;'.get_num_queries().' queries in '. timer_stop( 0, 5 ). ' seconds </code>';
?>

<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'tankenbak' ); ?></a>

	<header id="masthead" class=" " role="banner">
	<div class="container">
		<div class="site-header-holder row">
			<div class="col-xs-12">
				<div class="site-branding">
					<a class="page-logo" title="<?php echo get_bloginfo('name') ?>" href="<?php echo get_bloginfo( 'url' ) ?>">
						<?php
						if ( isset($option['general-logo-image']['media']['id'] ) && $option['general-logo-image']['media']['id'] != '' ):
							$image_data = wp_prepare_attachment_for_js($option['general-logo-image']['media']['id']);
							?>
							<img role="page-logo" alt="<?php echo $image_data['url'] ?>" src="<?php echo $image_data['url'] ?>">
						<?php else: ?>
							<img role="page-logo" alt="<?php echo TB_IMG ?>/logo.svg" src="<?php echo TB_IMG ?>/logo.svg">
						<?php endif; ?>
					</a>
				</div><!-- .site-branding -->

				<nav id="site-navigation" class="main-navigation" role="navigation">
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"></button>
					<?php wp_nav_menu( array(
						'theme_location' => 'primary',
						'container' => 'div',
						'container_id'	=> 'primary-menu',
						'menu_id' => ''
					) ); ?>
				</nav><!-- #site-navigation -->
			</div>
		</div>
	</div>
	</header><!-- #masthead -->
	<?php /*
	<div id="ajax-text" class="">
		<button data-action="ajax_short_init" class="btn btn-info ajax-test">Short init ajax</button>
		<button data-action="ajax_standard" class="btn btn-info ajax-test">Standard ajax</button>
		<button data-action="ajax_custom" class="btn btn-info ajax-test">custom ajax</button>
		<code class="log">log</code>
	</div>
	*/?>
	<div id="content" role="page-content">
