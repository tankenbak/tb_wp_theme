<?php
/**
 * The template for displaying archives page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package TankenBak
 *
 * @since 0.8.5
 * Rebuild to use only this fiel as base template, and tankenbak_get_template as setup for data display;
 */
get_header(); ?>
<?php
$args = array(
    'sidebar'   => false,
    );
$template_data['layout'] =  tankenbak_get_template( $args ); // setup all default data for layout
$template_data['post'] = $post;

get_template_part_loop( 'before', 'archive-loop', $template_data );

        get_template_part_part( 'block', 'archive-title' );
        if( have_posts() ):
            while ( have_posts() ) : the_post();
                fc_post($post, 'grid' );
                // you can change from where content should be loaded
                //get_template_part_content( 'content', $template_data['layout']['content'], $template_data );

            endwhile; // End of the loop.
            get_template_part_part( 'block', 'pagination' );
        else:
            get_template_part_content( 'content', 'none'); // post/s not found
        endif;

get_template_part_loop( 'after', 'archive-loop',  $template_data );

?>
<?php get_footer(); ?>
